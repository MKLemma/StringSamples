let graphtables = [];
let svgBox;
function constructSuffixArray(str) {
    const arr = new Array(str.length);
    for (let i = 0; i < str.length; i++) {
        arr[i] = i;
    }
    const func = function (item1, item2) {
        for (let i = 0; i <= str.length; i++) {
            if (item1 + i >= str.length || item2 + i >= str.length)
                break;
            if (str.charAt(item1 + i) < str.charAt(item2 + i)) {
                return -1;
            }
            else if (str.charAt(item1 + i) > str.charAt(item2 + i)) {
                return 1;
            }
        }
        if (item1 == item2) {
            return 0;
        }
        else {
            return item1 < item2 ? 1 : -1;
        }
    };
    arr.sort(func);
    return arr;
}
function createTextTable(text, canvas) {
    const textTable = new GraphTableSVG.GTable(canvas, { id: "text-table", columnCount: text.length + 1, rowCount: 2 });
    textTable.cells[0][0].svgText.textContent = "Index";
    //textTable.cells[0][0].svgGroup.setAttribute("class", "caption-cell");
    textTable.cells[1][0].svgText.textContent = "Text";
    //textTable.cells[1][0].svgGroup.setAttribute("class", "caption-cell");
    for (let i = 0; i < text.length; i++) {
        textTable.cells[0][i + 1].svgText.textContent = `${i + 1}`;
        //textTable.cells[0][i + 1].svgGroup.setAttribute("class", "cell");
        textTable.cells[1][i + 1].svgText.textContent = `${text[i]}`;
        //textTable.cells[1][i + 1].svgGroup.setAttribute("class", "cell");
    }
    return textTable;
}
function createSuffixArrayTable(text, canvas) {
    const sa = constructSuffixArray(text);
    const table = new GraphTableSVG.GTable(canvas, { id: "sa-table", columnCount: 2, rowCount: sa.length + 1 });
    table.cells[0][0].svgText.textContent = "SA";
    //table.cells[0][0].svgGroup.setAttribute("class", "caption-cell");
    table.cells[0][1].svgText.textContent = "Text";
    //table.cells[0][1].svgGroup.setAttribute("class", "caption-cell");
    for (let i = 0; i < sa.length; i++) {
        table.cells[i + 1][0].svgText.textContent = (sa[i] + 1).toString();
        //table.cells[i + 1][0].svgGroup.setAttribute("class", "cell");
        const suffix = text.substr(sa[i]);
        table.cells[i + 1][1].svgText.textContent = suffix;
        //table.cells[i + 1][1].svgGroup.setAttribute("class", "suffix-cell");
    }
    return table;
}
function createDisplay() {
    const text = GraphTableSVG.GUI.getInputText("textbox");
    svgBox.innerHTML = "";
    const textTable = createTextTable(text, svgBox);
    const saTable = createSuffixArrayTable(text, svgBox);
    const textRegion = textTable.getRegion();
    saTable.svgGroup.setY(textRegion.height + 30);
    graphtables = [textTable, saTable];
    GraphTableSVG.GUI.autostrech(svgBox, graphtables);
}
window.onload = () => {
    svgBox = GraphTableSVG.GUI.getNonNullElementById('svgbox');
    GraphTableSVG.GUI.setURLParametersToHTMLElements();
    createDisplay();
};
