﻿import SVGTable = GraphTableSVG.GTable;
import SVGToVBA = GraphTableSVG.SVGToVBA;
import Graph = GraphTableSVG.GGraph;
//const svgBox: HTMLElement;
let graphtables : (SVGTable | Graph)[] = [];
let svgBox : SVGSVGElement;


function createLZ77WSRLogicTable(text : string) : [GraphTableSVG.LogicTable, string] {
    const result = StringModule.LZ77WithSelfReference(text);
    const ref_num = result.filter((v)=> typeof(v) != "string").length;

    const table = new GraphTableSVG.LogicTable({columnCount : text.length, rowCount : ref_num+2, tableClassName : "table_text"});
    table.cellArray.forEach((v)=>{        
        v.topBorderClass = "no-border";
        v.leftBorderClass = "no-border";
        v.rightBorderClass = "no-border";
        v.bottomBorderClass = "no-border";
    })
    
    for (let i = 0; i < text.length; i++) {
        table.cells[0][i].set((i+1).toString(), false, "index_cell", undefined, "index_cell_text")
        table.cells[1][i].set(text[i], false, "text_cell", undefined, "text_cell_text")
    }

    let outputText = "";
    let x = 0;
    let y = 0;
    for (let i = 0; i < result.length; i++) {
        const r = result[i];
        if (typeof r == "string") {
            x++;
            outputText += `(${r})`;            
        } else {
            table.rowHeights[2 + y] = 10;
            const startPos = <number>r[0];
            const length = r[1];
            for (let p = 0; p < length; p++) {
                table.cells[2 + y][x].backgroundClass = "ref-background";         
                table.cells[2 + y][startPos + p].bottomBorderClass = "ref-border";
                if(3+y < table.rowHeights.length){
                    table.cells[3 + y][startPos + p].topBorderClass = null;                    
                }
                x++;
            }
            y++;
            outputText += `(${r[0] + 1},${r[1]})`;            

        }
    }
    return [table, outputText];
}

function createLZ77WSRTable() {
    svgBox.innerHTML = "";    
    const text: string = GraphTableSVG.GUI.getInputText(`textbox`);
    const [logicTable, output] = createLZ77WSRLogicTable(text);
    (<HTMLTextAreaElement>document.getElementById('output')).value = output;

    const table = new GraphTableSVG.GTable(svgBox);

    table.constructFromLogicTable(logicTable);
    /*
    const result = StringModule.LZ77WithSelfReference(text);
    const ref_num = result.filter((v)=> typeof(v) != "string").length;
    
    svgBox.innerHTML = "";
    const table = new SVGTable(svgBox, text.length, ref_num + 2, "table_text");
    table.borders.forEach(function (x, i, arr) { x.style.visibility = "hidden"; });
    
    for (let i = 0; i < text.length; i++) {
        table.cells[0][i].svgText.textContent = (i+1).toString();
        GraphTableSVG.SVG.setClass(table.cells[0][i].svgGroup, "index_cell");
        GraphTableSVG.SVG.setClass(table.cells[0][i].svgText, "index_cell_text");
        GraphTableSVG.SVG.setClass(table.cells[1][i].svgGroup, "text_cell");
        GraphTableSVG.SVG.setClass(table.cells[1][i].svgText, "text_cell_text");

        table.cells[1][i].svgText.textContent = text[i];
        
    }
    

    const outputBox : HTMLTextAreaElement = <HTMLTextAreaElement>document.getElementById('output');
    outputBox.textContent = "";

    let x = 0;
    let y = 0;
    const rows = table.rows;
    for (let i = 0; i < result.length; i++) {
        const r = result[i];
        if (typeof r == "string") {
            x++;
            outputBox.textContent += `(${r})`;            
        } else {
            rows[2 + y].height = 10;
            const startPos = <number>r[0];
            const length = r[1];
            for (let p = 0; p < length; p++) {
                table.cells[2 + y][x].svgBackground.style.fill = "red";
                const bottomLine = table.cells[2 + y][startPos + p].bottomBorder;
                bottomLine.style.visibility = "visible";
                bottomLine.style.stroke = "blue";
                bottomLine.style.strokeWidth = "2pt"
                x++;
            }
            y++;
            outputBox.textContent += `(${r[0] + 1},${r[1]})`;            

        }
    }
    */
    //table.resize();

    graphtables = [table];
    table.update();
    GraphTableSVG.GUI.setSVGBoxSize(svgBox, GraphTableSVG.Common.getRegion(graphtables), new GraphTableSVG.Padding(10,10,10,10));
    
}


window.onload = () => {
    svgBox = <any>GraphTableSVG.GUI.getNonNullElementById('svgbox');
    //const svgBox = document.getElementById('svgbox');   
    GraphTableSVG.GUI.setURLParametersToHTMLElements();
    createLZ77WSRTable();
};
