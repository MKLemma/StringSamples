var StringModule;
(function (StringModule) {
    function bunrui(strings) {
        const p = {};
        for (let i = 0; i < strings.length; i++) {
            const str1 = strings[i];
            const c = str1.charCodeAt(0);
            if (p[c] == null)
                p[c] = new Array(0);
            p[c].push(str1.substring(1));
        }
        return p;
    }
    function getChars(str) {
        const p = {};
        const r = new Array(0);
        let count = 0;
        for (let i = 0; i < str.length; i++) {
            if (p[str.charAt(i)] == null) {
                p[str.charCodeAt(i)] = str.charAt(i);
                count++;
            }
            else {
            }
        }
        for (let c in p) {
            const ch = +c;
            r.push(String.fromCharCode(ch));
        }
        return r;
    }
    function compare(str1, str2) {
        const min = Math.min(str1.length, str2.length);
        for (let i = 0; i <= min; i++) {
            if (str1.charAt(i) < str2.charAt(i)) {
                return [i, -1];
            }
            else if (str1.charAt(i) > str2.charAt(i)) {
                return [i, 1];
            }
        }
        if (str1 == str2) {
            return [str1.length, 0];
        }
        else {
            return str1.length < str2.length ? [str1.length, 1] : [str2.length, -1];
        }
    }
    StringModule.compare = compare;
    function computeSuffixArray(str) {
        const arr = new Array(str.length);
        for (let i = 0; i < str.length; i++) {
            arr[i] = i;
        }
        const func = function (item1, item2) {
            for (let i = 0; i <= str.length; i++) {
                if (item1 + i >= str.length || item2 + i >= str.length)
                    break;
                if (str.charAt(item1 + i) < str.charAt(item2 + i)) {
                    return -1;
                }
                else if (str.charAt(item1 + i) > str.charAt(item2 + i)) {
                    return 1;
                }
            }
            if (item1 == item2) {
                return 0;
            }
            else {
                return item1 < item2 ? 1 : -1;
            }
        };
        arr.sort(func);
        return arr;
    }
    StringModule.computeSuffixArray = computeSuffixArray;
    function getSortedSuffixes(str) {
        const arr = computeSuffixArray(str);
        const r = new Array(arr.length);
        for (let i = 0; i < arr.length; i++) {
            r[i] = str.substring(arr[i]);
        }
        return r;
    }
    function computeLCP(str1, str2) {
        const min = str1.length < str2.length ? str1.length : str2.length;
        let lcp = 0;
        for (let i = 0; i < min; i++) {
            if (str1.charAt[i] == str2.charAt[i]) {
                lcp++;
            }
            else {
                break;
            }
        }
        return lcp;
    }
    function computeLCPArray(str, sufarr) {
        const lcparr = new Array(sufarr.length);
        for (let i = 0; i < sufarr.length; i++) {
            if (i == 0 && sufarr.length > 1) {
                lcparr[i] = -1;
            }
            else {
                lcparr[i] = computeLCP(str.substring(sufarr[i]), str.substring(sufarr[i - 1]));
            }
        }
        return lcparr;
    }
    function createAllSuffixes(str) {
        let str2 = "";
        for (let i = 0; i < str.length; i++) {
            if (str[i] != "\n") {
                str2 += str[i];
            }
        }
        const suffixes = new Array(str2.length);
        for (let i = 0; i < suffixes.length; i++) {
            suffixes[i] = str2.substring(i);
        }
        return suffixes;
    }
    StringModule.createAllSuffixes = createAllSuffixes;
    function createAllTruncatedSuffixes(str, truncatedLength) {
        const suffixes = new Array(str.length);
        for (let i = 0; i < suffixes.length; i++) {
            suffixes[i] = str.substr(i, truncatedLength);
        }
        return suffixes;
    }
    StringModule.createAllTruncatedSuffixes = createAllTruncatedSuffixes;
    function removeSpace(str) {
        let r = "";
        const emptyCode = " ".charCodeAt(0);
        for (let i = 0; i < str.length; i++) {
            if (str.charCodeAt(i) != emptyCode) {
                r += str.charAt(i);
            }
        }
        return r;
    }
    StringModule.removeSpace = removeSpace;
    function removeFirstSpaces(str) {
        let i = 0;
        for (i = 0; i < str.length; i++) {
            if (str[i] != " ")
                break;
        }
        if (i == str.length) {
            return "";
        }
        else {
            return str.substring(i);
        }
    }
    function reverse(str) {
        const rv = [];
        for (let i = 0, n = str.length; i < n; i++) {
            rv[i] = str.charAt(n - i - 1);
        }
        return rv.join("");
    }
    StringModule.reverse = reverse;
    function LZ77WithSelfReference(str) {
        const r = new Array(0);
        let startPos = 0;
        let lastRefPos = -1;
        let i = 0;
        while (i < str.length) {
            const substr = str.substr(startPos, i - startPos + 1);
            const reference = i == 0 ? "" : str.substr(0, i);
            const refPos = reference.indexOf(substr);
            if (refPos == -1) {
                if (lastRefPos == -1) {
                    r.push(substr);
                    i++;
                }
                else {
                    r.push([lastRefPos, i - startPos]);
                }
                startPos = i;
                lastRefPos = -1;
            }
            else {
                lastRefPos = refPos;
                i++;
            }
        }
        if (lastRefPos != -1) {
            r.push([lastRefPos, str.length - startPos]);
        }
        return r;
    }
    StringModule.LZ77WithSelfReference = LZ77WithSelfReference;
    function computeEditDisutanceTable(str1, str2) {
        str1 = "_" + str1;
        str2 = "_" + str2;
        const r = new Array(str1.length);
        for (let y = 0; y < r.length; y++) {
            r[y] = new Array(str2.length);
            for (let x = 0; x < r[y].length; x++) {
                r[y][x] = -1;
            }
        }
        for (let y = 0; y < str1.length; y++) {
            r[y][0] = y;
        }
        for (let x = 0; x < str2.length; x++) {
            r[0][x] = x;
        }
        for (let y = 0; y < r.length; y++) {
            for (let x = 0; x < r[y].length; x++) {
                if (r[y][x] == -1) {
                    const top = r[y - 1][x] + 1;
                    const left = r[y][x - 1] + 1;
                    const upperLeft = r[y - 1][x - 1] + (str1[y] == str2[x] ? 0 : 1);
                    r[y][x] = Math.min(top, left, upperLeft);
                }
            }
        }
        return r;
    }
    StringModule.computeEditDisutanceTable = computeEditDisutanceTable;
})(StringModule || (StringModule = {}));
var SVGTable = GraphTableSVG.GTable;
var SVGToVBA = GraphTableSVG.SVGToVBA;
var Graph = GraphTableSVG.GGraph;
//const svgBox: HTMLElement;
let graphtables = [];
let svgBox;
function createLZ77WSRLogicTable(text) {
    const result = StringModule.LZ77WithSelfReference(text);
    const ref_num = result.filter((v) => typeof (v) != "string").length;
    const table = new GraphTableSVG.LogicTable({ columnCount: text.length, rowCount: ref_num + 2, tableClassName: "table_text" });
    table.cellArray.forEach((v) => {
        v.topBorderClass = "no-border";
        v.leftBorderClass = "no-border";
        v.rightBorderClass = "no-border";
        v.bottomBorderClass = "no-border";
    });
    for (let i = 0; i < text.length; i++) {
        table.cells[0][i].set((i + 1).toString(), false, "index_cell", undefined, "index_cell_text");
        table.cells[1][i].set(text[i], false, "text_cell", undefined, "text_cell_text");
    }
    let outputText = "";
    let x = 0;
    let y = 0;
    for (let i = 0; i < result.length; i++) {
        const r = result[i];
        if (typeof r == "string") {
            x++;
            outputText += `(${r})`;
        }
        else {
            table.rowHeights[2 + y] = 10;
            const startPos = r[0];
            const length = r[1];
            for (let p = 0; p < length; p++) {
                table.cells[2 + y][x].backgroundClass = "ref-background";
                table.cells[2 + y][startPos + p].bottomBorderClass = "ref-border";
                if (3 + y < table.rowHeights.length) {
                    table.cells[3 + y][startPos + p].topBorderClass = null;
                }
                x++;
            }
            y++;
            outputText += `(${r[0] + 1},${r[1]})`;
        }
    }
    return [table, outputText];
}
function createLZ77WSRTable() {
    svgBox.innerHTML = "";
    const text = GraphTableSVG.GUI.getInputText(`textbox`);
    const [logicTable, output] = createLZ77WSRLogicTable(text);
    document.getElementById('output').value = output;
    const table = new GraphTableSVG.GTable(svgBox);
    table.constructFromLogicTable(logicTable);
    /*
    const result = StringModule.LZ77WithSelfReference(text);
    const ref_num = result.filter((v)=> typeof(v) != "string").length;
    
    svgBox.innerHTML = "";
    const table = new SVGTable(svgBox, text.length, ref_num + 2, "table_text");
    table.borders.forEach(function (x, i, arr) { x.style.visibility = "hidden"; });
    
    for (let i = 0; i < text.length; i++) {
        table.cells[0][i].svgText.textContent = (i+1).toString();
        GraphTableSVG.SVG.setClass(table.cells[0][i].svgGroup, "index_cell");
        GraphTableSVG.SVG.setClass(table.cells[0][i].svgText, "index_cell_text");
        GraphTableSVG.SVG.setClass(table.cells[1][i].svgGroup, "text_cell");
        GraphTableSVG.SVG.setClass(table.cells[1][i].svgText, "text_cell_text");

        table.cells[1][i].svgText.textContent = text[i];
        
    }
    

    const outputBox : HTMLTextAreaElement = <HTMLTextAreaElement>document.getElementById('output');
    outputBox.textContent = "";

    let x = 0;
    let y = 0;
    const rows = table.rows;
    for (let i = 0; i < result.length; i++) {
        const r = result[i];
        if (typeof r == "string") {
            x++;
            outputBox.textContent += `(${r})`;
        } else {
            rows[2 + y].height = 10;
            const startPos = <number>r[0];
            const length = r[1];
            for (let p = 0; p < length; p++) {
                table.cells[2 + y][x].svgBackground.style.fill = "red";
                const bottomLine = table.cells[2 + y][startPos + p].bottomBorder;
                bottomLine.style.visibility = "visible";
                bottomLine.style.stroke = "blue";
                bottomLine.style.strokeWidth = "2pt"
                x++;
            }
            y++;
            outputBox.textContent += `(${r[0] + 1},${r[1]})`;

        }
    }
    */
    //table.resize();
    graphtables = [table];
    table.update();
    GraphTableSVG.GUI.setSVGBoxSize(svgBox, GraphTableSVG.Common.getRegion(graphtables), new GraphTableSVG.Padding(10, 10, 10, 10));
}
window.onload = () => {
    svgBox = GraphTableSVG.GUI.getNonNullElementById('svgbox');
    //const svgBox = document.getElementById('svgbox');   
    GraphTableSVG.GUI.setURLParametersToHTMLElements();
    createLZ77WSRTable();
};
