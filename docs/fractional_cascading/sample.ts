﻿import SVGTable = GraphTableSVG.GTable;
import SVGToVBA = GraphTableSVG.SVGToVBA;
import Graph = GraphTableSVG.GGraph;
//const svgBox: HTMLElement;
let graphtables: (SVGTable | Graph | SVGPathElement | SVGTextElement)[] = [];
let graph : Graph;
let tree : GraphTableSVG.GGraph;
let svgBox : SVGSVGElement;
let elements : GraphTableSVG.GVertex[][] = [];
let searchPath : SVGPathElement | null = null;
let searchPathLavel : SVGTextElement | null = null;
const intervalY = 100;
const elementWidth = 25;
const startX = 100;
function parse() : number[][] {
    const text = (<HTMLTextAreaElement>document.getElementById("textbox1")).value;
    const textlinecount = text.split("\n");

    const r2 : number[][] = [];
    textlinecount.forEach((v)=>{
        const r : number[] = [];
        v.split(",").forEach((w)=>{
            const num = Number.parseInt(w);
            if(num != undefined && !isNaN(num)){
                r.push(num);
            }
        })
        r.sort((a,b)=>{
            if( a < b ) return -1;
            if( a > b ) return 1;
            return 0;
        });
        if(r.length > 0)r2.push(r);
    });
    return r2;
}
function createFCArrays(input : number[][]) : [number, number][][]{
    const r : [number, number][][] = [];
    let prev : [number, number][] = [];
    for(var y=input.length-1;y >= 0;y--){
        const line : [number, number][] = [];
        input[y].forEach((v, i) => line.push([y, i]));
        prev.forEach((v)=>line.push(v));

        line.sort((a,b)=>{
            if( input[a[0]][a[1]] < input[b[0]][b[1]] ) return -1;
            if( input[a[0]][a[1]] > input[b[0]][b[1]] ) return 1;
            return 0;
        });

        prev = [];
        for(var x=1;x < line.length;x+=2) prev.push(line[x]);
        //if(line.length % 2 == 1) prev.push(line[line.length-1]);
        r.unshift(line);
    }
    return r;
}
function randInt(min : number, max : number){
    return Math.floor( Math.random() * (max + 1 - min) ) + min ;
}
function randinput() : number[][]{

    const linecount = randInt(5, 10);
    const r : number[][] = [];
    for(let y=0;y<linecount;y++){
        const num = randInt(1, 18);    
        const line : number[] = [];
        for(let x =0;x<num;x++){
            const value = randInt(1, 99);    
            line.push(value);
        }
        r.push(line);
    }
    return r;
}
function randInputButton(){
    const p = randinput();
    (<HTMLTextAreaElement>document.getElementById("textbox1")).value = p.map((v)=>v.map((w)=>w).join(", ")).join("\n");
    create();
}
function setColor(){
    for(var y=0;y<elements.length;y++){
        for(var x=0;x<elements[y].length;x++){
            const vertex = elements[y][x];
            if(vertex.surface != null){
                vertex.surface.style.fill = vertex.tag[1] ? "white" : "aqua";
                vertex.surface.style.stroke = "black";
                vertex.surface.style.strokeWidth = "1pt";
                
                const edge : GraphTableSVG.GEdge = vertex.tag[2];
                if(edge != null){
                    edge.svgPath.style.stroke = "black";
                    edge.svgPath.style.strokeWidth = "1pt";
                }
            }
        }           
    }
    if(searchPath != null){
        svgBox.removeChild(searchPath);
        searchPath = null;
    }
    if(searchPathLavel != null){
        svgBox.removeChild(searchPathLavel);
        searchPathLavel = null;
    }
    graphtables = [graph];

}
function search(n : number){
    setColor();
    const points : [number, number][] = [];
    if(searchPath == null){
        searchPath = GraphTableSVG.SVG.createPath(svgBox, 0,0,0,0,null);
        svgBox.appendChild(searchPath);
        searchPath.style.stroke = "blue";
        searchPath.style.strokeWidth = "3pt";        
    }


    for(var y=0;y<elements.length;y++){
        let b1 = false;
        let __x=elements[y].length-1;
        for(var x=0;x<elements[y].length;x++){
            const node = elements[y][x];
            const [num, b] = node.tag;


            if(num >= n && !b1){
                b1 = true;
                __x = x;
            }

            if(num >= n && b){
                node.surface!.style.stroke = "red";
                node.surface!.style.strokeWidth = "3pt";

                break;
            }
        }
        
        const node = elements[y][__x];
        const _x = node.x - (elementWidth /2);
        const _y = node.y;
        if(y == 0) points.push([_x,0]);
        points.push([_x,node.region.y]);
        points.push([_x,node.region.bottom]);
        if(y == elements.length) points.push([_x,graph.getRegion().bottom]);

    }

    searchPath.setPathLocations(points);

    for(var y=0;y<elements.length;y++){
        for(var x=0;x<elements[y].length;x++){
            const node = elements[y][x];
            const [num, b] = node.tag;
            if(num >= n){
                const edge : GraphTableSVG.GEdge = node.tag[2];
                if(edge != null){ 
                    edge.svgPath.style.stroke = "red";
                    edge.svgPath.style.strokeWidth = "2pt";

                }
                break;
            }
        }           
    }

    if(searchPathLavel == null){
        searchPathLavel = GraphTableSVG.SVG.createText();
        svgBox.appendChild(searchPathLavel);
    }
    searchPathLavel.textContent = `${n}`;
    searchPathLavel.style.fontSize = "24pt";
    searchPathLavel.setX(points[0][0]);
    searchPathLavel.setY(30);

    graphtables = [graph, searchPath, searchPathLavel];
}
function element_clicked(ev : MouseEvent) : any {
    /*
    const node = graph.getObject(this);
    if(node instanceof GraphTableSVG.GVertex){
        search(node.tag[0]);
    }else{
        setColor();
    }
    ev.stopPropagation();
    */
}

function create(){
    svgBox.innerHTML = "";
    searchPath = null;
    searchPathLavel = null;


    const input = parse();
    const p = createFCArrays(input);
    graph = new GraphTableSVG.GGraph(svgBox, {class : "graph" } );
    graphtables = [graph];

    elements = new Array(p.length);
    
    
    for(let y=0;y<p.length;y++){
        elements[y] = new Array();
        //const vertex1 = new GraphTableSVG.CircleVertex(graph, null, "", elementWidth, y * intervalY + intervalY);
        const vertex1 = <GraphTableSVG.GVertex>GraphTableSVG.createShape(graph, 
            GraphTableSVG.ShapeObjectType.Ellipse, {cx:elementWidth, cy : y * intervalY + intervalY});

        vertex1.svgText.setTextContent(`B_{${y}}`, true);
        for(let x=0;x<p[y].length;x++){
            const __y = p[y][x][0];
            const __x = p[y][x][1];
            //const vertex = new GraphTableSVG.RectangleVertex(graph, null, `${input[__y][__x]}`, x * elementWidth + startX, y * intervalY + intervalY);
            const vertex = <GraphTableSVG.GVertex>GraphTableSVG.createShape(graph, 
                GraphTableSVG.ShapeObjectType.Rect,{text : `${input[__y][__x]}`, cx : x * elementWidth + startX, cy : y * intervalY + intervalY});
            vertex.width = elementWidth;
            vertex.tag = [input[__y][__x], __y == y, null];
            vertex.svgGroup.onclick = element_clicked;
            elements[y].push(vertex);
        }           
    }
    setColor();
    const createDownPointer = (x, y) =>{
        const node = elements[y][x];
        const [num, _] = node.tag;
        for(let _x=0;_x<elements[y+1].length;_x++){
            const downNode = elements[y+1][_x];
            const [downNum, _] = downNode.tag;

            if(num <= downNum){
                const edge = new GraphTableSVG.GEdge(graph.svgGroup);
                edge.markerEnd = GraphTableSVG.GEdge.createEndMarker();
                node.tag[2] = edge;
                graph.connect(node, edge, downNode, {beginConnectorType : "bottom" , endConnectorType : "top"} );

                break;
            }
        }
    }

    for(var y=0;y<elements.length-1;y++){
        for(var x=0;x<elements[y].length;x++){
            createDownPointer(x, y);
        }           
    }

    //graph.update();
}

function update(){
}

window.onload = () => {
    svgBox = <any>GraphTableSVG.GUI.getNonNullElementById('svgbox');
    svgBox.onclick = element_clicked;
    const text = (<HTMLTextAreaElement>document.getElementById("textbox1")).value;

    GraphTableSVG.GUI.observeSVGBox(svgBox, () => GraphTableSVG.Common.getRegion(graphtables));
    create();

};

