﻿import SVGTable = GraphTableSVG.GTable;
import SVGToVBA = GraphTableSVG.SVGToVBA;
import Graph = GraphTableSVG.GGraph;
//const svgBox: HTMLElement;
let graphtables: (SVGTable | Graph)[] = [];
let svgBox: HTMLElement;


function getSubstrMap(text: string): { [key: string]: number[]; } {
    var map: { [key: string]: number[]; } = {};
    for (var i = 0; i < text.length; i++) {
        for (var j = i; j < text.length; j++) {
            var sub = text.substr(i, j-i+1);
            
            if (!map[sub]) map[sub] = [];
            map[sub].push(i);
        }
    }
    return map;
}
function checkDiffArray(a : number[], b : number[]) : number | null{
    if(a.length == b.length && a.length > 0){
        var diff = b[0] - a[0];
        for (var i = 0; i < a.length; i++) {
            if (b[i] -a[i] != diff) {
                return null;
            }
        }
        return diff;
    }else{
        return null;
    }
}
function checkMinimalGenericWord(word : string, map : { [key: string]: number[]; }) : boolean{
    var pos = map[word];
    for(var key in map){
        if(word != key && map[key].length == pos.length && key.length < word.length){
            var b = checkDiffArray(pos, map[key]);
            if(b != null && b >= 0 && b + key.length <= word.length){
                return false;
            }
        }
    }
    return true;
}
function getMinimalGenericWords(text: string) : { [key: string]: number[]; } {
    var map = getSubstrMap(text);
    var map3: { [key: string]: number[]; } = {};
    for(var key in map){
        if(checkMinimalGenericWord(key, map)){
            map3[key] = map[key];
        }
    }
    return map3;
}

function checkStraddle(attractors: number[], positionSet: number[], strLength: number): boolean {
    for (var i = 0; i < positionSet.length; i++) {
        var pos = positionSet[i];
        var b: boolean = false;
        for (var j = 0; j < attractors.length; j++) {
            var attractor = attractors[j];
            if (pos <= attractor && pos + strLength - 1 >= attractor) {
                return true;
            }
        }
    }
    return false;
}
function checkAttractors(attractors: number[], substrMap: { [key: string]: number[]; }): boolean {
    for (var substr in substrMap) {
        if (!checkStraddle(attractors, substrMap[substr], substr.length)) {
            return false;
        }
    }
    return true;
}
function checkRelativeAttractors(attractors: number[], substrMap: { [key: string]: number[];}, substrOccurMap: { [key: string]: boolean;}): boolean {
    for (var substr in substrMap) {
        if (!(checkStraddle(attractors, substrMap[substr], substr.length) || substrOccurMap[substr] )) {
            return false;
        }
    }
    return true;
}

function compareArray(a: number[], b: number[]): number {
    if (a.length < b.length) {
        return -1;
    } else if (a.length > b.length) {
        return 1;
    } else {
        for (var i = 0; i < a.length; i++) {
            if (a[i] < b[i]) {
                return -1;
            } else if (a[i] > b[i]) {
                return 1;
            }
        }
        return 0;
    }
}
function compareString(a : string, b : string){
    if (a.length < b.length) {
        return -1;
    } else if (a.length > b.length) {
        return 1;
    } else {
        for (var i = 0; i < a.length; i++) {
            if (a[i] < b[i]) {
                return -1;
            } else if (a[i] > b[i]) {
                return 1;
            }
        }
        return 0;
    }
}
function combination(items: number[], k: number, isSort: boolean = true): number[][] {
    var r: number[][] = [];
    for (var i = 0; i < items.length; i++) {
        var p = items[i];
        if (k == 1) {
            r.push([p]);
        } else {            
            var arr = items.slice(i + 1, items.length);
            if (arr.length >= k - 1) {
                var r2 = combination(arr, k - 1, false);
                for (var j = 0; j < r2.length; j++) {
                    r2[j].push(p);
                    r.push(r2[j]);
                }
            }
        }
    }
    if (isSort) {
        for (var i = 0; i < r.length; i++) {
            r[i].sort((a, b) => {
                if (a < b) return -1;
                if (a > b) return 1;
                return 0;
            })
        }
        r.sort(compareArray);
    }
    return r;
}
function combination2(n : number, k: number): number[][] {
    var r : number[] = [];
    for(var i=0;i<n;i++){
        r.push(i);
    }
    return combination(r, k);
}
function getSmallestAttractors(text : string) : number[][]{
    var r : number[][] = [];
    var substrMap = getMinimalGenericWords(text);
    
    var outputBox = document.getElementById('output2');
    if(outputBox != null){
        var arr : string[] = [];
        for(var key in substrMap){
            arr.push(key);
        }
        arr.sort(compareString);
        var arr2 = arr.map((v)=>`${v} : ${substrMap[v].join(", ")}`);
        (<HTMLTextAreaElement>outputBox).value = arr2.join("\n");
    }
    
    for(var i=1;i<=text.length;i++){
        var combinations = combination2(text.length, i);
        for(var j=0;j<combinations.length;j++){
            var candidate = combinations[j];
            if(checkAttractors(candidate, substrMap)){
                r.push(candidate);
            }
        }
        if(r.length > 0) break;
    }
    return r;
}

function getSubstrOccurMap(text : string, source : string) : { [key: string]: boolean;} {
    const r: { [key: string]: boolean;} = {};

    for (var i = 0; i < text.length; i++) {
        for (var j = i; j < text.length; j++) {
            var sub = text.substr(i, j-i+1);
            if(!r[sub]){
                r[sub] = false;
                r[sub] = r[sub] || (source.indexOf(sub) != -1);
            }
        }
    }

    return r;
}

function getSmallestRelativeAttractors(text : string, source : string) : number[][]{
    var r : number[][] = [];
    var substrMap = getMinimalGenericWords(text);
    const subOccurMap = getSubstrOccurMap(text, source);
    
    var outputBox = document.getElementById('output2');
    if(outputBox != null){
        var arr : string[] = [];
        for(var key in substrMap){
            arr.push(key);
        }
        arr.sort(compareString);
        var arr2 = arr.map((v)=>`${v} : ${substrMap[v].join(", ")}`);
        (<HTMLTextAreaElement>outputBox).value = arr2.join("\n");
    }
    
    for(var i=1;i<=text.length;i++){
        var combinations = combination2(text.length, i);
        for(var j=0;j<combinations.length;j++){
            var candidate = combinations[j];
            if(checkRelativeAttractors(candidate, substrMap, subOccurMap)){
                r.push(candidate);
            }
        }
        if(r.length > 0) break;
    }
    return r;
}

function search2(){
    const text: string = GraphTableSVG.GUI.getInputText(`textbox`);
    var minimumAttractor = getSmallestAttractors(text);
    var str = minimumAttractor.map((v)=>`{${v.join(", ")}}`).join("\n");
    (<HTMLTextAreaElement>document.getElementById('output')).value = str;
}
function search3(){
    const text: string = GraphTableSVG.GUI.getInputText(`textbox`);
    const sourceText: string = GraphTableSVG.GUI.getInputText(`textbox2`);
    var minimumAttractor = getSmallestRelativeAttractors(text, sourceText);
    var str = minimumAttractor.map((v)=>`{${v.join(", ")}}`).join("\n");
    (<HTMLTextAreaElement>document.getElementById('output')).value = str;
}

window.onload = () => {
    svgBox = GraphTableSVG.GUI.getNonNullElementById('svgbox');
    //const svgBox = document.getElementById('svgbox');   
    GraphTableSVG.GUI.setURLParametersToHTMLElements();
    //var arr = combination([1, 2, 3, 4, 5, 6, 7, 8], 4);
    //console.log(arr);
};
