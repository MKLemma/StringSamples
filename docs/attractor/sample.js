var StringModule;
(function (StringModule) {
    function bunrui(strings) {
        var p = {};
        for (var i = 0; i < strings.length; i++) {
            var str1 = strings[i];
            var c = str1.charCodeAt(0);
            if (p[c] == null)
                p[c] = new Array(0);
            p[c].push(str1.substring(1));
        }
        return p;
    }
    function getChars(str) {
        var p = {};
        var r = new Array(0);
        var count = 0;
        for (var i = 0; i < str.length; i++) {
            if (p[str.charAt(i)] == null) {
                p[str.charCodeAt(i)] = str.charAt(i);
                count++;
            }
            else {
            }
        }
        for (var c in p) {
            var ch = +c;
            r.push(String.fromCharCode(ch));
        }
        return r;
    }
    function compare(str1, str2) {
        var min = Math.min(str1.length, str2.length);
        for (var i = 0; i <= min; i++) {
            if (str1.charAt(i) < str2.charAt(i)) {
                return [i, -1];
            }
            else if (str1.charAt(i) > str2.charAt(i)) {
                return [i, 1];
            }
        }
        if (str1 == str2) {
            return [str1.length, 0];
        }
        else {
            return str1.length < str2.length ? [str1.length, 1] : [str2.length, -1];
        }
    }
    StringModule.compare = compare;
    function computeSuffixArray(str) {
        var arr = new Array(str.length);
        for (var i = 0; i < str.length; i++) {
            arr[i] = i;
        }
        var func = function (item1, item2) {
            for (var i = 0; i <= str.length; i++) {
                if (item1 + i >= str.length || item2 + i >= str.length)
                    break;
                if (str.charAt(item1 + i) < str.charAt(item2 + i)) {
                    return -1;
                }
                else if (str.charAt(item1 + i) > str.charAt(item2 + i)) {
                    return 1;
                }
            }
            if (item1 == item2) {
                return 0;
            }
            else {
                return item1 < item2 ? 1 : -1;
            }
        };
        arr.sort(func);
        return arr;
    }
    StringModule.computeSuffixArray = computeSuffixArray;
    function getSortedSuffixes(str) {
        var arr = computeSuffixArray(str);
        var r = new Array(arr.length);
        for (var i = 0; i < arr.length; i++) {
            r[i] = str.substring(arr[i]);
        }
        return r;
    }
    function computeLCP(str1, str2) {
        var min = str1.length < str2.length ? str1.length : str2.length;
        var lcp = 0;
        for (var i = 0; i < min; i++) {
            if (str1.charAt[i] == str2.charAt[i]) {
                lcp++;
            }
            else {
                break;
            }
        }
        return lcp;
    }
    function computeLCPArray(str, sufarr) {
        var lcparr = new Array(sufarr.length);
        for (var i = 0; i < sufarr.length; i++) {
            if (i == 0 && sufarr.length > 1) {
                lcparr[i] = -1;
            }
            else {
                lcparr[i] = computeLCP(str.substring(sufarr[i]), str.substring(sufarr[i - 1]));
            }
        }
        return lcparr;
    }
    function createAllSuffixes(str) {
        var str2 = "";
        for (var i = 0; i < str.length; i++) {
            if (str[i] != "\n") {
                str2 += str[i];
            }
        }
        var suffixes = new Array(str2.length);
        for (var i = 0; i < suffixes.length; i++) {
            suffixes[i] = str2.substring(i);
        }
        return suffixes;
    }
    StringModule.createAllSuffixes = createAllSuffixes;
    function createAllTruncatedSuffixes(str, truncatedLength) {
        var suffixes = new Array(str.length);
        for (var i = 0; i < suffixes.length; i++) {
            suffixes[i] = str.substr(i, truncatedLength);
        }
        return suffixes;
    }
    StringModule.createAllTruncatedSuffixes = createAllTruncatedSuffixes;
    function removeSpace(str) {
        var r = "";
        var emptyCode = " ".charCodeAt(0);
        for (var i = 0; i < str.length; i++) {
            if (str.charCodeAt(i) != emptyCode) {
                r += str.charAt(i);
            }
        }
        return r;
    }
    StringModule.removeSpace = removeSpace;
    function removeFirstSpaces(str) {
        var i = 0;
        for (i = 0; i < str.length; i++) {
            if (str[i] != " ")
                break;
        }
        if (i == str.length) {
            return "";
        }
        else {
            return str.substring(i);
        }
    }
    function reverse(str) {
        var rv = [];
        for (var i = 0, n = str.length; i < n; i++) {
            rv[i] = str.charAt(n - i - 1);
        }
        return rv.join("");
    }
    StringModule.reverse = reverse;
    function LZ77WithSelfReference(str) {
        var r = new Array(0);
        var startPos = 0;
        var lastRefPos = -1;
        var i = 0;
        while (i < str.length) {
            var substr = str.substr(startPos, i - startPos + 1);
            var reference = i == 0 ? "" : str.substr(0, i);
            var refPos = reference.indexOf(substr);
            if (refPos == -1) {
                if (lastRefPos == -1) {
                    r.push(substr);
                    i++;
                }
                else {
                    r.push([lastRefPos, i - startPos]);
                }
                startPos = i;
                lastRefPos = -1;
            }
            else {
                lastRefPos = refPos;
                i++;
            }
        }
        if (lastRefPos != -1) {
            r.push([lastRefPos, str.length - startPos]);
        }
        return r;
    }
    StringModule.LZ77WithSelfReference = LZ77WithSelfReference;
    function computeEditDisutanceTable(str1, str2) {
        str1 = "_" + str1;
        str2 = "_" + str2;
        var r = new Array(str1.length);
        for (var y = 0; y < r.length; y++) {
            r[y] = new Array(str2.length);
            for (var x = 0; x < r[y].length; x++) {
                r[y][x] = -1;
            }
        }
        for (var y = 0; y < str1.length; y++) {
            r[y][0] = y;
        }
        for (var x = 0; x < str2.length; x++) {
            r[0][x] = x;
        }
        for (var y = 0; y < r.length; y++) {
            for (var x = 0; x < r[y].length; x++) {
                if (r[y][x] == -1) {
                    var top_1 = r[y - 1][x] + 1;
                    var left = r[y][x - 1] + 1;
                    var upperLeft = r[y - 1][x - 1] + (str1[y] == str2[x] ? 0 : 1);
                    r[y][x] = Math.min(top_1, left, upperLeft);
                }
            }
        }
        return r;
    }
    StringModule.computeEditDisutanceTable = computeEditDisutanceTable;
})(StringModule || (StringModule = {}));
var SVGTable = GraphTableSVG.GTable;
var SVGToVBA = GraphTableSVG.SVGToVBA;
var Graph = GraphTableSVG.GGraph;
//const svgBox: HTMLElement;
var graphtables = [];
var svgBox;
function getSubstrMap(text) {
    var map = {};
    for (var i = 0; i < text.length; i++) {
        for (var j = i; j < text.length; j++) {
            var sub = text.substr(i, j - i + 1);
            if (!map[sub])
                map[sub] = [];
            map[sub].push(i);
        }
    }
    return map;
}
function checkDiffArray(a, b) {
    if (a.length == b.length && a.length > 0) {
        var diff = b[0] - a[0];
        for (var i = 0; i < a.length; i++) {
            if (b[i] - a[i] != diff) {
                return null;
            }
        }
        return diff;
    }
    else {
        return null;
    }
}
function checkMinimalGenericWord(word, map) {
    var pos = map[word];
    for (var key in map) {
        if (word != key && map[key].length == pos.length && key.length < word.length) {
            var b = checkDiffArray(pos, map[key]);
            if (b != null && b >= 0 && b + key.length <= word.length) {
                return false;
            }
        }
    }
    return true;
}
function getMinimalGenericWords(text) {
    var map = getSubstrMap(text);
    var map3 = {};
    for (var key in map) {
        if (checkMinimalGenericWord(key, map)) {
            map3[key] = map[key];
        }
    }
    return map3;
}
function checkStraddle(attractors, positionSet, strLength) {
    for (var i = 0; i < positionSet.length; i++) {
        var pos = positionSet[i];
        var b = false;
        for (var j = 0; j < attractors.length; j++) {
            var attractor = attractors[j];
            if (pos <= attractor && pos + strLength - 1 >= attractor) {
                return true;
            }
        }
    }
    return false;
}
function checkAttractors(attractors, substrMap) {
    for (var substr in substrMap) {
        if (!checkStraddle(attractors, substrMap[substr], substr.length)) {
            return false;
        }
    }
    return true;
}
function checkRelativeAttractors(attractors, substrMap, substrOccurMap) {
    for (var substr in substrMap) {
        if (!(checkStraddle(attractors, substrMap[substr], substr.length) || substrOccurMap[substr])) {
            return false;
        }
    }
    return true;
}
function compareArray(a, b) {
    if (a.length < b.length) {
        return -1;
    }
    else if (a.length > b.length) {
        return 1;
    }
    else {
        for (var i = 0; i < a.length; i++) {
            if (a[i] < b[i]) {
                return -1;
            }
            else if (a[i] > b[i]) {
                return 1;
            }
        }
        return 0;
    }
}
function compareString(a, b) {
    if (a.length < b.length) {
        return -1;
    }
    else if (a.length > b.length) {
        return 1;
    }
    else {
        for (var i = 0; i < a.length; i++) {
            if (a[i] < b[i]) {
                return -1;
            }
            else if (a[i] > b[i]) {
                return 1;
            }
        }
        return 0;
    }
}
function combination(items, k, isSort) {
    if (isSort === void 0) { isSort = true; }
    var r = [];
    for (var i = 0; i < items.length; i++) {
        var p = items[i];
        if (k == 1) {
            r.push([p]);
        }
        else {
            var arr = items.slice(i + 1, items.length);
            if (arr.length >= k - 1) {
                var r2 = combination(arr, k - 1, false);
                for (var j = 0; j < r2.length; j++) {
                    r2[j].push(p);
                    r.push(r2[j]);
                }
            }
        }
    }
    if (isSort) {
        for (var i = 0; i < r.length; i++) {
            r[i].sort(function (a, b) {
                if (a < b)
                    return -1;
                if (a > b)
                    return 1;
                return 0;
            });
        }
        r.sort(compareArray);
    }
    return r;
}
function combination2(n, k) {
    var r = [];
    for (var i = 0; i < n; i++) {
        r.push(i);
    }
    return combination(r, k);
}
function getSmallestAttractors(text) {
    var r = [];
    var substrMap = getMinimalGenericWords(text);
    var outputBox = document.getElementById('output2');
    if (outputBox != null) {
        var arr = [];
        for (var key in substrMap) {
            arr.push(key);
        }
        arr.sort(compareString);
        var arr2 = arr.map(function (v) { return v + " : " + substrMap[v].join(", "); });
        outputBox.value = arr2.join("\n");
    }
    for (var i = 1; i <= text.length; i++) {
        var combinations = combination2(text.length, i);
        for (var j = 0; j < combinations.length; j++) {
            var candidate = combinations[j];
            if (checkAttractors(candidate, substrMap)) {
                r.push(candidate);
            }
        }
        if (r.length > 0)
            break;
    }
    return r;
}
function getSubstrOccurMap(text, source) {
    var r = {};
    for (var i = 0; i < text.length; i++) {
        for (var j = i; j < text.length; j++) {
            var sub = text.substr(i, j - i + 1);
            if (!r[sub]) {
                r[sub] = false;
                r[sub] = r[sub] || (source.indexOf(sub) != -1);
            }
        }
    }
    return r;
}
function getSmallestRelativeAttractors(text, source) {
    var r = [];
    var substrMap = getMinimalGenericWords(text);
    var subOccurMap = getSubstrOccurMap(text, source);
    var outputBox = document.getElementById('output2');
    if (outputBox != null) {
        var arr = [];
        for (var key in substrMap) {
            arr.push(key);
        }
        arr.sort(compareString);
        var arr2 = arr.map(function (v) { return v + " : " + substrMap[v].join(", "); });
        outputBox.value = arr2.join("\n");
    }
    for (var i = 1; i <= text.length; i++) {
        var combinations = combination2(text.length, i);
        for (var j = 0; j < combinations.length; j++) {
            var candidate = combinations[j];
            if (checkRelativeAttractors(candidate, substrMap, subOccurMap)) {
                r.push(candidate);
            }
        }
        if (r.length > 0)
            break;
    }
    return r;
}
function search2() {
    var text = GraphTableSVG.GUI.getInputText("textbox");
    var minimumAttractor = getSmallestAttractors(text);
    var str = minimumAttractor.map(function (v) { return "{" + v.join(", ") + "}"; }).join("\n");
    document.getElementById('output').value = str;
}
function search3() {
    var text = GraphTableSVG.GUI.getInputText("textbox");
    var sourceText = GraphTableSVG.GUI.getInputText("textbox2");
    var minimumAttractor = getSmallestRelativeAttractors(text, sourceText);
    var str = minimumAttractor.map(function (v) { return "{" + v.join(", ") + "}"; }).join("\n");
    document.getElementById('output').value = str;
}
window.onload = function () {
    svgBox = GraphTableSVG.GUI.getNonNullElementById('svgbox');
    //const svgBox = document.getElementById('svgbox');   
    GraphTableSVG.GUI.setURLParametersToHTMLElements();
    //var arr = combination([1, 2, 3, 4, 5, 6, 7, 8], 4);
    //console.log(arr);
};
