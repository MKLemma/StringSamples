﻿import SVGTable = GraphTableSVG.GTable;
import SVGToVBA = GraphTableSVG.SVGToVBA;
import Graph = GraphTableSVG.GGraph;
//const svgBox: HTMLElement;
let graphtables: (SVGTable | Graph)[] = [];
let viewer : Grammar.CFGViewer;
let svgBox : SVGSVGElement;


function create(){

    svgBox.innerHTML = "";
    const text = (<HTMLTextAreaElement>document.getElementById("textbox")).value;
    const cfg = Grammar.CFG.parse(text);
    viewer = new Grammar.CFGViewer(svgBox, null, null);
    viewer.create(cfg);
    graphtables = [viewer.table, viewer.tree];  
    console.log(cfg);
}


function update(){
}

window.onload = () => {
    svgBox = <any>GraphTableSVG.GUI.getNonNullElementById('svgbox');

    GraphTableSVG.GUI.observeSVGBox(svgBox, () => GraphTableSVG.Common.getRegion(graphtables));
    create();

};

