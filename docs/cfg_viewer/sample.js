var Grammar;
(function (Grammar) {
    class CFG {
        constructor() {
            this.rules = [];
        }
        get startVariable() {
            return this.rules[this.rules.length - 1];
        }
        //startVariables: (number | string)[] = [];
        copy() {
            const r = new CFG();
            this.rules.forEach((v) => { r.rules.push(v.copy()); });
            //this.startVariable.right.forEach((v) => { r.startVariables.push(v) });
            return r;
        }
        createDerivationTree() {
            return this.createDerivationTreeSub(this.rules.length - 1);
            //this.startVariable.right.map((v)=>this.createDerivationTreeSub(v));
        }
        createDerivationTreeSub(i) {
            if (typeof (i) == "number") {
                const variable = this.rules[i];
                const children = variable.right.map((v) => this.createDerivationTreeSub(v));
                const r = new GraphTableSVG.LogicTree({ item: variable.name, children: children, vertexText: variable.name });
                return r;
            }
            else {
                const r = new GraphTableSVG.LogicTree({ item: i, children: [], vertexText: i });
                return r;
            }
        }
        toString(i) {
            const rule = this.rules[i];
            return `${rule.name} → ${rule.right.map((v) => typeof (v) == "number" ? this.rules[v].name : v).join("")}`;
        }
        createRuleArray() {
            return this.rules.map((v, i) => [this.toString(i)]);
        }
        static parse(text) {
            const r = new CFG();
            const rules = text.split("\n");
            const dic = {};
            rules.forEach((v, i) => {
                const rule = v.split(",");
                let name = "";
                rule.forEach((w, j) => {
                    const w2 = w.trim();
                    if (j == 0) {
                        name = w2;
                        dic[name] = [i, []];
                    }
                    else {
                        dic[name][1].push(w2);
                    }
                });
            });
            r.rules = new Array(rules.length);
            Object.keys(dic).forEach((key) => {
                const val = dic[key]; // this は obj
                const element = document.getElementById(key);
                const right = val[1].map((v) => { return v in dic ? dic[v][0] : v; });
                const rule = new Rule(key, right);
                r.rules[val[0]] = rule;
            }, dic);
            return r;
        }
        getRule(right) {
            for (let i = 0; i < this.rules.length; i++) {
                if (this.rules[i].equals(right))
                    return i;
            }
            return null;
        }
    }
    Grammar.CFG = CFG;
    class Rule {
        constructor(name, right = []) {
            this.name = name;
            this.right = right;
        }
        copy() {
            return new Rule(this.name, this.right.map((v) => v));
        }
        equals(right) {
            if (this.right.length == right.length) {
                for (let i = 0; i < this.right.length; i++) {
                    if (this.right[i] != right[i])
                        return false;
                }
                return true;
            }
            else {
                return false;
            }
        }
    }
    Grammar.Rule = Rule;
    /*
    export class SLP2Dictionary{
        public cfg : CFG;
        public dic : { [key: string]: number; }
        public constructor(cfg : CFG){

        }
    }
    */
})(Grammar || (Grammar = {}));
var Grammar;
(function (Grammar) {
    class CFGViewer {
        constructor(svg, tableClass = null, graphClass = null) {
            this.tree = new GraphTableSVG.GGraph(svg, { class: graphClass });
            this.table = new GraphTableSVG.GTable(svg, { class: tableClass });
            this.table.setSize(1, 1);
            if (this.tree.vertexXInterval == null)
                this.tree.vertexXInterval = 50;
            if (this.tree.vertexYInterval == null)
                this.tree.vertexYInterval = 50;
            if (graphClass == null) {
                GraphTableSVG.CustomAttributeNames.defaultCircleRadius = 15;
            }
            this.tree.relocateAttribute = "Grammar.CFGViewer.relocateFunction";
        }
        static relocateFunction(graph) {
            GraphTableSVG.TreeArrangement.alignVerticeByLeave(graph);
            GraphTableSVG.TreeArrangement.addOffset(graph, 50, 200);
        }
        update() {
            //this.locate();
            this.table.update();
            //this.tree.update();
            this.tree.relocate();
            const rect = this.tree.getRegion();
            this.table.svgGroup.setX(rect.right + 50);
        }
        create(cfg) {
            this.dic = cfg;
            this.tree.clear();
            this.table.clear();
            this.tree.constructFromLogicTree([this.dic.createDerivationTree()], { isLatexMode: true });
            this.table.construct(this.dic.createRuleArray(), { isLatexMode: true });
            this.update();
        }
    }
    Grammar.CFGViewer = CFGViewer;
    class HistoryDic {
        constructor(viewer) {
            this.history = [];
            this.historyIndex = 0;
            this.viewer = viewer;
        }
        changeHistory(i) {
            this.viewer.create(this.history[i].copy());
            this.historyIndex = i;
        }
        add(dic) {
            if (this.historyIndex + 1 < this.history.length)
                this.history.splice(this.historyIndex + 1, this.history.length - this.historyIndex - 1);
            this.history.push(dic.copy());
            this.historyIndex++;
        }
        prev() {
            this.changeHistory(this.historyIndex - 1);
        }
        get canPrev() {
            return this.historyIndex > 0;
        }
        succ() {
            this.changeHistory(this.historyIndex + 1);
        }
        get canSucc() {
            return this.historyIndex + 1 < this.history.length;
        }
    }
    Grammar.HistoryDic = HistoryDic;
})(Grammar || (Grammar = {}));
var SVGTable = GraphTableSVG.GTable;
var SVGToVBA = GraphTableSVG.SVGToVBA;
var Graph = GraphTableSVG.GGraph;
//const svgBox: HTMLElement;
let graphtables = [];
let viewer;
let svgBox;
function create() {
    svgBox.innerHTML = "";
    const text = document.getElementById("textbox").value;
    const cfg = Grammar.CFG.parse(text);
    viewer = new Grammar.CFGViewer(svgBox, null, null);
    viewer.create(cfg);
    graphtables = [viewer.table, viewer.tree];
    console.log(cfg);
}
function update() {
}
window.onload = () => {
    svgBox = GraphTableSVG.GUI.getNonNullElementById('svgbox');
    GraphTableSVG.GUI.observeSVGBox(svgBox, () => GraphTableSVG.Common.getRegion(graphtables));
    create();
};
