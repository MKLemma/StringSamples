﻿import SVGTable = GraphTableSVG.GTable;
import SVGToVBA = GraphTableSVG.SVGToVBA;
import Graph = GraphTableSVG.GGraph;
//const svgBox: HTMLElement;
let graphtables: (SVGTable | Graph)[] = [];
let clicker : Grammar.Clicker;
let svgBox : SVGSVGElement;

function createSLP() {
    svgBox.innerHTML = "";
    const text = GraphTableSVG.GUI.getInputText("textbox");    
    clicker = new Grammar.Clicker(text, svgBox, 20, "slp-table", "slp-graph");
    clicker.clickCallback.push(updateHistoryButtons);
    graphtables = [clicker.table, clicker.graph];  
    updateHistoryButtons();  
}
function changeCSS(){
    clicker.isChoosable = !clicker.isChoosable;
    clicker.graph.vertices.forEach((v)=>{
        if(v.surface != null){
            v.surface.style.visibility = clicker.isChoosable ? "visible" : "hidden";
        }
    });
    
}
function update(){
    clicker.update();
    alert("CSS updated");
}

window.onload = () => {    
    
    svgBox = <any>GraphTableSVG.GUI.getNonNullElementById('svgbox');
    GraphTableSVG.GUI.observeSVGBox(svgBox, () => GraphTableSVG.Common.getRegion(graphtables), new GraphTableSVG.Padding(30,30,30,30));

    GraphTableSVG.GUI.setURLParametersToHTMLElements();
    
    createSLP();
   // clicker.graph.relocateAttribute = "GraphTableSVG.GTreeArrangement.alignVerticeByLeave";
   //clicker.graph.relocate();
    
};
function view(i : number){
    clicker.changeHistory(i);
    
    updateHistoryButtons();
}
function updateHistoryButtons(){
    const nb = <HTMLButtonElement>document.getElementById('next_button');
    const pb = <HTMLButtonElement>document.getElementById('prev_button');
    nb.disabled = clicker.historyIndex == clicker.history.length-1;
    pb.disabled = clicker.historyIndex == 0;
}
function prev(){
    view(clicker.historyIndex-1);
}
function next(){
    view(clicker.historyIndex+1);    
}
