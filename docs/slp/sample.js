var Grammar;
(function (Grammar) {
    ;
    ;
    class SLPDictionary {
        constructor() {
            this.slpNodes = [];
            this.startVariables = [];
        }
        //private _outcomingEdgesDic: { [key: number]: Edge[]; } = [];
        addVariable(left, right) {
            const result = this.getVariable(left, right);
            if (result == null) {
                const newNumber = this.slpNodes.length;
                const node = { type: "nonchar", value: newNumber, left: left, right: right };
                this.slpNodes.push(node);
                return node.value;
            }
            else {
                return result.value;
            }
        }
        connect(i) {
            const left = this.startVariables[i];
            const right = this.startVariables[i + 1];
            const newSig = this.addVariable(left, right);
            this.startVariables.splice(i, 2, newSig);
            return newSig;
        }
        addChar(char) {
            const result = this.getChar(char);
            if (result == null) {
                const newNumber = this.slpNodes.length;
                const node = { type: "char", value: newNumber, child: char };
                this.slpNodes.push(node);
                return node.value;
            }
            else {
                return result.value;
            }
        }
        getVariable(left, right) {
            for (let i = 0; i < this.slpNodes.length; i++) {
                const p = this.slpNodes[i];
                if (p.type == "nonchar") {
                    if (p.left == left && p.right == right) {
                        return p;
                    }
                }
            }
            return null;
        }
        getChar(child) {
            for (let i = 0; i < this.slpNodes.length; i++) {
                const p = this.slpNodes[i];
                if (p.type == "char") {
                    if (p.child == child) {
                        return p;
                    }
                }
            }
            return null;
        }
        getTextCharvariables(variable = null) {
            if (variable != null) {
                const p = this.slpNodes[variable];
                if (p.type == "char") {
                    return [p];
                }
                else {
                    const left = this.getTextCharvariables(p.left);
                    const right = this.getTextCharvariables(p.right);
                    right.forEach((v) => left.push(v));
                    return left;
                }
            }
            else {
                const r = [];
                const roots = this.startVariables.map((v) => this.getTextCharvariables(v)).forEach((v) => {
                    v.forEach((w) => {
                        r.push(w);
                    });
                });
                return r;
            }
        }
        get text() {
            let r = "";
            this.getTextCharvariables().forEach((v) => { r += v.child; });
            return r;
        }
        copy() {
            const r = new SLPDictionary();
            this.slpNodes.forEach((v) => { r.slpNodes.push(v); });
            this.startVariables.forEach((v) => { r.startVariables.push(v); });
            return r;
        }
        toCFG() {
            const r = new Grammar.CFG();
            r.rules = this.slpNodes.map((v, i) => {
                if (v.type == "char") {
                    return new Grammar.Rule(`X_{${i + 1}}`, [v.child]);
                }
                else {
                    return new Grammar.Rule(`X_{${i + 1}}`, [v.left, v.right]);
                }
            });
            if (this.startVariables.length != 1) {
                r.rules.push(new Grammar.Rule(`S`, this.startVariables.map((v) => v)));
            }
            return r;
        }
    }
    Grammar.SLPDictionary = SLPDictionary;
})(Grammar || (Grammar = {}));
var Grammar;
(function (Grammar) {
    class CFG {
        constructor() {
            this.rules = [];
        }
        get startVariable() {
            return this.rules[this.rules.length - 1];
        }
        //startVariables: (number | string)[] = [];
        copy() {
            const r = new CFG();
            this.rules.forEach((v) => { r.rules.push(v.copy()); });
            //this.startVariable.right.forEach((v) => { r.startVariables.push(v) });
            return r;
        }
        createDerivationTree() {
            return this.createDerivationTreeSub(this.rules.length - 1);
            //this.startVariable.right.map((v)=>this.createDerivationTreeSub(v));
        }
        createDerivationTreeSub(i) {
            if (typeof (i) == "number") {
                const variable = this.rules[i];
                const children = variable.right.map((v) => this.createDerivationTreeSub(v));
                const r = new GraphTableSVG.LogicTree({ item: variable.name, children: children, vertexText: variable.name });
                return r;
            }
            else {
                const r = new GraphTableSVG.LogicTree({ item: i, children: [], vertexText: i });
                return r;
            }
        }
        toString(i) {
            const rule = this.rules[i];
            return `${rule.name} → ${rule.right.map((v) => typeof (v) == "number" ? this.rules[v].name : v).join("")}`;
        }
        createRuleArray() {
            return this.rules.map((v, i) => [this.toString(i)]);
        }
        static parse(text) {
            const r = new CFG();
            const rules = text.split("\n");
            const dic = {};
            rules.forEach((v, i) => {
                const rule = v.split(",");
                let name = "";
                rule.forEach((w, j) => {
                    const w2 = w.trim();
                    if (j == 0) {
                        name = w2;
                        dic[name] = [i, []];
                    }
                    else {
                        dic[name][1].push(w2);
                    }
                });
            });
            r.rules = new Array(rules.length);
            Object.keys(dic).forEach((key) => {
                const val = dic[key]; // this は obj
                const element = document.getElementById(key);
                const right = val[1].map((v) => { return v in dic ? dic[v][0] : v; });
                const rule = new Rule(key, right);
                r.rules[val[0]] = rule;
            }, dic);
            return r;
        }
        getRule(right) {
            for (let i = 0; i < this.rules.length; i++) {
                if (this.rules[i].equals(right))
                    return i;
            }
            return null;
        }
    }
    Grammar.CFG = CFG;
    class Rule {
        constructor(name, right = []) {
            this.name = name;
            this.right = right;
        }
        copy() {
            return new Rule(this.name, this.right.map((v) => v));
        }
        equals(right) {
            if (this.right.length == right.length) {
                for (let i = 0; i < this.right.length; i++) {
                    if (this.right[i] != right[i])
                        return false;
                }
                return true;
            }
            else {
                return false;
            }
        }
    }
    Grammar.Rule = Rule;
    /*
    export class SLP2Dictionary{
        public cfg : CFG;
        public dic : { [key: string]: number; }
        public constructor(cfg : CFG){

        }
    }
    */
})(Grammar || (Grammar = {}));
var Grammar;
(function (Grammar) {
    class SLPViewer {
        //private _idVariableDic: { [key: number]: number; } = [];
        constructor(svg, r = 30, tableClass = null, graphClass = null) {
            this.tableClass = null;
            this.graphClass = null;
            this.r = r;
            this.svg = svg;
            this.tableClass = tableClass;
            this.graphClass = graphClass;
            this.graph = new GraphTableSVG.GGraph(this.svg, { class: this.graphClass });
            this.graph.relocateAttribute = "Grammar.SLPViewer.relocateFunction";
            this.table = new GraphTableSVG.GTable(this.svg, { class: this.tableClass });
            this.table.setSize(1, 1);
            /*
            this.graph = new GraphTableSVG.Graph(svg, this.graphClass);
            this.table = new GraphTableSVG.Table(svg, 1, 1, this.tableClass);
            this.slp = slp;
            */
            //this.create(slp);
            //this.locate();
            //this.graph.update();
        }
        //graphOffsetY : number = 300;
        //graphOffsetX : number = 50;
        //private _nodeXInterval: number = 50;
        //private _nodeYInterval: number = 50;
        get nodeXInterval() {
            if (this.graph.vertexXInterval == null)
                this.graph.vertexXInterval = 50;
            return this.graph.vertexXInterval;
        }
        set nodeXInterval(value) {
            this.graph.vertexXInterval = value;
            this.locate();
        }
        get nodeYInterval() {
            if (this.graph.vertexYInterval == null)
                this.graph.vertexYInterval = 50;
            return this.graph.vertexYInterval;
        }
        set nodeYInterval(value) {
            this.graph.vertexYInterval = value;
            this.locate();
        }
        static relocateFunction(graph) {
            GraphTableSVG.TreeArrangement.alignVerticeByLeave(graph);
            GraphTableSVG.TreeArrangement.addOffset(graph, 50, 200);
        }
        locate() {
            this.graph.relocate();
            /*
            this.graph.vertices.forEach((v)=>{v.cx = 0 ; v.cy = 0});
            GraphTableSVG.GTreeArrangement.alignVerticeByLeave(this.graph);

            //GraphTableSVG.GTreeArrangement.reverse(this.graph, false, true);
            this.graph.svgGroup.setY(180);
            this.graph.svgGroup.setX(30);
            */
            const rect = this.graph.getRegion();
            this.table.svgGroup.setX(rect.right + 50);
        }
        update() {
            this.locate();
            this.table.update();
            //this.graph.update();
        }
        create(slp) {
            this.slp = slp;
            this.graph.clear();
            this.table.clear();
            this.slp.startVariables.forEach((v, i) => {
                const node = this.createVariable(v, null);
                this.graph.setRootIndex(node, i);
            });
            this.slp.slpNodes.forEach((v) => {
                this.appendInfo(v.value);
            });
            this.locate();
        }
        appendInfo(variable) {
            let str = "";
            const c = this.slp.slpNodes[variable];
            if (c.type == 'nonchar') {
                str = `X_{${c.value + 1}} → X_{${c.left + 1}}X_{${c.right + 1}}`;
            }
            else {
                str = `X_{${c.value + 1}} → ${c.child}`;
            }
            /*
            if (this.table.cells[this.table.height - 1][0].svgText.textContent != "") {
            }
            */
            this.table.appendRow();
            this.table.cells[this.table.rowCount - 1][0].svgText.setTextContent(str, true);
        }
        createNode(variable) {
            //const variableNode = GraphTableSVG.CircleVertex.create(this.graph, this.graph.defaultVertexClass);
            const variableNode = new GraphTableSVG.GEllipse(this.graph.svgGroup, { class: this.graph.defaultVertexClass });
            variableNode.svgText.setAttribute("pointer-events", "none");
            if (typeof (variable) == "string") {
                variableNode.svgGroup.setAttribute("str", variable);
                variableNode.svgText.textContent = `${variable}`;
            }
            else {
                variableNode.svgGroup.setAttribute("variable", variable.toString());
                variableNode.svgText.setTextContent(`X_{${variable + 1}}`, true);
            }
            return variableNode;
        }
        createNodeFunc(node) {
        }
        createVariable(variable, parent, insertIndex = 0) {
            const v = this.slp.slpNodes[variable];
            const variableNode = this.createNode(variable);
            if (parent != null) {
                const edge = new GraphTableSVG.GEdge(this.graph.svgGroup, { class: this.graph.defaultEdgeClass });
                //this.graph.connect(parent, edge, variableNode, insertIndex,null, "bottom", "top");
                this.graph.connect(parent, edge, variableNode, { outcomingInsertIndex: insertIndex });
            }
            else {
                this.graph.setRootIndex(variableNode, this.graph.roots.length);
                //this.graph.roots.push(variableNode);
            }
            if (v.type == "nonchar") {
                this.createVariable(v.left, variableNode, 0);
                this.createVariable(v.right, variableNode, 1);
            }
            else {
                const charNode = this.createNode(v.child);
                //charNode.svgText.textContent = `${v.child}`;
                const edge2 = new GraphTableSVG.GEdge(this.graph.svgGroup, { class: this.graph.defaultEdgeClass });
                //this.graph.connect(variableNode, edge2, charNode, 0, null, "bottom", "top");
                this.graph.connect(variableNode, edge2, charNode, { outcomingInsertIndex: 0 });
                this.createNodeFunc(charNode);
            }
            this.createNodeFunc(variableNode);
            return variableNode;
        }
        connect(i) {
            const node1 = this.graph.roots[i];
            const node2 = this.graph.roots[i + 1];
            const variable1 = Number(node1.svgGroup.getAttribute("variable"));
            const variable2 = Number(node2.svgGroup.getAttribute("variable"));
            const b = this.slp.getVariable(variable1, variable2) == null;
            const variable3 = this.slp.connect(i);
            if (b) {
                this.appendInfo(variable3);
            }
            const newNode = this.createNode(variable3);
            this.graph.setRootIndex(newNode, i);
            //this._idVariableDic[newNode.symbol] = variable3;
            const newEdge1 = new GraphTableSVG.GEdge(this.graph.svgGroup, { class: this.graph.defaultEdgeClass });
            const newEdge2 = new GraphTableSVG.GEdge(this.graph.svgGroup, { class: this.graph.defaultEdgeClass });
            //this.graph.connect(newNode, newEdge1, node1, 0, null, "bottom", "top");
            this.graph.connect(newNode, newEdge1, node1, { outcomingInsertIndex: 0 });
            this.graph.connect(newNode, newEdge2, node2, { outcomingInsertIndex: 1 });
            this.locate();
            //this.graph.update();
            return newNode;
        }
    }
    Grammar.SLPViewer = SLPViewer;
})(Grammar || (Grammar = {}));
var Grammar;
(function (Grammar) {
    class Clicker extends Grammar.SLPViewer {
        constructor(text, svg, r = 30, tableClass = null, nodeClass = null) {
            super(svg, r, tableClass, nodeClass);
            this._firstSelectedNode = null;
            this.rootNodeClass = "root-node";
            this.chosenNodeClass = "chosen-node";
            this.unrootNodeClass = "unroot-node";
            this.isChoosable = true;
            this.history = [];
            this.historyIndex = 0;
            this.click = (x) => {
                if (!this.isChoosable)
                    return;
                const svg = x.currentTarget;
                const id = svg.getAttribute(GraphTableSVG.CustomAttributeNames.objectIDName);
                if (id == null)
                    throw Error("Null Error");
                const node = GraphTableSVG.GObject.getObjectFromObjectID(id);
                if (node instanceof GraphTableSVG.GVertex) {
                    if (node.isNoParent) {
                        const rootIndex = this.graph.roots.indexOf(node);
                        if (this.firstSelectedNode != null) {
                            if (this.firstSelectedNode == node) {
                                this.firstSelectedNode = null;
                            }
                            else {
                                const fstRootIndex = this.graph.roots.indexOf(this.firstSelectedNode);
                                let newNode = null;
                                if (rootIndex + 1 == fstRootIndex) {
                                    newNode = this.connect(rootIndex);
                                }
                                else if (fstRootIndex + 1 == rootIndex) {
                                    newNode = this.connect(fstRootIndex);
                                }
                                else {
                                    this.firstSelectedNode = node;
                                }
                                if (newNode != null) {
                                    this.firstSelectedNode = null;
                                    if (newNode.surface != null)
                                        newNode.surface.setAttribute("class", this.rootNodeClass);
                                    const node1 = newNode.outcomingEdges[0].endVertex;
                                    const node2 = newNode.outcomingEdges[1].endVertex;
                                    if (node1 == null || node2 == null)
                                        throw Error("Null Error");
                                    if (node1.surface != null)
                                        node1.surface.setAttribute("class", this.unrootNodeClass);
                                    if (node2.surface != null)
                                        node2.surface.setAttribute("class", this.unrootNodeClass);
                                    newNode.svgGroup.onclick = this.click;
                                }
                            }
                        }
                        else {
                            this.firstSelectedNode = node;
                        }
                    }
                }
            };
            this.clickCallback = [];
            this.graph.vertices.forEach((v) => {
                v.svgGroup.onclick = this.click;
                if (v.surface != null) {
                    v.surface.setAttribute("class", v.isNoParent ? this.rootNodeClass : this.unrootNodeClass);
                }
            });
            this.create(Clicker.createSLP(text));
            this.history.push(this.slp.copy());
        }
        changeHistory(i) {
            this.firstSelectedNode = null;
            this.create(this.history[i].copy());
            this.historyIndex = i;
        }
        get firstSelectedNode() {
            return this._firstSelectedNode;
        }
        set firstSelectedNode(value) {
            if (this.firstSelectedNode != null) {
                if (this.firstSelectedNode.surface != null) {
                    this.firstSelectedNode.surface.setAttribute("class", this.rootNodeClass);
                }
            }
            this._firstSelectedNode = value;
            if (this._firstSelectedNode != null) {
                if (this._firstSelectedNode.surface != null) {
                    this._firstSelectedNode.surface.setAttribute("class", this.chosenNodeClass);
                }
            }
        }
        static createSLP(text) {
            const slp = new Grammar.SLPDictionary();
            for (let i = 0; i < text.length; i++) {
                const c = slp.addChar(text[i]);
                slp.startVariables.push(c);
            }
            return slp;
        }
        createNodeFunc(node) {
            this.updateNode(node);
            node.svgGroup.onclick = this.click;
        }
        updateNode(node) {
            if (node.surface != null) {
                node.surface.setAttribute("class", node.isNoParent ? this.rootNodeClass : this.unrootNodeClass);
            }
        }
        connect(i) {
            if (this.historyIndex + 1 < this.history.length)
                this.history.splice(this.historyIndex + 1, this.history.length - this.historyIndex - 1);
            const result = super.connect(i);
            result.outcomingEdges.forEach((v) => {
                const w = v.endVertex;
                if (w != null)
                    this.updateNode(w);
            });
            this.updateNode(result);
            this.history.push(this.slp.copy());
            this.historyIndex++;
            this.clickCallback.forEach((v) => v());
            return result;
        }
    }
    Grammar.Clicker = Clicker;
})(Grammar || (Grammar = {}));
var SVGTable = GraphTableSVG.GTable;
var SVGToVBA = GraphTableSVG.SVGToVBA;
var Graph = GraphTableSVG.GGraph;
//const svgBox: HTMLElement;
let graphtables = [];
let clicker;
let svgBox;
function createSLP() {
    svgBox.innerHTML = "";
    const text = GraphTableSVG.GUI.getInputText("textbox");
    clicker = new Grammar.Clicker(text, svgBox, 20, "slp-table", "slp-graph");
    clicker.clickCallback.push(updateHistoryButtons);
    graphtables = [clicker.table, clicker.graph];
    updateHistoryButtons();
}
function changeCSS() {
    clicker.isChoosable = !clicker.isChoosable;
    clicker.graph.vertices.forEach((v) => {
        if (v.surface != null) {
            v.surface.style.visibility = clicker.isChoosable ? "visible" : "hidden";
        }
    });
}
function update() {
    clicker.update();
    alert("CSS updated");
}
window.onload = () => {
    svgBox = GraphTableSVG.GUI.getNonNullElementById('svgbox');
    GraphTableSVG.GUI.observeSVGBox(svgBox, () => GraphTableSVG.Common.getRegion(graphtables), new GraphTableSVG.Padding(30, 30, 30, 30));
    GraphTableSVG.GUI.setURLParametersToHTMLElements();
    createSLP();
    // clicker.graph.relocateAttribute = "GraphTableSVG.GTreeArrangement.alignVerticeByLeave";
    //clicker.graph.relocate();
};
function view(i) {
    clicker.changeHistory(i);
    updateHistoryButtons();
}
function updateHistoryButtons() {
    const nb = document.getElementById('next_button');
    const pb = document.getElementById('prev_button');
    nb.disabled = clicker.historyIndex == clicker.history.length - 1;
    pb.disabled = clicker.historyIndex == 0;
}
function prev() {
    view(clicker.historyIndex - 1);
}
function next() {
    view(clicker.historyIndex + 1);
}
