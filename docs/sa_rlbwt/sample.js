let graphtables = [];
let svgBox;
const saColumn = 0;
const fColumn = 1;
const substringColumn = 2;
const lColumn = 3;
function createTextTable(text, canvas) {
    const textTable = new GraphTableSVG.GTable(canvas, { id: "text-table", columnCount: text.length + 1, rowCount: 2 });
    textTable.cells[0][0].svgText.textContent = "Index";
    textTable.cells[1][0].svgText.textContent = "Text";
    for (let i = 0; i < text.length; i++) {
        textTable.cells[0][i + 1].svgText.textContent = `${i + 1}`;
        textTable.cells[1][i + 1].svgText.textContent = `${text[i]}`;
    }
    return textTable;
}
function constructSuffixArray(str) {
    const arr = new Array(str.length);
    for (let i = 0; i < str.length; i++) {
        arr[i] = i;
    }
    const func = function (item1, item2) {
        for (let i = 0; i <= str.length; i++) {
            if (item1 + i >= str.length || item2 + i >= str.length)
                break;
            if (str.charAt(item1 + i) < str.charAt(item2 + i)) {
                return -1;
            }
            else if (str.charAt(item1 + i) > str.charAt(item2 + i)) {
                return 1;
            }
        }
        if (item1 == item2) {
            return 0;
        }
        else {
            return item1 < item2 ? 1 : -1;
        }
    };
    arr.sort(func);
    return arr;
}
/*
function get(text : string) : string{
    const sa = constructSuffixArray(text);
    const bwt : string = sa.map((x)=> x == 0 ? text[text.length-1] : text[x-1] ).join();
    return bwt;
}
*/
function createBWTTable(text, canvas) {
    const sa = constructSuffixArray(text);
    const table = new GraphTableSVG.GTable(canvas, { id: "bwt-table", columnCount: 4, rowCount: sa.length + 1 });
    table.cells[0][saColumn].svgText.textContent = "SA";
    table.cells[0][fColumn].svgText.textContent = "F";
    table.cells[0][lColumn].svgText.textContent = "L";
    for (let i = 0; i < sa.length; i++) {
        const circular = text.substring(sa[i]) + text.substring(0, sa[i]);
        table.cells[i + 1][fColumn].svgText.textContent = circular[0];
        table.cells[i + 1][lColumn].svgText.textContent = circular[circular.length - 1];
        table.cells[i + 1][substringColumn].svgText.textContent = circular.substring(1, circular.length - 1);
    }
    table.cellArray.forEach((v) => createCharSpan(v.svgText));
    createPartitionsBetweenRuns(text, table);
    return table;
}
function createPartitionsBetweenRuns(text, table) {
    const sa = constructSuffixArray(text);
    const isa = new Array(sa.length);
    sa.forEach((x, i) => isa[x] = i);
    const bwt = sa.map((x) => x == 0 ? text[text.length - 1] : text[x - 1]).join("");
    for (let i = 0; i < sa.length; i++) {
        if (i == 0 || bwt.charCodeAt(i) != bwt.charCodeAt(i - 1)) {
            if (i != 0)
                table.cells[1 + i][lColumn].svgTopBorder.setAttribute("data-partition", "1");
            const fpos = sa[i] == 0 ? isa[text.length - 1] : isa[sa[i] - 1];
            if (fpos != 0)
                table.cells[1 + fpos][fColumn].svgTopBorder.setAttribute("data-partition", "1");
        }
    }
}
function createCharSpan(element) {
    const text = element.textContent;
    element.textContent = null;
    for (let i = 0; i < text.length; i++) {
        const tspan = document.createElementNS('http://www.w3.org/2000/svg', 'tspan');
        tspan.textContent = text[i];
        element.appendChild(tspan);
    }
}
function getTSpan(table, row, i, textLen) {
    if (i == 0) {
        return table.cells[row + 1][fColumn].svgText.children.item(0);
    }
    else if (i == textLen - 1) {
        return table.cells[row + 1][lColumn].svgText.children.item(0);
    }
    else {
        return table.cells[row + 1][substringColumn].svgText.children.item(i - 1);
    }
}
function setCharOccAttribute(bwtTable, textTable, text) {
    const sa = constructSuffixArray(text);
    const counter = {};
    for (let i = 0; i < text.length; i++) {
        counter[text.charCodeAt(i)] = 0;
    }
    for (let i = 0; i < text.length; i++) {
        const j = sa[i];
        const charCode = text.charCodeAt(j);
        const color_count = counter[charCode];
        counter[charCode]++;
        for (let x = 0; x < text.length; x++) {
            const pos = j >= sa[x] ? j - sa[x] : j + text.length - sa[x];
            const tspan = getTSpan(bwtTable, x, pos, text.length);
            tspan.setAttribute("data-occ", color_count.toString());
        }
        textTable.cells[1][1 + sa[i]].svgText.setAttribute("data-occ", color_count.toString());
    }
}
function createDisplay() {
    svgBox.innerHTML = "";
    const text = GraphTableSVG.GUI.getInputText(`textbox`);
    const textTable = createTextTable(text, svgBox);
    const bwtTable = createBWTTable(text, svgBox);
    const textRegion = textTable.getRegion();
    bwtTable.svgGroup.setY(textRegion.height + 30);
    graphtables = [bwtTable, textTable];
    setCharOccAttribute(bwtTable, textTable, text);
    GraphTableSVG.GUI.autostrech(svgBox, graphtables);
}
window.onload = () => {
    svgBox = GraphTableSVG.GUI.getNonNullElementById('svgbox');
    GraphTableSVG.GUI.setURLParametersToHTMLElements();
    createDisplay();
};
