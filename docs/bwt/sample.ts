﻿let graphtables: (GraphTableSVG.GObject)[] = [];
let svgBox: SVGSVGElement;

function createTextTable(text: string, canvas: SVGSVGElement): GraphTableSVG.GTable {
    const textTable = new GraphTableSVG.GTable(canvas, { id : "text-table", columnCount: text.length + 1, rowCount: 2 });
    textTable.cells[0][0].svgText.textContent = "Index";
    textTable.cells[1][0].svgText.textContent = "Text";

    for (let i = 0; i < text.length; i++) {
        textTable.cells[0][i + 1].svgText.textContent = `${i + 1}`;
        textTable.cells[1][i + 1].svgText.textContent = `${text[i]}`;
    }

    return textTable;
}
function constructSuffixArray(str: string): number[] {
    const arr: number[] = new Array(str.length);
    for (let i = 0; i < str.length; i++) {
        arr[i] = i;
    }

    const func = function (item1: number, item2: number): number {
        for (let i = 0; i <= str.length; i++) {
            if (item1 + i >= str.length || item2 + i >= str.length) break;
            if (str.charAt(item1 + i) < str.charAt(item2 + i)) {
                return - 1;
            } else if (str.charAt(item1 + i) > str.charAt(item2 + i)) {
                return 1;
            }
        }
        if (item1 == item2) {
            return 0;
        } else {
            return item1 < item2 ? 1 : -1;
        }
    };
    arr.sort(func);
    return arr;
}
function createBWTTable(text: string, canvas: SVGSVGElement): GraphTableSVG.GTable {
    const sa = constructSuffixArray(text);
    
    const table = new GraphTableSVG.GTable(canvas, { id : "bwt-table", columnCount: 3, rowCount: sa.length + 1 });
    table.cells[0][0].svgText.textContent = "F";
    table.cells[0][2].svgText.textContent = "L";

    for (let i = 0; i < sa.length; i++) {
        const circular = text.substring(sa[i]) + text.substring(0, sa[i]);
        table.cells[i + 1][0].svgText.textContent = circular[0];
        table.cells[i + 1][2].svgText.textContent = circular[circular.length - 1];
        table.cells[i + 1][1].svgText.textContent = circular.substring(1, circular.length - 1);


    }

    table.cellArray.forEach((v) => createCharSpan(v.svgText));

    return table;
}
function createCharSpan(element: SVGTextElement) {
    const text = element.textContent!;
    element.textContent = null;
    for (let i = 0; i < text.length; i++) {
        const tspan: SVGTSpanElement = document.createElementNS('http://www.w3.org/2000/svg', 'tspan');
        tspan.textContent = text[i];
        element.appendChild(tspan);
    }
}
function getTSpan(table: GraphTableSVG.GTable, row: number, i: number, textLen : number): SVGTSpanElement {
    if(i==0){
        return table.cells[row + 1][0].svgText.children.item(0) as SVGTSpanElement;
    }else if(i == textLen-1){
        return table.cells[row + 1][2].svgText.children.item(0) as SVGTSpanElement;
    }else{
        return table.cells[row + 1][1].svgText.children.item(i - 1) as SVGTSpanElement;
    }
}

function setCharOccAttribute(bwtTable: GraphTableSVG.GTable, textTable : GraphTableSVG.GTable, text: string) {
    const sa = constructSuffixArray(text);
    const counter: { [key: number]: number; } = {};
    for (let i = 0; i < text.length; i++) {
        counter[text.charCodeAt(i)] = 0;
    }

    for (let i = 0; i < text.length; i++) {
        const j = sa[i];
        const charCode = text.charCodeAt(j);
        const color_count = counter[charCode];
        counter[charCode]++;
        for (let x = 0; x < text.length; x++) {
            const pos = j >= sa[x] ? j - sa[x] : j + text.length - sa[x];
            const tspan = getTSpan(bwtTable, x, pos, text.length);
            tspan.setAttribute("data-occ", color_count.toString());
        }
        textTable.cells[1][1+sa[i]].svgText.setAttribute("data-occ", color_count.toString());
    }

}
function createDisplay() {
    svgBox.innerHTML = "";
    const text: string = GraphTableSVG.GUI.getInputText(`textbox`);
    const textTable = createTextTable(text, svgBox);

    const bwtTable = createBWTTable(text, svgBox);
    const textRegion = textTable.getRegion();
    bwtTable.svgGroup.setY(textRegion.height + 30);

    graphtables = [bwtTable, textTable];
    setCharOccAttribute(bwtTable, textTable, text);
    GraphTableSVG.GUI.autostrech(svgBox, graphtables);
}

window.onload = () => {
    svgBox = GraphTableSVG.GUI.getNonNullElementById('svgbox') as any;
    GraphTableSVG.GUI.setURLParametersToHTMLElements();
    createDisplay();
};

