﻿import SVGTable = GraphTableSVG.GTable;
import SVGToVBA = GraphTableSVG.SVGToVBA;
import Graph = GraphTableSVG.GGraph;
//const svgBox: HTMLElement;
let graphtables : (SVGTable | Graph)[] = [];
let svgBox : SVGSVGElement;

enum EditType {
    INSERT, DELETE, REPLACE, NONE
}
function constructEditResult(_svgBox : SVGSVGElement, str1 : string, str2 : string, editSequence : EditType[]) : GraphTableSVG.GTable {
    const tb = new GraphTableSVG.LogicTable({columnCount : editSequence.length, rowCount : 3, tableClassName : "sa-table"});
    //const table2 = new SVGTable(_svgBox, "sa-table")
    //table2.setSize(editSequence.length, 3)
    let x =0;
    let y = 0;
    for(let i=0;i<editSequence.length;i++){
        const type = editSequence[i];
        if (type == EditType.REPLACE) {
            tb.cells[1][i].text = "↓";
            tb.cells[0][i].text = str1[y++];
            tb.cells[2][i].text = str2[x++];
        } else if (type == EditType.NONE) {
            tb.cells[1][i].text = " ";
            tb.cells[0][i].text = str1[y++];
            tb.cells[2][i].text = str2[x++];
        } else if (type == EditType.INSERT) {
            tb.cells[0][i].text = " ";
            tb.cells[1][i].text = " ";
            tb.cells[2][i].text = str2[x++];
        } else {
            tb.cells[0][i].text = str1[y++];
            tb.cells[1][i].text = " ";
            tb.cells[2][i].text = " ";
        }
    }
    const table2 = new SVGTable(_svgBox)
    table2.constructFromLogicTable(tb);
    return table2;
}

function constructEditDistanceTableX(_svgBox : SVGSVGElement, str1 : string, str2 : string, edTable : number[][]) : GraphTableSVG.GTable{
    const table = new GraphTableSVG.LogicTable({columnCount : str2.length + 2, rowCount : str1.length + 2, tableClassName : "sa-table"} );
    table.cells[1][0].text = "_";
    table.cells[0][1].text = "_";
    table.getRow(0).forEach((v)=>v.bottomBorderClass = "bold-border");
    table.getColumn(0).forEach((v)=>v.rightBorderClass = "bold-border");

    for (let y = 0; y < str1.length; y++) {
        table.cells[y+2][0].text = str1[y];
    }
    for (let x = 0; x < str2.length; x++) {
        table.cells[0][x+2].text = str2[x];
    }
    
    for (let y = 0; y < edTable.length; y++) {
        for (let x = 0; x < edTable[y].length; x++) {
            table.cells[y+1][x+1].text = edTable[y][x].toString();
        }
    }
    const svgTable = new GraphTableSVG.GTable(_svgBox);
    svgTable.constructFromLogicTable(table);
    return svgTable;
}


function coloring(table : GraphTableSVG.GTable, seq : EditType[]){
    let [x, y] = [1,1];
    table.cells[y][x].svgBackground.style.fill = "pink";
    for(let i=0;i<seq.length;i++){
        const type = seq[i];
        if(type == EditType.NONE){
            x += 2;
            y +=2;
        }else if(type == EditType.REPLACE){
            x += 2;
            y +=2;            
        }else if(type == EditType.INSERT){
            x +=2;
        }else{
            y +=2;
        }
        table.cells[y][x].svgBackground.style.fill = "pink";
    }
}
function computeEditSequence(str1 : string, str2 : string, edTable : number[][]) : EditType[]{
    const seq :EditType[] = [];
    const [edStr1, edStr2] = ["_" + str1, "_" + str2];

    let [x, y] = [edStr2.length-1, edStr1.length-1];
    while(x > 0 || y > 0){
        let now = edTable[y][x];
        const top = y > 0 ? edTable[y-1][x] : Number.MAX_VALUE;
        const left = x > 0 ? edTable[y][x-1] : Number.MAX_VALUE;
        const upperLeft = x > 0 && y > 0 ? edTable[y-1][x-1] : Number.MAX_VALUE;

        if(upperLeft + 1 == now && edStr1[y] != edStr2[x]){
            seq.push(EditType.REPLACE);            
            x--;
            y--;
        }else if((upperLeft == now && edStr1[y] == edStr2[x])){
            seq.push(EditType.NONE);
            x--;
            y--;
        }else if(left + 1 == now){
            seq.push(EditType.INSERT);
            x--;
        }else{
            seq.push(EditType.DELETE);
            y--;
        }
    }
    return seq.reverse();
}
function constructEditDistanceTable2(svgBox : SVGSVGElement,str1 : string, str2 : string, edTable : number[][]) : GraphTableSVG.GTable {
    const tb = new GraphTableSVG.LogicTable({ columnCount : (str2.length + 1) * 2, rowCount : (str1.length + 1) * 2, tableClassName : "sa-table" });

    //table.setSize((str2.length + 1) * 2, (str1.length + 1) * 2)
    tb.cells[1][0].text = "_";
    tb.cells[0][1].text = "_";

    tb.getColumn(0).forEach((v)=>v.rightBorderClass = "bold-border");
    tb.getRow(0).forEach((v)=>v.bottomBorderClass = "bold-border");


    for (let y = 0; y < str1.length; y++) {
        tb.cells[2*(y+1) + 1][0].text = str1[y];
    }
    for (let x = 0; x < str2.length; x++) {
        tb.cells[0][2*(x+1) + 1].text = str2[x];
    }
    
    const width = str2.length + 1;
    const height = str1.length+1;
    const edStr1 = "_" + str1;
    const edStr2 = "_" + str2;

    for (let y = 0; y < edStr1.length; y++) {
        for (let x = 0; x < edStr2.length; x++) {
            const [cellX, cellY] = [(2*x)+1, (2*y)+1];
            const cell = tb.cells[cellY][cellX];
            cell.text = edTable[y][x].toString();            
            const now = edTable[y][x];
            const bottom = y + 1 < height ? edTable[y+1][x] : Number.MAX_VALUE;            
            const right = x + 1 < width ? edTable[y][x+1] : Number.MAX_VALUE;
            const lowerRight = y + 1 < height && x + 1 < width ? edTable[y+1][x+1] : Number.MAX_VALUE;
            if(now + 1 == bottom && cellY + 1 < tb.rowCount) {                
                tb.cells[cellY+1][cellX].text = "↓";
            }
            if(now + 1 == right && cellX + 1 != tb.columnCount){
                tb.cells[cellY][cellX+1].text = "→";                
            }
            if((now + 1 == lowerRight && edStr1[y+1] != edStr2[x+1]) || now == lowerRight && edStr1[y+1] == edStr2[x+1]){
                if(cellX + 1 < width && cellY + 1 < height){
                    tb.cells[cellY+1][cellX+1].text = "＼";                
                }
            }
        }
    }
    for(let y = 2;y < tb.rowCount;y+=2){
        tb.getRow(y).forEach((v)=>v.textClass = "small");
        tb.rowHeights[y] = 12;
    }
    for(let x = 2;x < tb.columnCount;x+=2){
        tb.getColumn(x).forEach((v)=>v.textClass = "small");
        tb.columnWidths[x] = 12;
    }
    const table = new SVGTable(svgBox);
    table.constructFromLogicTable(tb);
    return table;    
}

function construct(){
    svgBox.innerHTML = "";
    const str1: string = (<HTMLTextAreaElement>document.getElementById(`textbox1`)).value;
    const str2: string = (<HTMLTextAreaElement>document.getElementById(`textbox2`)).value;

    //const [table, editSequence] = constructEditDistanceTable(svgBox,str1, str2);
    const edTable = StringModule.computeEditDisutanceTable(str1, str2);
    const table = constructEditDistanceTable2(svgBox, str1, str2, edTable);
    const editSeq = computeEditSequence(str1, str2, edTable);
    const table2 = constructEditResult(svgBox, str1, str2, editSeq);
    coloring(table, editSeq);

    table.update();
    table2.update();

    table2.svgGroup.setX(0)
    table2.svgGroup.setY(0)

    table.svgGroup.setX(0);    
    table.svgGroup.setY(table2.getRegion().height + 50);

    graphtables = [table, table2]


    GraphTableSVG.GUI.setSVGBoxSize(svgBox, GraphTableSVG.Common.getRegion(graphtables), new GraphTableSVG.Padding(5,5,5,5));
}
function construct2(){
    svgBox.innerHTML = "";
    const str1: string = (<HTMLTextAreaElement>document.getElementById(`textbox1`)).value;
    const str2: string = (<HTMLTextAreaElement>document.getElementById(`textbox2`)).value;

    //const [table, editSequence] = constructEditDistanceTable(svgBox,str1, str2);
    const edTable = StringModule.computeEditDisutanceTable(str1, str2);
    const table = constructEditDistanceTableX(svgBox, str1, str2, edTable);

    table.update();

    table.svgGroup.setX(0);    
    table.svgGroup.setY(0);

    graphtables = [table]

    GraphTableSVG.GUI.setSVGBoxSize(svgBox, GraphTableSVG.Common.getRegion(graphtables), new GraphTableSVG.Padding(5,5,5,5));
}

window.onload = () => {    
    svgBox = <any>GraphTableSVG.GUI.getNonNullElementById('svgbox');    
    GraphTableSVG.GUI.setURLParametersToHTMLElements();    
    construct2();

};
