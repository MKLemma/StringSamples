var StringModule;
(function (StringModule) {
    function bunrui(strings) {
        const p = {};
        for (let i = 0; i < strings.length; i++) {
            const str1 = strings[i];
            const c = str1.charCodeAt(0);
            if (p[c] == null)
                p[c] = new Array(0);
            p[c].push(str1.substring(1));
        }
        return p;
    }
    function getChars(str) {
        const p = {};
        const r = new Array(0);
        let count = 0;
        for (let i = 0; i < str.length; i++) {
            if (p[str.charAt(i)] == null) {
                p[str.charCodeAt(i)] = str.charAt(i);
                count++;
            }
            else {
            }
        }
        for (let c in p) {
            const ch = +c;
            r.push(String.fromCharCode(ch));
        }
        return r;
    }
    function compare(str1, str2) {
        const min = Math.min(str1.length, str2.length);
        for (let i = 0; i <= min; i++) {
            if (str1.charAt(i) < str2.charAt(i)) {
                return [i, -1];
            }
            else if (str1.charAt(i) > str2.charAt(i)) {
                return [i, 1];
            }
        }
        if (str1 == str2) {
            return [str1.length, 0];
        }
        else {
            return str1.length < str2.length ? [str1.length, 1] : [str2.length, -1];
        }
    }
    StringModule.compare = compare;
    function computeSuffixArray(str) {
        const arr = new Array(str.length);
        for (let i = 0; i < str.length; i++) {
            arr[i] = i;
        }
        const func = function (item1, item2) {
            for (let i = 0; i <= str.length; i++) {
                if (item1 + i >= str.length || item2 + i >= str.length)
                    break;
                if (str.charAt(item1 + i) < str.charAt(item2 + i)) {
                    return -1;
                }
                else if (str.charAt(item1 + i) > str.charAt(item2 + i)) {
                    return 1;
                }
            }
            if (item1 == item2) {
                return 0;
            }
            else {
                return item1 < item2 ? 1 : -1;
            }
        };
        arr.sort(func);
        return arr;
    }
    StringModule.computeSuffixArray = computeSuffixArray;
    function getSortedSuffixes(str) {
        const arr = computeSuffixArray(str);
        const r = new Array(arr.length);
        for (let i = 0; i < arr.length; i++) {
            r[i] = str.substring(arr[i]);
        }
        return r;
    }
    function computeLCP(str1, str2) {
        const min = str1.length < str2.length ? str1.length : str2.length;
        let lcp = 0;
        for (let i = 0; i < min; i++) {
            if (str1.charAt[i] == str2.charAt[i]) {
                lcp++;
            }
            else {
                break;
            }
        }
        return lcp;
    }
    function computeLCPArray(str, sufarr) {
        const lcparr = new Array(sufarr.length);
        for (let i = 0; i < sufarr.length; i++) {
            if (i == 0 && sufarr.length > 1) {
                lcparr[i] = -1;
            }
            else {
                lcparr[i] = computeLCP(str.substring(sufarr[i]), str.substring(sufarr[i - 1]));
            }
        }
        return lcparr;
    }
    function createAllSuffixes(str) {
        let str2 = "";
        for (let i = 0; i < str.length; i++) {
            if (str[i] != "\n") {
                str2 += str[i];
            }
        }
        const suffixes = new Array(str2.length);
        for (let i = 0; i < suffixes.length; i++) {
            suffixes[i] = str2.substring(i);
        }
        return suffixes;
    }
    StringModule.createAllSuffixes = createAllSuffixes;
    function createAllTruncatedSuffixes(str, truncatedLength) {
        const suffixes = new Array(str.length);
        for (let i = 0; i < suffixes.length; i++) {
            suffixes[i] = str.substr(i, truncatedLength);
        }
        return suffixes;
    }
    StringModule.createAllTruncatedSuffixes = createAllTruncatedSuffixes;
    function removeSpace(str) {
        let r = "";
        const emptyCode = " ".charCodeAt(0);
        for (let i = 0; i < str.length; i++) {
            if (str.charCodeAt(i) != emptyCode) {
                r += str.charAt(i);
            }
        }
        return r;
    }
    StringModule.removeSpace = removeSpace;
    function removeFirstSpaces(str) {
        let i = 0;
        for (i = 0; i < str.length; i++) {
            if (str[i] != " ")
                break;
        }
        if (i == str.length) {
            return "";
        }
        else {
            return str.substring(i);
        }
    }
    function reverse(str) {
        const rv = [];
        for (let i = 0, n = str.length; i < n; i++) {
            rv[i] = str.charAt(n - i - 1);
        }
        return rv.join("");
    }
    StringModule.reverse = reverse;
    function LZ77WithSelfReference(str) {
        const r = new Array(0);
        let startPos = 0;
        let lastRefPos = -1;
        let i = 0;
        while (i < str.length) {
            const substr = str.substr(startPos, i - startPos + 1);
            const reference = i == 0 ? "" : str.substr(0, i);
            const refPos = reference.indexOf(substr);
            if (refPos == -1) {
                if (lastRefPos == -1) {
                    r.push(substr);
                    i++;
                }
                else {
                    r.push([lastRefPos, i - startPos]);
                }
                startPos = i;
                lastRefPos = -1;
            }
            else {
                lastRefPos = refPos;
                i++;
            }
        }
        if (lastRefPos != -1) {
            r.push([lastRefPos, str.length - startPos]);
        }
        return r;
    }
    StringModule.LZ77WithSelfReference = LZ77WithSelfReference;
    function computeEditDisutanceTable(str1, str2) {
        str1 = "_" + str1;
        str2 = "_" + str2;
        const r = new Array(str1.length);
        for (let y = 0; y < r.length; y++) {
            r[y] = new Array(str2.length);
            for (let x = 0; x < r[y].length; x++) {
                r[y][x] = -1;
            }
        }
        for (let y = 0; y < str1.length; y++) {
            r[y][0] = y;
        }
        for (let x = 0; x < str2.length; x++) {
            r[0][x] = x;
        }
        for (let y = 0; y < r.length; y++) {
            for (let x = 0; x < r[y].length; x++) {
                if (r[y][x] == -1) {
                    const top = r[y - 1][x] + 1;
                    const left = r[y][x - 1] + 1;
                    const upperLeft = r[y - 1][x - 1] + (str1[y] == str2[x] ? 0 : 1);
                    r[y][x] = Math.min(top, left, upperLeft);
                }
            }
        }
        return r;
    }
    StringModule.computeEditDisutanceTable = computeEditDisutanceTable;
})(StringModule || (StringModule = {}));
var SVGTable = GraphTableSVG.GTable;
var SVGToVBA = GraphTableSVG.SVGToVBA;
var Graph = GraphTableSVG.GGraph;
//const svgBox: HTMLElement;
let graphtables = [];
let svgBox;
var EditType;
(function (EditType) {
    EditType[EditType["INSERT"] = 0] = "INSERT";
    EditType[EditType["DELETE"] = 1] = "DELETE";
    EditType[EditType["REPLACE"] = 2] = "REPLACE";
    EditType[EditType["NONE"] = 3] = "NONE";
})(EditType || (EditType = {}));
function constructEditResult(_svgBox, str1, str2, editSequence) {
    const tb = new GraphTableSVG.LogicTable({ columnCount: editSequence.length, rowCount: 3, tableClassName: "sa-table" });
    //const table2 = new SVGTable(_svgBox, "sa-table")
    //table2.setSize(editSequence.length, 3)
    let x = 0;
    let y = 0;
    for (let i = 0; i < editSequence.length; i++) {
        const type = editSequence[i];
        if (type == EditType.REPLACE) {
            tb.cells[1][i].text = "↓";
            tb.cells[0][i].text = str1[y++];
            tb.cells[2][i].text = str2[x++];
        }
        else if (type == EditType.NONE) {
            tb.cells[1][i].text = " ";
            tb.cells[0][i].text = str1[y++];
            tb.cells[2][i].text = str2[x++];
        }
        else if (type == EditType.INSERT) {
            tb.cells[0][i].text = " ";
            tb.cells[1][i].text = " ";
            tb.cells[2][i].text = str2[x++];
        }
        else {
            tb.cells[0][i].text = str1[y++];
            tb.cells[1][i].text = " ";
            tb.cells[2][i].text = " ";
        }
    }
    const table2 = new SVGTable(_svgBox);
    table2.constructFromLogicTable(tb);
    return table2;
}
function constructEditDistanceTableX(_svgBox, str1, str2, edTable) {
    const table = new GraphTableSVG.LogicTable({ columnCount: str2.length + 2, rowCount: str1.length + 2, tableClassName: "sa-table" });
    table.cells[1][0].text = "_";
    table.cells[0][1].text = "_";
    table.getRow(0).forEach((v) => v.bottomBorderClass = "bold-border");
    table.getColumn(0).forEach((v) => v.rightBorderClass = "bold-border");
    for (let y = 0; y < str1.length; y++) {
        table.cells[y + 2][0].text = str1[y];
    }
    for (let x = 0; x < str2.length; x++) {
        table.cells[0][x + 2].text = str2[x];
    }
    for (let y = 0; y < edTable.length; y++) {
        for (let x = 0; x < edTable[y].length; x++) {
            table.cells[y + 1][x + 1].text = edTable[y][x].toString();
        }
    }
    const svgTable = new GraphTableSVG.GTable(_svgBox);
    svgTable.constructFromLogicTable(table);
    return svgTable;
}
function coloring(table, seq) {
    let [x, y] = [1, 1];
    table.cells[y][x].svgBackground.style.fill = "pink";
    for (let i = 0; i < seq.length; i++) {
        const type = seq[i];
        if (type == EditType.NONE) {
            x += 2;
            y += 2;
        }
        else if (type == EditType.REPLACE) {
            x += 2;
            y += 2;
        }
        else if (type == EditType.INSERT) {
            x += 2;
        }
        else {
            y += 2;
        }
        table.cells[y][x].svgBackground.style.fill = "pink";
    }
}
function computeEditSequence(str1, str2, edTable) {
    const seq = [];
    const [edStr1, edStr2] = ["_" + str1, "_" + str2];
    let [x, y] = [edStr2.length - 1, edStr1.length - 1];
    while (x > 0 || y > 0) {
        let now = edTable[y][x];
        const top = y > 0 ? edTable[y - 1][x] : Number.MAX_VALUE;
        const left = x > 0 ? edTable[y][x - 1] : Number.MAX_VALUE;
        const upperLeft = x > 0 && y > 0 ? edTable[y - 1][x - 1] : Number.MAX_VALUE;
        if (upperLeft + 1 == now && edStr1[y] != edStr2[x]) {
            seq.push(EditType.REPLACE);
            x--;
            y--;
        }
        else if ((upperLeft == now && edStr1[y] == edStr2[x])) {
            seq.push(EditType.NONE);
            x--;
            y--;
        }
        else if (left + 1 == now) {
            seq.push(EditType.INSERT);
            x--;
        }
        else {
            seq.push(EditType.DELETE);
            y--;
        }
    }
    return seq.reverse();
}
function constructEditDistanceTable2(svgBox, str1, str2, edTable) {
    const tb = new GraphTableSVG.LogicTable({ columnCount: (str2.length + 1) * 2, rowCount: (str1.length + 1) * 2, tableClassName: "sa-table" });
    //table.setSize((str2.length + 1) * 2, (str1.length + 1) * 2)
    tb.cells[1][0].text = "_";
    tb.cells[0][1].text = "_";
    tb.getColumn(0).forEach((v) => v.rightBorderClass = "bold-border");
    tb.getRow(0).forEach((v) => v.bottomBorderClass = "bold-border");
    for (let y = 0; y < str1.length; y++) {
        tb.cells[2 * (y + 1) + 1][0].text = str1[y];
    }
    for (let x = 0; x < str2.length; x++) {
        tb.cells[0][2 * (x + 1) + 1].text = str2[x];
    }
    const width = str2.length + 1;
    const height = str1.length + 1;
    const edStr1 = "_" + str1;
    const edStr2 = "_" + str2;
    for (let y = 0; y < edStr1.length; y++) {
        for (let x = 0; x < edStr2.length; x++) {
            const [cellX, cellY] = [(2 * x) + 1, (2 * y) + 1];
            const cell = tb.cells[cellY][cellX];
            cell.text = edTable[y][x].toString();
            const now = edTable[y][x];
            const bottom = y + 1 < height ? edTable[y + 1][x] : Number.MAX_VALUE;
            const right = x + 1 < width ? edTable[y][x + 1] : Number.MAX_VALUE;
            const lowerRight = y + 1 < height && x + 1 < width ? edTable[y + 1][x + 1] : Number.MAX_VALUE;
            if (now + 1 == bottom && cellY + 1 < tb.rowCount) {
                tb.cells[cellY + 1][cellX].text = "↓";
            }
            if (now + 1 == right && cellX + 1 != tb.columnCount) {
                tb.cells[cellY][cellX + 1].text = "→";
            }
            if ((now + 1 == lowerRight && edStr1[y + 1] != edStr2[x + 1]) || now == lowerRight && edStr1[y + 1] == edStr2[x + 1]) {
                if (cellX + 1 < width && cellY + 1 < height) {
                    tb.cells[cellY + 1][cellX + 1].text = "＼";
                }
            }
        }
    }
    for (let y = 2; y < tb.rowCount; y += 2) {
        tb.getRow(y).forEach((v) => v.textClass = "small");
        tb.rowHeights[y] = 12;
    }
    for (let x = 2; x < tb.columnCount; x += 2) {
        tb.getColumn(x).forEach((v) => v.textClass = "small");
        tb.columnWidths[x] = 12;
    }
    const table = new SVGTable(svgBox);
    table.constructFromLogicTable(tb);
    return table;
}
function construct() {
    svgBox.innerHTML = "";
    const str1 = document.getElementById(`textbox1`).value;
    const str2 = document.getElementById(`textbox2`).value;
    //const [table, editSequence] = constructEditDistanceTable(svgBox,str1, str2);
    const edTable = StringModule.computeEditDisutanceTable(str1, str2);
    const table = constructEditDistanceTable2(svgBox, str1, str2, edTable);
    const editSeq = computeEditSequence(str1, str2, edTable);
    const table2 = constructEditResult(svgBox, str1, str2, editSeq);
    coloring(table, editSeq);
    table.update();
    table2.update();
    table2.svgGroup.setX(0);
    table2.svgGroup.setY(0);
    table.svgGroup.setX(0);
    table.svgGroup.setY(table2.getRegion().height + 50);
    graphtables = [table, table2];
    GraphTableSVG.GUI.setSVGBoxSize(svgBox, GraphTableSVG.Common.getRegion(graphtables), new GraphTableSVG.Padding(5, 5, 5, 5));
}
function construct2() {
    svgBox.innerHTML = "";
    const str1 = document.getElementById(`textbox1`).value;
    const str2 = document.getElementById(`textbox2`).value;
    //const [table, editSequence] = constructEditDistanceTable(svgBox,str1, str2);
    const edTable = StringModule.computeEditDisutanceTable(str1, str2);
    const table = constructEditDistanceTableX(svgBox, str1, str2, edTable);
    table.update();
    table.svgGroup.setX(0);
    table.svgGroup.setY(0);
    graphtables = [table];
    GraphTableSVG.GUI.setSVGBoxSize(svgBox, GraphTableSVG.Common.getRegion(graphtables), new GraphTableSVG.Padding(5, 5, 5, 5));
}
window.onload = () => {
    svgBox = GraphTableSVG.GUI.getNonNullElementById('svgbox');
    GraphTableSVG.GUI.setURLParametersToHTMLElements();
    construct2();
};
