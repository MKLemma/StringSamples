﻿import SVGTable = GraphTableSVG.GTable;
import SVGToVBA = GraphTableSVG.SVGToVBA;
import Graph = GraphTableSVG.GGraph;

//const svgBox: HTMLElement;
let graphtables: (SVGTable | Graph)[] = [];
let svgBox: SVGSVGElement;

function createIBSTree(items: [number, number][]): GraphTableSVG.BinaryLogicTree {
    const i = getMiddleItemIndex(items);
    const tree = new GraphTableSVG.BinaryLogicTree(items[i], null, null, `[${items[i][0]}, ${items[i][1]}]`, null);
    if (i > 0) {
        const leftArr = items.slice(0, i);
        tree.left = createIBSTree(leftArr);
    }
    if (i < (items.length - 1)) {
        const rightArr = items.slice(i + 1, items.length);
        tree.right = createIBSTree(rightArr);
    }
    return tree;
}

function createNormalTree() {
    svgBox.innerHTML = "";

    //const graph = new GraphTableSVG.Graph(svgBox);
    const items = inputParse();
    const pTree = createIBSTree(items);
    const tree = new GraphTableSVG.GGraph(svgBox, {class : "graph"});
    tree.relocateAttribute = "GraphTableSVG.TreeArrangement.standardTreeWidthArrangement";
    tree.constructFromLogicTree([pTree]);
    //tree.update();
    tree.svgGroup.setX(Math.abs(tree.getRegion().x) + 30);
    tree.svgGroup.setY(Math.abs(tree.getRegion().y) + 30);
    graphtables = [tree];
}
function createBlockTree() {
    svgBox.innerHTML = "";

    
    const items = inputParse();
    const pTree = createIBSTree(items);
    const tree = new GraphTableSVG.GGraph(svgBox);
    if (items.length > 0) {
        createBlockNode(tree, null, pTree, false, 1);
    }    
    //tree.update();
    tree.svgGroup.setX(Math.abs(tree.getRegion().x) + 30);
    tree.svgGroup.setY(Math.abs(tree.getRegion().y) + 30);
    graphtables = [tree];
}

function createBlockNode(graph: Graph, parent: GraphTableSVG.GVertex | null, pTree: GraphTableSVG.BinaryLogicTree, isLeft: boolean, depth: number) {

    const nodeArray: GraphTableSVG.GVertex[] = [];
    const width = pTree.item[1] - pTree.item[0];
    for (let i = pTree.item[0]; i < pTree.item[1]; i++) {

        const node = <GraphTableSVG.GVertex>GraphTableSVG.createShape(graph, GraphTableSVG.ShapeObjectType.Rect);
        node.cy = depth * 50;
        node.cx = i * 15;
        node.width = 15;
        node.height = 15;
        const j = i + 1;
        if (node.surface == null) throw Error("Null Error");
        node.surface.style.fill = j % 100 == 0 ? "red" :
            j % 50 == 0 ? "yellow" :
                j % 10 == 0 ? "aqua" : "white";
        nodeArray.push(node);
    }
    if (parent != null) {
        const edge = <GraphTableSVG.GEdge>GraphTableSVG.createShape(graph, GraphTableSVG.ShapeObjectType.Edge);
        if (isLeft) {
            parent.insertOutcomingEdge(edge, 0);
        } else {
            parent.insertOutcomingEdge(edge, parent.outcomingEdges.length);
        }
        nodeArray[0].insertIncomingEdge(edge, 0);
        edge.beginConnectorType = GraphTableSVG.ConnectorPosition.Bottom;
        edge.endConnectorType = GraphTableSVG.ConnectorPosition.Top;
    } else {
        //graph = nodeArray[0];
    }

    if (pTree.left != null) createBlockNode(graph, nodeArray[0], pTree.left, true, depth + 1);
    if (pTree.right != null) createBlockNode(graph, nodeArray[0], pTree.right, false, depth + 1);
}



/*
function createNode(graph: Graph, parent: GraphTableSVG.Vertex | null, pTree: GraphTableSVG.BinaryLogicTree<[number, number]>, isLeft: boolean) {


    const node = GraphTableSVG.Vertex.create(graph, null, "rectangle");

    if (parent != null) {
        const edge = GraphTableSVG.Edge.create(graph, null);
        if (isLeft) {
            parent.insertOutcomingEdge(edge, 0);
        } else {
            parent.insertOutcomingEdge(edge, parent.outcomingEdges.length);
        }
        node.insertIncomingEdge(edge, 0);
        edge.beginConnectorType = GraphTableSVG.ConnectorPosition.Bottom;
        edge.endConnectorType = GraphTableSVG.ConnectorPosition.Top;
    } else {
        graph.rootVertex = node;
    }

    GraphTableSVG.SVG.setTextToSVGText(node.svgText, `[${pTree.item[0]}, ${pTree.item[1]}]`, true);
    node.isAutoSizeShapeToFitText = true;

    if (pTree.left != null) {
        createNode(graph, node, pTree.left, true);
    }
    if (pTree.right != null) {
        createNode(graph, node, pTree.right, false);
    }
}
*/
function getMiddleItemIndex(items: [number, number][]): number {
    if (items.length == 0) {
        throw new Error("Error Input Length");
    } else {
        const min = items[0][0];
        const max = items[items.length - 1][1];
        const middle = ((max + min) / 2);
        for (let i = 0; i < items.length; i++) {
            if (items[i][0] <= middle && middle <= items[i][1]) {
                return i;
            }
        }
        throw new Error("Error Input Values");
    }
}
function getMaxItemIndex(items: [number, number][]): number {
    if (items.length == 0) {
        throw new Error("Error Input Length");
    } else {
        let maxInterval = 0;
        let x = 0;
        for (let i = 0; i < items.length; i++) {
            const interval = items[i][1] - items[i][0] + 1;
            if (interval > maxInterval) {
                x = i;
                maxInterval = interval;
            }
        }
        return x;
    }
}

function inputParse(): [number, number][] {
    const text = (<HTMLTextAreaElement>document.getElementById("textbox")).value;
    let prev = 0;
    //const r = new Array(0);
    const r: [number, number][] = text.split(",").map((v) => parseInt(v)).map((v) => {
        const p1: number = prev;
        const p2: number = v;
        prev = v;
        const p3: [number, number] = [p1, p2];
        return p3;
    });
    return r;

}

function update() {
}

window.onload = () => {
    svgBox = <any>GraphTableSVG.GUI.getNonNullElementById('svgbox');
    GraphTableSVG.GUI.observeSVGBox(svgBox, () => GraphTableSVG.Common.getRegion(graphtables), new GraphTableSVG.Padding(10,10,10,10));
    GraphTableSVG.GUI.setURLParametersToHTMLElements();
    createBlockTree();

};