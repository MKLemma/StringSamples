﻿import SVGTable = GraphTableSVG.GTable;
import SVGToVBA = GraphTableSVG.SVGToVBA;
import Graph = GraphTableSVG.GGraph;
let graphtables: (SVGTable | Graph)[] = [];
let history2 : Grammar.HistoryDic;
let viewer : Grammar.CFGViewer;
//let grammarIndex = 0;
let svgBox : SVGSVGElement;
function createRepair() {
    //clear();

    const text = (<HTMLTextAreaElement>document.getElementById("textbox")).value;    
    //const r = new Grammar.RepairCompressor(text);
    viewer = new Grammar.CFGViewer(svgBox, "slp-table", "slp-graph");
    history2 = new Grammar.HistoryDic(viewer);
    history2.history = Grammar.RepairCompressor.createRepairHistory(text);
    graphtables = [viewer.table, viewer.tree];

    /*
    repairGrammars.push(r.slp.copy());

    while(r.slp.startVariables.length > 1){
        r.repair();
        repairGrammars.push(r.slp.copy());
    }
    */

    view(history2.history.length-1);
    
}
function view(i : number){
    history2.changeHistory(i);    
    console.log(history2.history[i]);
    const nb = <HTMLButtonElement>document.getElementById('next_button');
    const pb = <HTMLButtonElement>document.getElementById('prev_button');
    nb.disabled = i == history2.history.length-1;
    pb.disabled = i == 0;
}
function prev(){
    view(history2.historyIndex-1);
}
function next(){
    view(history2.historyIndex+1);    
}

window.onload = () => {
    svgBox = <any>GraphTableSVG.GUI.getNonNullElementById('svgbox');
    //const svgBox = document.getElementById('svgbox');

    GraphTableSVG.GUI.observeSVGBox(svgBox, () => GraphTableSVG.Common.getRegion(graphtables));
    GraphTableSVG.GUI.setURLParametersToHTMLElements();

    createRepair();
};