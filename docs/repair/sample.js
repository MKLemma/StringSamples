var Grammar;
(function (Grammar) {
    class CFG {
        constructor() {
            this.rules = [];
        }
        get startVariable() {
            return this.rules[this.rules.length - 1];
        }
        //startVariables: (number | string)[] = [];
        copy() {
            const r = new CFG();
            this.rules.forEach((v) => { r.rules.push(v.copy()); });
            //this.startVariable.right.forEach((v) => { r.startVariables.push(v) });
            return r;
        }
        createDerivationTree() {
            return this.createDerivationTreeSub(this.rules.length - 1);
            //this.startVariable.right.map((v)=>this.createDerivationTreeSub(v));
        }
        createDerivationTreeSub(i) {
            if (typeof (i) == "number") {
                const variable = this.rules[i];
                const children = variable.right.map((v) => this.createDerivationTreeSub(v));
                const r = new GraphTableSVG.LogicTree({ item: variable.name, children: children, vertexText: variable.name });
                return r;
            }
            else {
                const r = new GraphTableSVG.LogicTree({ item: i, children: [], vertexText: i });
                return r;
            }
        }
        toString(i) {
            const rule = this.rules[i];
            return `${rule.name} → ${rule.right.map((v) => typeof (v) == "number" ? this.rules[v].name : v).join("")}`;
        }
        createRuleArray() {
            return this.rules.map((v, i) => [this.toString(i)]);
        }
        static parse(text) {
            const r = new CFG();
            const rules = text.split("\n");
            const dic = {};
            rules.forEach((v, i) => {
                const rule = v.split(",");
                let name = "";
                rule.forEach((w, j) => {
                    const w2 = w.trim();
                    if (j == 0) {
                        name = w2;
                        dic[name] = [i, []];
                    }
                    else {
                        dic[name][1].push(w2);
                    }
                });
            });
            r.rules = new Array(rules.length);
            Object.keys(dic).forEach((key) => {
                const val = dic[key]; // this は obj
                const element = document.getElementById(key);
                const right = val[1].map((v) => { return v in dic ? dic[v][0] : v; });
                const rule = new Rule(key, right);
                r.rules[val[0]] = rule;
            }, dic);
            return r;
        }
        getRule(right) {
            for (let i = 0; i < this.rules.length; i++) {
                if (this.rules[i].equals(right))
                    return i;
            }
            return null;
        }
    }
    Grammar.CFG = CFG;
    class Rule {
        constructor(name, right = []) {
            this.name = name;
            this.right = right;
        }
        copy() {
            return new Rule(this.name, this.right.map((v) => v));
        }
        equals(right) {
            if (this.right.length == right.length) {
                for (let i = 0; i < this.right.length; i++) {
                    if (this.right[i] != right[i])
                        return false;
                }
                return true;
            }
            else {
                return false;
            }
        }
    }
    Grammar.Rule = Rule;
    /*
    export class SLP2Dictionary{
        public cfg : CFG;
        public dic : { [key: string]: number; }
        public constructor(cfg : CFG){

        }
    }
    */
})(Grammar || (Grammar = {}));
var Grammar;
(function (Grammar) {
    class CFGViewer {
        constructor(svg, tableClass = null, graphClass = null) {
            this.tree = new GraphTableSVG.GGraph(svg, { class: graphClass });
            this.table = new GraphTableSVG.GTable(svg, { class: tableClass });
            this.table.setSize(1, 1);
            if (this.tree.vertexXInterval == null)
                this.tree.vertexXInterval = 50;
            if (this.tree.vertexYInterval == null)
                this.tree.vertexYInterval = 50;
            if (graphClass == null) {
                GraphTableSVG.CustomAttributeNames.defaultCircleRadius = 15;
            }
            this.tree.relocateAttribute = "Grammar.CFGViewer.relocateFunction";
        }
        static relocateFunction(graph) {
            GraphTableSVG.TreeArrangement.alignVerticeByLeave(graph);
            GraphTableSVG.TreeArrangement.addOffset(graph, 50, 200);
        }
        update() {
            //this.locate();
            this.table.update();
            //this.tree.update();
            this.tree.relocate();
            const rect = this.tree.getRegion();
            this.table.svgGroup.setX(rect.right + 50);
        }
        create(cfg) {
            this.dic = cfg;
            this.tree.clear();
            this.table.clear();
            this.tree.constructFromLogicTree([this.dic.createDerivationTree()], { isLatexMode: true });
            this.table.construct(this.dic.createRuleArray(), { isLatexMode: true });
            this.update();
        }
    }
    Grammar.CFGViewer = CFGViewer;
    class HistoryDic {
        constructor(viewer) {
            this.history = [];
            this.historyIndex = 0;
            this.viewer = viewer;
        }
        changeHistory(i) {
            this.viewer.create(this.history[i].copy());
            this.historyIndex = i;
        }
        add(dic) {
            if (this.historyIndex + 1 < this.history.length)
                this.history.splice(this.historyIndex + 1, this.history.length - this.historyIndex - 1);
            this.history.push(dic.copy());
            this.historyIndex++;
        }
        prev() {
            this.changeHistory(this.historyIndex - 1);
        }
        get canPrev() {
            return this.historyIndex > 0;
        }
        succ() {
            this.changeHistory(this.historyIndex + 1);
        }
        get canSucc() {
            return this.historyIndex + 1 < this.history.length;
        }
    }
    Grammar.HistoryDic = HistoryDic;
})(Grammar || (Grammar = {}));
var Grammar;
(function (Grammar) {
    ;
    ;
    class SLPDictionary {
        constructor() {
            this.slpNodes = [];
            this.startVariables = [];
        }
        //private _outcomingEdgesDic: { [key: number]: Edge[]; } = [];
        addVariable(left, right) {
            const result = this.getVariable(left, right);
            if (result == null) {
                const newNumber = this.slpNodes.length;
                const node = { type: "nonchar", value: newNumber, left: left, right: right };
                this.slpNodes.push(node);
                return node.value;
            }
            else {
                return result.value;
            }
        }
        connect(i) {
            const left = this.startVariables[i];
            const right = this.startVariables[i + 1];
            const newSig = this.addVariable(left, right);
            this.startVariables.splice(i, 2, newSig);
            return newSig;
        }
        addChar(char) {
            const result = this.getChar(char);
            if (result == null) {
                const newNumber = this.slpNodes.length;
                const node = { type: "char", value: newNumber, child: char };
                this.slpNodes.push(node);
                return node.value;
            }
            else {
                return result.value;
            }
        }
        getVariable(left, right) {
            for (let i = 0; i < this.slpNodes.length; i++) {
                const p = this.slpNodes[i];
                if (p.type == "nonchar") {
                    if (p.left == left && p.right == right) {
                        return p;
                    }
                }
            }
            return null;
        }
        getChar(child) {
            for (let i = 0; i < this.slpNodes.length; i++) {
                const p = this.slpNodes[i];
                if (p.type == "char") {
                    if (p.child == child) {
                        return p;
                    }
                }
            }
            return null;
        }
        getTextCharvariables(variable = null) {
            if (variable != null) {
                const p = this.slpNodes[variable];
                if (p.type == "char") {
                    return [p];
                }
                else {
                    const left = this.getTextCharvariables(p.left);
                    const right = this.getTextCharvariables(p.right);
                    right.forEach((v) => left.push(v));
                    return left;
                }
            }
            else {
                const r = [];
                const roots = this.startVariables.map((v) => this.getTextCharvariables(v)).forEach((v) => {
                    v.forEach((w) => {
                        r.push(w);
                    });
                });
                return r;
            }
        }
        get text() {
            let r = "";
            this.getTextCharvariables().forEach((v) => { r += v.child; });
            return r;
        }
        copy() {
            const r = new SLPDictionary();
            this.slpNodes.forEach((v) => { r.slpNodes.push(v); });
            this.startVariables.forEach((v) => { r.startVariables.push(v); });
            return r;
        }
        toCFG() {
            const r = new Grammar.CFG();
            r.rules = this.slpNodes.map((v, i) => {
                if (v.type == "char") {
                    return new Grammar.Rule(`X_{${i + 1}}`, [v.child]);
                }
                else {
                    return new Grammar.Rule(`X_{${i + 1}}`, [v.left, v.right]);
                }
            });
            if (this.startVariables.length != 1) {
                r.rules.push(new Grammar.Rule(`S`, this.startVariables.map((v) => v)));
            }
            return r;
        }
    }
    Grammar.SLPDictionary = SLPDictionary;
})(Grammar || (Grammar = {}));
var Grammar;
(function (Grammar) {
    class SLPViewer {
        //private _idVariableDic: { [key: number]: number; } = [];
        constructor(svg, r = 30, tableClass = null, graphClass = null) {
            this.tableClass = null;
            this.graphClass = null;
            this.r = r;
            this.svg = svg;
            this.tableClass = tableClass;
            this.graphClass = graphClass;
            this.graph = new GraphTableSVG.GGraph(this.svg, { class: this.graphClass });
            this.graph.relocateAttribute = "Grammar.SLPViewer.relocateFunction";
            this.table = new GraphTableSVG.GTable(this.svg, { class: this.tableClass });
            this.table.setSize(1, 1);
            /*
            this.graph = new GraphTableSVG.Graph(svg, this.graphClass);
            this.table = new GraphTableSVG.Table(svg, 1, 1, this.tableClass);
            this.slp = slp;
            */
            //this.create(slp);
            //this.locate();
            //this.graph.update();
        }
        //graphOffsetY : number = 300;
        //graphOffsetX : number = 50;
        //private _nodeXInterval: number = 50;
        //private _nodeYInterval: number = 50;
        get nodeXInterval() {
            if (this.graph.vertexXInterval == null)
                this.graph.vertexXInterval = 50;
            return this.graph.vertexXInterval;
        }
        set nodeXInterval(value) {
            this.graph.vertexXInterval = value;
            this.locate();
        }
        get nodeYInterval() {
            if (this.graph.vertexYInterval == null)
                this.graph.vertexYInterval = 50;
            return this.graph.vertexYInterval;
        }
        set nodeYInterval(value) {
            this.graph.vertexYInterval = value;
            this.locate();
        }
        static relocateFunction(graph) {
            GraphTableSVG.TreeArrangement.alignVerticeByLeave(graph);
            GraphTableSVG.TreeArrangement.addOffset(graph, 50, 200);
        }
        locate() {
            this.graph.relocate();
            /*
            this.graph.vertices.forEach((v)=>{v.cx = 0 ; v.cy = 0});
            GraphTableSVG.GTreeArrangement.alignVerticeByLeave(this.graph);

            //GraphTableSVG.GTreeArrangement.reverse(this.graph, false, true);
            this.graph.svgGroup.setY(180);
            this.graph.svgGroup.setX(30);
            */
            const rect = this.graph.getRegion();
            this.table.svgGroup.setX(rect.right + 50);
        }
        update() {
            this.locate();
            this.table.update();
            //this.graph.update();
        }
        create(slp) {
            this.slp = slp;
            this.graph.clear();
            this.table.clear();
            this.slp.startVariables.forEach((v, i) => {
                const node = this.createVariable(v, null);
                this.graph.setRootIndex(node, i);
            });
            this.slp.slpNodes.forEach((v) => {
                this.appendInfo(v.value);
            });
            this.locate();
        }
        appendInfo(variable) {
            let str = "";
            const c = this.slp.slpNodes[variable];
            if (c.type == 'nonchar') {
                str = `X_{${c.value + 1}} → X_{${c.left + 1}}X_{${c.right + 1}}`;
            }
            else {
                str = `X_{${c.value + 1}} → ${c.child}`;
            }
            /*
            if (this.table.cells[this.table.height - 1][0].svgText.textContent != "") {
            }
            */
            this.table.appendRow();
            this.table.cells[this.table.rowCount - 1][0].svgText.setTextContent(str, true);
        }
        createNode(variable) {
            //const variableNode = GraphTableSVG.CircleVertex.create(this.graph, this.graph.defaultVertexClass);
            const variableNode = new GraphTableSVG.GEllipse(this.graph.svgGroup, { class: this.graph.defaultVertexClass });
            variableNode.svgText.setAttribute("pointer-events", "none");
            if (typeof (variable) == "string") {
                variableNode.svgGroup.setAttribute("str", variable);
                variableNode.svgText.textContent = `${variable}`;
            }
            else {
                variableNode.svgGroup.setAttribute("variable", variable.toString());
                variableNode.svgText.setTextContent(`X_{${variable + 1}}`, true);
            }
            return variableNode;
        }
        createNodeFunc(node) {
        }
        createVariable(variable, parent, insertIndex = 0) {
            const v = this.slp.slpNodes[variable];
            const variableNode = this.createNode(variable);
            if (parent != null) {
                const edge = new GraphTableSVG.GEdge(this.graph.svgGroup, { class: this.graph.defaultEdgeClass });
                //this.graph.connect(parent, edge, variableNode, insertIndex,null, "bottom", "top");
                this.graph.connect(parent, edge, variableNode, { outcomingInsertIndex: insertIndex });
            }
            else {
                this.graph.setRootIndex(variableNode, this.graph.roots.length);
                //this.graph.roots.push(variableNode);
            }
            if (v.type == "nonchar") {
                this.createVariable(v.left, variableNode, 0);
                this.createVariable(v.right, variableNode, 1);
            }
            else {
                const charNode = this.createNode(v.child);
                //charNode.svgText.textContent = `${v.child}`;
                const edge2 = new GraphTableSVG.GEdge(this.graph.svgGroup, { class: this.graph.defaultEdgeClass });
                //this.graph.connect(variableNode, edge2, charNode, 0, null, "bottom", "top");
                this.graph.connect(variableNode, edge2, charNode, { outcomingInsertIndex: 0 });
                this.createNodeFunc(charNode);
            }
            this.createNodeFunc(variableNode);
            return variableNode;
        }
        connect(i) {
            const node1 = this.graph.roots[i];
            const node2 = this.graph.roots[i + 1];
            const variable1 = Number(node1.svgGroup.getAttribute("variable"));
            const variable2 = Number(node2.svgGroup.getAttribute("variable"));
            const b = this.slp.getVariable(variable1, variable2) == null;
            const variable3 = this.slp.connect(i);
            if (b) {
                this.appendInfo(variable3);
            }
            const newNode = this.createNode(variable3);
            this.graph.setRootIndex(newNode, i);
            //this._idVariableDic[newNode.symbol] = variable3;
            const newEdge1 = new GraphTableSVG.GEdge(this.graph.svgGroup, { class: this.graph.defaultEdgeClass });
            const newEdge2 = new GraphTableSVG.GEdge(this.graph.svgGroup, { class: this.graph.defaultEdgeClass });
            //this.graph.connect(newNode, newEdge1, node1, 0, null, "bottom", "top");
            this.graph.connect(newNode, newEdge1, node1, { outcomingInsertIndex: 0 });
            this.graph.connect(newNode, newEdge2, node2, { outcomingInsertIndex: 1 });
            this.locate();
            //this.graph.update();
            return newNode;
        }
    }
    Grammar.SLPViewer = SLPViewer;
})(Grammar || (Grammar = {}));
var Grammar;
(function (Grammar) {
    class VariablePair {
        constructor(left, right) {
            this.left = left;
            this.right = right;
        }
        toHash() {
            return `${this.left}/${this.right}`;
        }
    }
    ;
    class RepairCompressor {
        constructor(text) {
            this.occurrenceDic = {};
            this.slp = new Grammar.SLPDictionary();
            for (let i = 0; i < text.length; i++) {
                const v = this.slp.addChar(text[i]);
                this.slp.startVariables.push(v);
            }
        }
        collect() {
            this.occurrenceDic = {};
            for (let i = 0; i < this.slp.startVariables.length - 1; i++) {
                const pair = new VariablePair(this.slp.startVariables[i], this.slp.startVariables[i + 1]);
                const b = pair.left == pair.right;
                const str = pair.toHash();
                if (!(str in this.occurrenceDic))
                    this.occurrenceDic[str] = [];
                this.occurrenceDic[str].push(i);
                if (b && i + 1 < this.slp.startVariables.length - 1) {
                    const nextpair = new VariablePair(this.slp.startVariables[i + 1], this.slp.startVariables[i + 2]);
                    if (nextpair.left == nextpair.right) {
                        i++;
                    }
                }
            }
        }
        compress() {
            while (this.slp.startVariables.length > 1) {
                this.repair();
            }
        }
        repair() {
            this.collect();
            let i = 0;
            let max = 0;
            for (let key in this.occurrenceDic) {
                const size = this.occurrenceDic[key].length;
                if (size > max) {
                    max = size;
                    i = this.occurrenceDic[key][0];
                }
            }
            const pair = new VariablePair(this.slp.startVariables[i], this.slp.startVariables[i + 1]);
            this.replace(pair);
        }
        replace(pair) {
            const arr = this.occurrenceDic[pair.toHash()];
            const newNumber = this.slp.addVariable(pair.left, pair.right);
            for (let i = arr.length - 1; i >= 0; i--) {
                const x = arr[i];
                this.slp.startVariables.splice(x, 2, newNumber);
            }
        }
        static createRepairHistory(text) {
            const r = new Grammar.RepairCompressor(text);
            const grammars = [];
            grammars.push(r.slp.toCFG());
            while (r.slp.startVariables.length > 1) {
                r.repair();
                grammars.push(r.slp.toCFG());
            }
            return grammars;
        }
    }
    Grammar.RepairCompressor = RepairCompressor;
})(Grammar || (Grammar = {}));
var SVGTable = GraphTableSVG.GTable;
var SVGToVBA = GraphTableSVG.SVGToVBA;
var Graph = GraphTableSVG.GGraph;
let graphtables = [];
let history2;
let viewer;
//let grammarIndex = 0;
let svgBox;
function createRepair() {
    //clear();
    const text = document.getElementById("textbox").value;
    //const r = new Grammar.RepairCompressor(text);
    viewer = new Grammar.CFGViewer(svgBox, "slp-table", "slp-graph");
    history2 = new Grammar.HistoryDic(viewer);
    history2.history = Grammar.RepairCompressor.createRepairHistory(text);
    graphtables = [viewer.table, viewer.tree];
    /*
    repairGrammars.push(r.slp.copy());

    while(r.slp.startVariables.length > 1){
        r.repair();
        repairGrammars.push(r.slp.copy());
    }
    */
    view(history2.history.length - 1);
}
function view(i) {
    history2.changeHistory(i);
    console.log(history2.history[i]);
    const nb = document.getElementById('next_button');
    const pb = document.getElementById('prev_button');
    nb.disabled = i == history2.history.length - 1;
    pb.disabled = i == 0;
}
function prev() {
    view(history2.historyIndex - 1);
}
function next() {
    view(history2.historyIndex + 1);
}
window.onload = () => {
    svgBox = GraphTableSVG.GUI.getNonNullElementById('svgbox');
    //const svgBox = document.getElementById('svgbox');
    GraphTableSVG.GUI.observeSVGBox(svgBox, () => GraphTableSVG.Common.getRegion(graphtables));
    GraphTableSVG.GUI.setURLParametersToHTMLElements();
    createRepair();
};
