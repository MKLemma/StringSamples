var StringModule;
(function (StringModule) {
    function bunrui(strings) {
        const p = {};
        for (let i = 0; i < strings.length; i++) {
            const str1 = strings[i];
            const c = str1.charCodeAt(0);
            if (p[c] == null)
                p[c] = new Array(0);
            p[c].push(str1.substring(1));
        }
        return p;
    }
    function getChars(str) {
        const p = {};
        const r = new Array(0);
        let count = 0;
        for (let i = 0; i < str.length; i++) {
            if (p[str.charAt(i)] == null) {
                p[str.charCodeAt(i)] = str.charAt(i);
                count++;
            }
            else {
            }
        }
        for (let c in p) {
            const ch = +c;
            r.push(String.fromCharCode(ch));
        }
        return r;
    }
    function compare(str1, str2) {
        const min = Math.min(str1.length, str2.length);
        for (let i = 0; i <= min; i++) {
            if (str1.charAt(i) < str2.charAt(i)) {
                return [i, -1];
            }
            else if (str1.charAt(i) > str2.charAt(i)) {
                return [i, 1];
            }
        }
        if (str1 == str2) {
            return [str1.length, 0];
        }
        else {
            return str1.length < str2.length ? [str1.length, 1] : [str2.length, -1];
        }
    }
    StringModule.compare = compare;
    function computeSuffixArray(str) {
        const arr = new Array(str.length);
        for (let i = 0; i < str.length; i++) {
            arr[i] = i;
        }
        const func = function (item1, item2) {
            for (let i = 0; i <= str.length; i++) {
                if (item1 + i >= str.length || item2 + i >= str.length)
                    break;
                if (str.charAt(item1 + i) < str.charAt(item2 + i)) {
                    return -1;
                }
                else if (str.charAt(item1 + i) > str.charAt(item2 + i)) {
                    return 1;
                }
            }
            if (item1 == item2) {
                return 0;
            }
            else {
                return item1 < item2 ? 1 : -1;
            }
        };
        arr.sort(func);
        return arr;
    }
    StringModule.computeSuffixArray = computeSuffixArray;
    function getSortedSuffixes(str) {
        const arr = computeSuffixArray(str);
        const r = new Array(arr.length);
        for (let i = 0; i < arr.length; i++) {
            r[i] = str.substring(arr[i]);
        }
        return r;
    }
    function computeLCP(str1, str2) {
        const min = str1.length < str2.length ? str1.length : str2.length;
        let lcp = 0;
        for (let i = 0; i < min; i++) {
            if (str1.charAt[i] == str2.charAt[i]) {
                lcp++;
            }
            else {
                break;
            }
        }
        return lcp;
    }
    function computeLCPArray(str, sufarr) {
        const lcparr = new Array(sufarr.length);
        for (let i = 0; i < sufarr.length; i++) {
            if (i == 0 && sufarr.length > 1) {
                lcparr[i] = -1;
            }
            else {
                lcparr[i] = computeLCP(str.substring(sufarr[i]), str.substring(sufarr[i - 1]));
            }
        }
        return lcparr;
    }
    function createAllSuffixes(str) {
        let str2 = "";
        for (let i = 0; i < str.length; i++) {
            if (str[i] != "\n") {
                str2 += str[i];
            }
        }
        const suffixes = new Array(str2.length);
        for (let i = 0; i < suffixes.length; i++) {
            suffixes[i] = str2.substring(i);
        }
        return suffixes;
    }
    StringModule.createAllSuffixes = createAllSuffixes;
    function createAllTruncatedSuffixes(str, truncatedLength) {
        const suffixes = new Array(str.length);
        for (let i = 0; i < suffixes.length; i++) {
            suffixes[i] = str.substr(i, truncatedLength);
        }
        return suffixes;
    }
    StringModule.createAllTruncatedSuffixes = createAllTruncatedSuffixes;
    function removeSpace(str) {
        let r = "";
        const emptyCode = " ".charCodeAt(0);
        for (let i = 0; i < str.length; i++) {
            if (str.charCodeAt(i) != emptyCode) {
                r += str.charAt(i);
            }
        }
        return r;
    }
    StringModule.removeSpace = removeSpace;
    function removeFirstSpaces(str) {
        let i = 0;
        for (i = 0; i < str.length; i++) {
            if (str[i] != " ")
                break;
        }
        if (i == str.length) {
            return "";
        }
        else {
            return str.substring(i);
        }
    }
    function reverse(str) {
        const rv = [];
        for (let i = 0, n = str.length; i < n; i++) {
            rv[i] = str.charAt(n - i - 1);
        }
        return rv.join("");
    }
    StringModule.reverse = reverse;
    function LZ77WithSelfReference(str) {
        const r = new Array(0);
        let startPos = 0;
        let lastRefPos = -1;
        let i = 0;
        while (i < str.length) {
            const substr = str.substr(startPos, i - startPos + 1);
            const reference = i == 0 ? "" : str.substr(0, i);
            const refPos = reference.indexOf(substr);
            if (refPos == -1) {
                if (lastRefPos == -1) {
                    r.push(substr);
                    i++;
                }
                else {
                    r.push([lastRefPos, i - startPos]);
                }
                startPos = i;
                lastRefPos = -1;
            }
            else {
                lastRefPos = refPos;
                i++;
            }
        }
        if (lastRefPos != -1) {
            r.push([lastRefPos, str.length - startPos]);
        }
        return r;
    }
    StringModule.LZ77WithSelfReference = LZ77WithSelfReference;
    function computeEditDisutanceTable(str1, str2) {
        str1 = "_" + str1;
        str2 = "_" + str2;
        const r = new Array(str1.length);
        for (let y = 0; y < r.length; y++) {
            r[y] = new Array(str2.length);
            for (let x = 0; x < r[y].length; x++) {
                r[y][x] = -1;
            }
        }
        for (let y = 0; y < str1.length; y++) {
            r[y][0] = y;
        }
        for (let x = 0; x < str2.length; x++) {
            r[0][x] = x;
        }
        for (let y = 0; y < r.length; y++) {
            for (let x = 0; x < r[y].length; x++) {
                if (r[y][x] == -1) {
                    const top = r[y - 1][x] + 1;
                    const left = r[y][x - 1] + 1;
                    const upperLeft = r[y - 1][x - 1] + (str1[y] == str2[x] ? 0 : 1);
                    r[y][x] = Math.min(top, left, upperLeft);
                }
            }
        }
        return r;
    }
    StringModule.computeEditDisutanceTable = computeEditDisutanceTable;
})(StringModule || (StringModule = {}));
var TreeFunctions;
(function (TreeFunctions) {
    class TreeNode {
        constructor() {
            this.children = [];
            this.edgeText = "";
            this.nodeText = "";
            this.parent = null;
            this._id = TreeNode.counter++;
            this.tag = "";
        }
        get id() {
            return this._id;
        }
        get path() {
            if (this.parent == null) {
                return this.edgeText;
            }
            else {
                return this.parent.path + this.edgeText;
            }
        }
        get isRoot() {
            return this.parent == null;
        }
        get isLeaf() {
            return this.children.length == 0;
        }
        getNodes() {
            const r = [this];
            if (this.isLeaf) {
                return r;
            }
            else {
                this.children.forEach(function (x, i, arr) {
                    x.getNodes().forEach(function (y, j, arr2) {
                        r.push(y);
                    });
                });
                return r;
            }
        }
        addLeaf(insertIndex, str) {
            const newNode = new TreeNode();
            newNode.parent = this;
            newNode.edgeText = str;
            this.children.splice(insertIndex, 0, newNode);
            return newNode;
        }
        split(splitPosition) {
            const pref = this.edgeText.substr(0, splitPosition);
            const suf = this.edgeText.substr(splitPosition);
            if (this.parent != null) {
                const newNode = new TreeNode();
                const i = this.parent.children.indexOf(this);
                this.parent.children[i] = newNode;
                newNode.children.push(this);
                this.parent = newNode;
                newNode.edgeText = pref;
                this.edgeText = suf;
                return newNode;
            }
            else {
                return this;
            }
        }
        locus(pattern) {
            //const matchLen = 0;
            if (pattern.length == 0)
                return [this, 0];
            const [matchLen, comp] = StringModule.compare(this.edgeText, pattern);
            if (matchLen == this.edgeText.length && this.edgeText.length < pattern.length) {
                const edges = this.children.map(function (x, i, arr) { return x.edgeText.charCodeAt(0); });
                const suf = pattern.substr(matchLen);
                const [i, b] = getInsertIndex(edges, suf.charCodeAt(0));
                if (b) {
                    return this.children[i].locus(suf);
                }
                else {
                    return [this, matchLen];
                }
            }
            else {
                return [this, matchLen];
            }
        }
        toLogicTree() {
            const node = new GraphTableSVG.LogicTree({ item: this, children: [], vertexText: this.nodeText, parentEdgeText: this.edgeText });
            this.children.forEach((v) => node.children.push(v.toLogicTree()));
            return node;
        }
    }
    TreeNode.counter = 0;
    TreeFunctions.TreeNode = TreeNode;
    function addString(node, pattern) {
        if (pattern.length == 0)
            return node;
        const edges = node.children.map(function (x, i, arr) { return x.edgeText.charCodeAt(0); });
        const [i, isMatch] = getInsertIndex(edges, pattern.charCodeAt(0));
        if (!isMatch) {
            node.addLeaf(i, pattern[0]);
        }
        return addString(node.children[i], pattern.substr(1));
    }
    TreeFunctions.addString = addString;
    function getInsertIndex(texts, pattern) {
        for (let i = 0; i < texts.length; i++) {
            if (pattern < texts[i]) {
            }
            else if (pattern == texts[i]) {
                return [i, true];
            }
            else {
                return [i, false];
            }
        }
        return [texts.length, false];
    }
    TreeFunctions.getInsertIndex = getInsertIndex;
    function shrink(root) {
        if (root.parent != null) {
            if (root.children.length == 1) {
                const child = root.children[0];
                const i = root.parent.children.indexOf(root);
                root.parent.children[i] = child;
                child.parent = root.parent;
                child.edgeText = root.edgeText + child.edgeText;
                shrink(child);
            }
            else {
                root.children.forEach(function (x) { shrink(x); });
            }
        }
        else {
            root.children.forEach(function (x) { shrink(x); });
        }
    }
    TreeFunctions.shrink = shrink;
    /*
    function createNode(treeNode: TreeNode, graph: GraphTableSVG.Graph): GraphTableSVG.Vertex {
        const node = GraphTableSVG.CircleVertex.create(graph);
        node.svgText.textContent = treeNode.id.toString();
        treeNode.tag = node.objectID;
        return node;

        
    }
    */
})(TreeFunctions || (TreeFunctions = {}));
var TreeFunctions;
(function (TreeFunctions) {
    function createTrie(text) {
        const root = new TreeFunctions.TreeNode();
        for (let i = 0; i < text.length; i++) {
            TreeFunctions.addString(root, text.substr(i));
            //root.addString(text.substr(i));
        }
        return root;
    }
    TreeFunctions.createTrie = createTrie;
    function createSuffixTree(text) {
        const root = new TreeFunctions.TreeNode();
        for (let i = 0; i < text.length; i++) {
            TreeFunctions.addString(root, text.substr(i));
            //root.addString(text.substr(i));
        }
        TreeFunctions.shrink(root);
        return root;
    }
    TreeFunctions.createSuffixTree = createSuffixTree;
    /*
    function addString1(node: TreeNode, pattern: string): TreeNode {
        if (pattern.length == 0) return node;
        const edges = node.children.map(function (x, i, arr) { return x.edgeText.charCodeAt(0); });
        const [i, isMatch] = getInsertIndex(edges, pattern.charCodeAt(0));
        if (!isMatch) {
            return node.addLeaf(i, pattern[0]);
        } else {
            return addString1(node.children[i], pattern.substr(1));
        }

    }
    */
})(TreeFunctions || (TreeFunctions = {}));
var SVGTable = GraphTableSVG.GTable;
var SVGToVBA = GraphTableSVG.SVGToVBA;
var Graph = GraphTableSVG.GGraph;
//const svgBox: HTMLElement;
let graphtables = [];
let saTable;
let svgBox;
function createSuffixTrieOrTree(isSuffixTree) {
    svgBox.innerHTML = "";
    const text = GraphTableSVG.GUI.getInputText(`textbox`);
    const graph = new GraphTableSVG.GGraph(svgBox, { class: "graph" });
    if (graph.vertexXInterval == null)
        graph.vertexXInterval = 90;
    if (graph.vertexYInterval == null)
        graph.vertexYInterval = 90;
    const trie = TreeFunctions.createTrie(text).toLogicTree();
    if (isSuffixTree) {
        trie.getOrderedNodes(GraphTableSVG.VertexOrder.Preorder).forEach((v) => {
            if (v.item.isRoot || v.item.isLeaf || v.item.children.length > 1) {
            }
            else {
                v.vertexClass = "implicit-vertex";
            }
        });
    }
    graph.relocateAttribute = "GraphTableSVG.TreeArrangement.alignVerticeByChildren";
    graph.constructFromLogicTree(trie);
    if (graph.rootVertex != null) {
        const tree = graph.rootVertex.tree;
        tree.setRectangleLocation(0, 0);
    }
    //graph.update();
    graphtables = [graph];
    const rect = graph.getRegion();
    GraphTableSVG.GUI.setSVGBoxSize(svgBox, GraphTableSVG.Common.getRegion(graphtables), new GraphTableSVG.Padding(30, 30, 30, 30));
}
function createSuffixTrie() {
    createSuffixTrieOrTree(false);
}
function createSuffixTree() {
    createSuffixTrieOrTree(true);
}
window.onload = () => {
    svgBox = GraphTableSVG.GUI.getNonNullElementById('svgbox');
    GraphTableSVG.GUI.setURLParametersToHTMLElements();
    createSuffixTrie();
};
