﻿import SVGTable = GraphTableSVG.GTable;
import SVGToVBA = GraphTableSVG.SVGToVBA;
import Graph = GraphTableSVG.GGraph;
//const svgBox: HTMLElement;
let graphtables: GraphTableSVG.VBAObjectType[] = [];
let saTable : SVGTable;
let svgBox : SVGSVGElement;

function createSuffixTrieOrTree(isSuffixTree : boolean) {
    svgBox.innerHTML = "";
    const text = GraphTableSVG.GUI.getInputText(`textbox`);
    const graph = new GraphTableSVG.GGraph(svgBox, {class : "graph"});
    if(graph.vertexXInterval == null) graph.vertexXInterval = 90;
    if(graph.vertexYInterval == null) graph.vertexYInterval = 90;

    const trie = TreeFunctions.createTrie(text).toLogicTree();
    if(isSuffixTree){
    trie.getOrderedNodes(GraphTableSVG.VertexOrder.Preorder).forEach((v)=> {
        if(v.item.isRoot || v.item.isLeaf || v.item.children.length > 1){

        }else{
            v.vertexClass = "implicit-vertex"
        }
    })
    }
    graph.relocateAttribute = "GraphTableSVG.TreeArrangement.alignVerticeByChildren";
    graph.constructFromLogicTree(trie);
    if(graph.rootVertex != null){
        const tree = graph.rootVertex.tree;
        tree.setRectangleLocation(0, 0);
    }
    //graph.update();

    graphtables = [graph];
    const rect = graph.getRegion();
    GraphTableSVG.GUI.setSVGBoxSize(svgBox, GraphTableSVG.Common.getRegion(graphtables), new GraphTableSVG.Padding(30,30,30,30));
}

function createSuffixTrie() {
    createSuffixTrieOrTree(false);
}
function createSuffixTree(){
    createSuffixTrieOrTree(true);

}

window.onload = () => {
    svgBox = <any>GraphTableSVG.GUI.getNonNullElementById('svgbox');
    GraphTableSVG.GUI.setURLParametersToHTMLElements();

    createSuffixTrie();
    

};

