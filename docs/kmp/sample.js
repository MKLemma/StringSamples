var SVGTable = GraphTableSVG.GTable;
var SVGToVBA = GraphTableSVG.SVGToVBA;
var Graph = GraphTableSVG.GGraph;
let svgBox;
let graphtables = [];
function createKMPArray(str) {
    const arr = new Array(str.length);
    for (let i = 0; i < str.length; i++) {
        arr[i] = getBorder(str.substring(0, i + 1));
    }
    return arr;
}
function getBorder(str) {
    for (var i = 1; i < str.length; i++) {
        const suf = str.substr(i);
        const pref = str.substr(0, str.length - i);
        if (suf == pref) {
            return str.length - i;
        }
    }
    return 0;
}
function createBorderTable(str, box, tableClass = null) {
    var borderArray = createKMPArray(str);
    var table = new GraphTableSVG.GTable(box, { class: tableClass });
    table.setSize(borderArray.length, 3);
    for (let i = 0; i < borderArray.length; i++) {
        table.cells[0][i].svgText.textContent = (i + 1).toString();
        table.cells[1][i].svgText.textContent = str[i];
        table.cells[2][i].svgText.textContent = (borderArray[i]).toString();
    }
    return table;
}
function createKMPGraph(str, box, graphClass = null) {
    var arr = createKMPArray(str);
    console.log(arr);
    var graph = new GraphTableSVG.GGraph(box, { class: graphClass });
    let x = 150;
    let y = 200;
    const bottomNode = GraphTableSVG.createVertex(graph);
    bottomNode.cx = x - 100;
    bottomNode.cy = y + 100;
    const startNode = GraphTableSVG.createVertex(graph);
    startNode.cx = x;
    startNode.cy = y;
    for (let i = 0; i < str.length; i++) {
        const node = GraphTableSVG.createVertex(graph);
        x += 100;
        node.cx = x;
        node.cy = y;
    }
    for (let i = 0; i < graph.vertices.length - 1; i++) {
        const edge = GraphTableSVG.createShape(graph, GraphTableSVG.ShapeObjectType.Edge);
        edge.beginVertex = graph.vertices[i];
        edge.endVertex = graph.vertices[i + 1];
        const char = i == 0 ? "Σ" : str[i - 1];
        edge.svgTextPath.setTextContent(char);
        edge.markerEnd = GraphTableSVG.GEdge.createEndMarker();
        edge.pathTextAlignment = GraphTableSVG.pathTextAlighnment.regularInterval;
    }
    for (let i = 0; i < str.length; i++) {
        const nodeIndex = i + 2;
        const failureIndex = arr[i] + 1;
        const failure = GraphTableSVG.createShape(graph, GraphTableSVG.ShapeObjectType.Edge);
        failure.beginVertex = graph.vertices[nodeIndex];
        failure.endVertex = graph.vertices[failureIndex];
        var middle = (graph.vertices[i].x + graph.vertices[failureIndex].x) / 2;
        failure.controlPoint = [[middle, 0]];
        //failure.svgTextPath.setTextContent("aiueokakik");
        failure.pathTextAlignment = GraphTableSVG.pathTextAlighnment.regularInterval;
        failure.svgPath.style.stroke = "black";
        failure.markerEnd = GraphTableSVG.GEdge.createEndMarker();
        failure.svgPath.style.strokeDasharray = "5,5";
        failure.beginConnectorType = GraphTableSVG.ConnectorPosition.TopLeft;
        failure.endConnectorType = GraphTableSVG.ConnectorPosition.TopRight;
    }
    //graph.update();
    return graph;
}
function create() {
    svgBox.innerHTML = "";
    const text = document.getElementById("textbox").value;
    const table = createBorderTable(text, svgBox, null);
    const graph = createKMPGraph(text, svgBox, null);
    graphtables = [table, graph];
}
function update() {
}
window.onload = () => {
    svgBox = GraphTableSVG.GUI.getNonNullElementById('svgbox');
    GraphTableSVG.GUI.observeSVGBox(svgBox, () => GraphTableSVG.Common.getRegion(graphtables));
    GraphTableSVG.GUI.setURLParametersToHTMLElements();
    create();
};
