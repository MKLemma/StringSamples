var SVGTable = GraphTableSVG.GTable;
var SVGToVBA = GraphTableSVG.SVGToVBA;
var Graph = GraphTableSVG.GGraph;
/// <reference path="../../Samples/src/data_structures/tree_viewer.ts"/>
//const svgBox: HTMLElement;
let graphtables = [];
let tree;
let svgBox;
function createdNodeCallback(e) {
    const obj = GraphTableSVG.GObject.getObjectFromObjectID(e.target);
    if (obj instanceof GraphTableSVG.GVertex) {
        obj.svgGroup.onclick = clickEvent;
    }
}
function relocate(tree) {
    GraphTableSVG.TreeArrangement.alignVerticeByChildren(tree);
    if (tree.rootVertex == null) {
    }
    else {
        tree.svgGroup.setX(-tree.rootVertex.tree.region().x + 50);
        tree.svgGroup.setY(Math.abs(50));
    }
}
function clickEvent(ev) {
    const p = this;
    const id = p.getAttribute("data-objectID");
    if (id == null)
        throw Error("Null Error");
    const obj = GraphTableSVG.GObject.getObjectFromObjectID(id);
    if (obj instanceof GraphTableSVG.GVertex) {
        tree.appendChild(obj, null, { insertIndex: 0 });
    }
    updateTreeForm();
    //tree.rootVertex.tree.setRootLocation(0, 0);
}
function create() {
    svgBox.innerHTML = "";
    const text = document.getElementById("textbox1").value;
    const pureTree = GraphTableSVG.Parse.parseTree(text);
    tree = new GraphTableSVG.GGraph(svgBox, { class: "graph" });
    tree.relocateAttribute = "relocate";
    tree.svgGroup.addEventListener(GraphTableSVG.CustomAttributeNames.vertexCreatedEventName, createdNodeCallback);
    tree.constructFromLogicTree([pureTree]);
    graphtables = [tree];
}
function updateTreeForm() {
    if (tree.rootVertex != null) {
        const text = GraphTableSVG.Parse.getParseString(tree.rootVertex);
        document.getElementById("textbox1").value = text;
    }
}
function update() {
}
window.onload = () => {
    svgBox = GraphTableSVG.GUI.getNonNullElementById('svgbox');
    const text = document.getElementById("textbox1").value;
    GraphTableSVG.GUI.observeSVGBox(svgBox, () => GraphTableSVG.Common.getRegion(graphtables));
    create();
};
