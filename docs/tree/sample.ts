﻿import SVGTable = GraphTableSVG.GTable;
import SVGToVBA = GraphTableSVG.SVGToVBA;
import Graph = GraphTableSVG.GGraph;
/// <reference path="../../Samples/src/data_structures/tree_viewer.ts"/>
//const svgBox: HTMLElement;
let graphtables: (SVGTable | Graph)[] = [];
let tree : GraphTableSVG.GGraph;
let svgBox : SVGSVGElement;

function createdNodeCallback(e : Event){
    const obj = GraphTableSVG.GObject.getObjectFromObjectID(<SVGElement>e.target);
    if(obj instanceof GraphTableSVG.GVertex){
        obj.svgGroup.onclick = clickEvent;
    }
}
function relocate(tree : GraphTableSVG.GGraph){
    GraphTableSVG.TreeArrangement.alignVerticeByChildren(tree);
    if(tree.rootVertex == null){

    }else{
        tree.svgGroup.setX(-tree.rootVertex.tree.region().x + 50);
        tree.svgGroup.setY(Math.abs(50));    
    }
}

function clickEvent(ev : MouseEvent){
    const p : SVGGElement = this;
    const id = p.getAttribute("data-objectID");
    if(id == null) throw Error("Null Error");
    const obj = GraphTableSVG.GObject.getObjectFromObjectID(id);
    if(obj instanceof GraphTableSVG.GVertex){
        tree.appendChild(obj, null, {insertIndex : 0});
    }
    updateTreeForm();
    //tree.rootVertex.tree.setRootLocation(0, 0);
}
function create(){

    svgBox.innerHTML = "";
    const text = (<HTMLTextAreaElement>document.getElementById("textbox1")).value;
    const pureTree = GraphTableSVG.Parse.parseTree(text);

    tree = new GraphTableSVG.GGraph(svgBox, {class : "graph"});
    tree.relocateAttribute = "relocate";
    tree.svgGroup.addEventListener(GraphTableSVG.CustomAttributeNames.vertexCreatedEventName, createdNodeCallback);

    tree.constructFromLogicTree([pureTree]);
    graphtables = [tree];
}
function updateTreeForm(){
    
    if(tree.rootVertex != null){
        const text = GraphTableSVG.Parse.getParseString(tree.rootVertex);
        (<HTMLTextAreaElement>document.getElementById("textbox1")).value = text;
    }
}


function update(){
}

window.onload = () => {
    svgBox = <any>GraphTableSVG.GUI.getNonNullElementById('svgbox');
    const text = (<HTMLTextAreaElement>document.getElementById("textbox1")).value;

    GraphTableSVG.GUI.observeSVGBox(svgBox, () => GraphTableSVG.Common.getRegion(graphtables));
    create();

};

