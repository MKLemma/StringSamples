﻿let graphtables: GraphTableSVG.VBAObjectType[] = [];
let saTable : GraphTableSVG.GTable;
let svgBox : SVGSVGElement;

function constructSuffixArray(str: string): number[] {
    const arr: number[] = new Array(str.length);
    for (let i = 0; i < str.length; i++) {
        arr[i] = i;
    }

    const func = function (item1: number, item2: number): number {
        for (let i = 0; i <= str.length; i++) {
            if (item1 + i >= str.length || item2 + i >= str.length) break;
            if (str.charAt(item1 + i) < str.charAt(item2 + i)) {
                return - 1;
            } else if (str.charAt(item1 + i) > str.charAt(item2 + i)) {
                return 1;
            }
        }
        if (item1 == item2) {
            return 0;
        } else {
            return item1 < item2 ? 1 : -1;
        }
    };
    arr.sort(func);
    return arr;
}

function createLogicTextTable(text : string) : GraphTableSVG.LogicTable{
    const textTable = new GraphTableSVG.LogicTable({columnCount : text.length+1,rowCount : 2, tableClassName : "sa-table"});

    
    textTable.cells[0][0].set("Index", false, undefined, undefined, "title-table-text");

    textTable.cells[1][0].set("Text", false, undefined, undefined, "title-table-text");
    
    for (let i = 0; i < text.length; i++) {
        textTable.cells[0][i+1].set(`${i+1}`, false, "cell", undefined,"table-text"); 
        textTable.cells[1][i+1].set(`${text[i]}`, false, "cell", undefined, "table-text"); 
    }
    return textTable;
}

function createLogicSuffixArray(text : string) : GraphTableSVG.LogicTable{
    const sa: number[] = constructSuffixArray(text);
    const saTable = new GraphTableSVG.LogicTable({columnCount : 2, rowCount : sa.length + 1, tableClassName : "sa-table"});
    
    saTable.cells[0][0].set("SA", false, "textarea-cell", undefined, "title-table-text");
    saTable.cells[0][1].set("Text", false, "textarea-cell", undefined, "title-table-text");
    for (let i = 0; i < sa.length; i++) {
        const suffix = text.substr(sa[i]);
        saTable.cells[i + 1][0].set((sa[i] + 1).toString(), false, "cell", undefined, "table-text");
        saTable.cells[i + 1][1].set(suffix, false, "textarea-cell", undefined, "table-text");

    }
    return saTable;
}

function createSuffixArrayTable() {

    const text: string = GraphTableSVG.GUI.getInputText("textbox");
    const sa: number[] = constructSuffixArray(text);
    svgBox.innerHTML = "";

    const textLogicTextTable = createLogicTextTable(text);
    const textLogicSATable = createLogicSuffixArray(text);
    const textTable = new GraphTableSVG.GTable(svgBox);
    const saTable = new GraphTableSVG.GTable(svgBox);
    textTable.constructFromLogicTable(textLogicTextTable);
    saTable.constructFromLogicTable(textLogicSATable);

    //textTable.update();
    const textRegion = textTable.getRegion();
    saTable.svgGroup.setY(textRegion.height + 30);

    graphtables = [textTable, saTable];
    //graphtable = table;
    //GraphTableSVG.GUI.setSVGBoxSize(svgBox, Graph.getRegion(graphtables), new GraphTableSVG.Padding(10,10,10,10));


}


window.onload = () => {
    svgBox = GraphTableSVG.GUI.getNonNullElementById('svgbox') as any;
    GraphTableSVG.GUI.setURLParametersToHTMLElements();
    GraphTableSVG.GUI.observeSVGBox(svgBox, () => GraphTableSVG.Common.getRegion(graphtables));
    //GraphTableSVG.openSVG(svgBox);
    createSuffixArrayTable();
    

};

