var SVGTable = GraphTableSVG.GTable;
var SVGToVBA = GraphTableSVG.SVGToVBA;
var Graph = GraphTableSVG.GGraph;
//const svgBox: HTMLElement;
let graphtables = [];
let svgBox;
function parse(text) {
    const r = [];
    text.split(",").forEach((w) => {
        const num = Number.parseInt(w);
        if (num != undefined && !isNaN(num)) {
            r.push(num);
        }
    });
    r.sort((a, b) => {
        if (a < b)
            return -1;
        if (a > b)
            return 1;
        return 0;
    });
    return r;
}
window.onload = () => {
    svgBox = GraphTableSVG.GUI.getNonNullElementById('svgbox');
    const table = new GraphTableSVG.GTable(svgBox);
    const text = document.getElementById("textbox1").value;
    const items = parse(text);
    const logicTable = new GraphTableSVG.LogicTable({ rowCount: 1, columnCount: items.length, tableClassName: "table" });
    items.forEach((v, i) => logicTable.cells[0][i].text = v.toString());
    table.constructFromLogicTable(logicTable);
    graphtables = [table];
    GraphTableSVG.GUI.observeSVGBox(svgBox, () => GraphTableSVG.Common.getRegion(graphtables));
};
