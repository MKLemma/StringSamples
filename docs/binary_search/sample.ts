﻿import SVGTable = GraphTableSVG.GTable;
import SVGToVBA = GraphTableSVG.SVGToVBA;
import Graph = GraphTableSVG.GGraph;
//const svgBox: HTMLElement;
let graphtables : (SVGTable | Graph)[] = [];
let svgBox : SVGSVGElement;
function parse(text : string) : number[] {
    const r : number[] = [];
    text.split(",").forEach((w)=>{
        const num = Number.parseInt(w);
        if(num != undefined && !isNaN(num)){
            r.push(num);
        }
    })
    r.sort((a,b)=>{
        if( a < b ) return -1;
        if( a > b ) return 1;
        return 0;
    });
    return r;
}

window.onload = () => {
    svgBox = <any>GraphTableSVG.GUI.getNonNullElementById('svgbox');
    const table = new GraphTableSVG.GTable(svgBox);
    const text = (<HTMLTextAreaElement>document.getElementById("textbox1")).value;
    const items = parse(text);
    const logicTable = new GraphTableSVG.LogicTable({rowCount : 1, columnCount : items.length, tableClassName : "table"});
    items.forEach((v,i) => logicTable.cells[0][i].text = v.toString());    
    table.constructFromLogicTable(logicTable);
    graphtables = [table];
    GraphTableSVG.GUI.observeSVGBox(svgBox, () => GraphTableSVG.Common.getRegion(graphtables));

};

