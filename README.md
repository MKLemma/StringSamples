
# StringSamples

[lz77](https://mklemma.github.io/StringSamples/lz77/sample.html "lz77")

[Suffix Array](https://mklemma.github.io/StringSamples/suffix_array/sample.html "Suffix Array")

[Repair](https://mklemma.github.io/StringSamples/repair/sample.html "Repair")

[SLP](https://mklemma.github.io/StringSamples/slp/sample.html "SLP")

[Suffix Tree](https://mklemma.github.io/StringSamples/suffix_tree/sample.html "Suffix Tree")


MIT License

Firefox, Chrome, Edgeで動作を確認中。

## その他
jquery(https://jquery.com/)を使っています。
jQuery contextMenu(https://swisnl.github.io/jQuery-contextMenu/)を使っています。