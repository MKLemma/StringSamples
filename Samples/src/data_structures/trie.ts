﻿namespace TreeFunctions {
    export class TreeNode {
        private static counter: number = 0;

        children: TreeNode[] = [];
        edgeText: string = "";
        nodeText: string = "";
        parent: TreeNode | null = null;
        _id: number = TreeNode.counter++;

        tag: string = "";

        get id(): number {
            return this._id;
        }
        
        get path(): string {
            if (this.parent == null) {
                return this.edgeText;
            } else {
                return this.parent.path + this.edgeText;
            }
        }

        public get isRoot() {
            return this.parent == null;
        }
        public get isLeaf() {
            return this.children.length == 0;
        }
        public getNodes(): TreeNode[] {
            const r: TreeNode[] = [this];
            if (this.isLeaf) {
                return r;
            } else {
                this.children.forEach(function (x, i, arr) {
                    x.getNodes().forEach(function (y, j, arr2) {
                        r.push(y);
                    });
                });
                return r;
            }
        }

        public addLeaf(insertIndex: number, str: string): TreeNode {
            const newNode = new TreeNode();
            newNode.parent = this;
            newNode.edgeText = str;
            this.children.splice(insertIndex, 0, newNode);
            return newNode;
        }
        public split(splitPosition: number): TreeNode {
            const pref = this.edgeText.substr(0, splitPosition);
            const suf = this.edgeText.substr(splitPosition);

            if (this.parent != null) {

                const newNode = new TreeNode();

                const i = this.parent.children.indexOf(this);
                this.parent.children[i] = newNode;
                newNode.children.push(this);
                this.parent = newNode;

                newNode.edgeText = pref;
                this.edgeText = suf;

                return newNode;
            } else {
                return this;
            }
        }
        public locus(pattern: string): [TreeNode, number] {
            //const matchLen = 0;
            if (pattern.length == 0) return [this, 0];

            const [matchLen, comp] = StringModule.compare(this.edgeText, pattern);
            if (matchLen == this.edgeText.length && this.edgeText.length < pattern.length) {
                const edges = this.children.map(function (x, i, arr) { return x.edgeText.charCodeAt(0); });
                const suf = pattern.substr(matchLen);
                const [i, b] = getInsertIndex(edges, suf.charCodeAt(0));
                if (b) {
                    return this.children[i].locus(suf);
                } else {
                    return [this, matchLen];
                }

            } else {
                return [this, matchLen];
            }
        }
        public toLogicTree() : GraphTableSVG.LogicTree{
            const node = new GraphTableSVG.LogicTree({item : this, children : [], vertexText : this.nodeText, parentEdgeText : this.edgeText });
            this.children.forEach((v)=> node.children.push(v.toLogicTree()));
            return node;
        }
        
    }
    export function addString(node: TreeNode, pattern: string): TreeNode {
        if (pattern.length == 0) return node;
        const edges = node.children.map(function (x, i, arr) { return x.edgeText.charCodeAt(0); });
        const [i, isMatch] = getInsertIndex(edges, pattern.charCodeAt(0));
        if (!isMatch) {
            node.addLeaf(i, pattern[0]);
        }

        return addString(node.children[i], pattern.substr(1));
    }

    export function getInsertIndex(texts: number[], pattern: number): [number, boolean] {
        for (let i = 0; i < texts.length; i++) {
            if (pattern < texts[i]) {

            } else if (pattern == texts[i]) {
                return [i, true];
            } else {
                return [i, false];
            }

        }
        return [texts.length, false];
    }
    
    export function shrink(root: TreeNode) {
        if (root.parent != null) {
            if (root.children.length == 1) {
                const child = root.children[0];

                const i = root.parent.children.indexOf(root);
                
                root.parent.children[i] = child;
                child.parent = root.parent;
                child.edgeText = root.edgeText + child.edgeText;
                shrink(child);
            } else {
                root.children.forEach(function (x) { shrink(x) });
            }
        } else {
            root.children.forEach(function (x) { shrink(x) });
        }
    }

    /*
    function createNode(treeNode: TreeNode, graph: GraphTableSVG.Graph): GraphTableSVG.Vertex {
        const node = GraphTableSVG.CircleVertex.create(graph);
        node.svgText.textContent = treeNode.id.toString();
        treeNode.tag = node.objectID;
        return node;

        
    }
    */

}