namespace Grammar {
    export class CFG{
        rules : Rule[] = [];
        public get startVariable() : Rule {
            return this.rules[this.rules.length-1];
        }
        //startVariables: (number | string)[] = [];

        public copy(): CFG {
            const r = new CFG();
            this.rules.forEach((v) => { r.rules.push(v.copy()) });
            //this.startVariable.right.forEach((v) => { r.startVariables.push(v) });
            return r;
        }
        public createDerivationTree() : GraphTableSVG.LogicTree {
            return this.createDerivationTreeSub(this.rules.length-1);
            //this.startVariable.right.map((v)=>this.createDerivationTreeSub(v));
        }
        private createDerivationTreeSub(i : number | string) : GraphTableSVG.LogicTree {
            if(typeof(i) == "number"){
                const variable = this.rules[i];
                const children = variable.right.map((v)=>this.createDerivationTreeSub(v));
                const r = new GraphTableSVG.LogicTree({item : variable.name, children : children, vertexText : variable.name});
                return r;
            }else{
                const r = new GraphTableSVG.LogicTree({item : i, children : [], vertexText : i});
                return r;
            }
        }
        public toString(i : number) : string {
            const rule = this.rules[i];
            return  `${rule.name} → ${rule.right.map((v)=> typeof(v) == "number" ? this.rules[v].name : v).join("")}`;            
        }
        public createRuleArray() : string[][]{
            return this.rules.map((v,i)=>[this.toString(i)]);
        }
        static parse(text : string) : CFG {
            const r = new CFG();
            const rules = text.split("\n");
            const dic : { [key: string]: [number, string[]]; } = {};
            rules.forEach((v, i)=>{
                const rule = v.split(",")
                let name = "";
                rule.forEach((w, j) =>{
                    const w2 = w.trim();
                    if(j == 0){
                        name = w2;
                        dic[name] = [i, []];
                    }else{
                        dic[name][1].push(w2);
                    }
                })
            })
            r.rules = new Array(rules.length);
            Object.keys(dic).forEach((key) => {
                const val = dic[key]; // this は obj
                const element = document.getElementById(key);
                const right : (number | string)[] = val[1].map((v)=>{ return v in dic ? dic[v][0] : v});
                const rule = new Rule(key, right);
                r.rules[val[0]] = rule;
            }, dic);
            return r;
        }
        public getRule(right : (number | string)[] ): number | null {
            for (let i = 0; i < this.rules.length; i++) {
                if(this.rules[i].equals(right)) return i;
            }
            return null;
        }
    }
    export class Rule{
        constructor(public name : string, public right : (number | string)[] = []){

        }
        public copy() : Rule{
            return new Rule(this.name, this.right.map((v)=>v));
        }
        public equals(right : (number | string)[] ) : boolean{
            if(this.right.length == right.length){
                for(let i=0;i<this.right.length;i++){
                    if(this.right[i] != right[i]) return false;                    
                }
                return true;
            }else{
                return false;
            }
        }
    }
    /*
    export class SLP2Dictionary{
        public cfg : CFG;
        public dic : { [key: string]: number; }
        public constructor(cfg : CFG){

        }
    }
    */
}