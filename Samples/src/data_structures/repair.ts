﻿
namespace Grammar {
    class VariablePair {
        constructor(public left: number, public right: number) {

        }
        public toHash(): string {
            return `${this.left}/${this.right}`;
        }
    };
    export class RepairCompressor {
        slp: SLPDictionary;
        private occurrenceDic: { [key: string]: number[]; } = {};

        constructor(text: string) {
            this.slp = new SLPDictionary();
            for (let i = 0; i < text.length; i++) {
                const v = this.slp.addChar(text[i]);
                this.slp.startVariables.push(v);
            }
        }
        private collect() {
            this.occurrenceDic = {};
            for (let i = 0; i < this.slp.startVariables.length - 1; i++) {
                const pair = new VariablePair(this.slp.startVariables[i], this.slp.startVariables[i + 1]);
                const b = pair.left == pair.right;
                const str = pair.toHash();
                if (!(str in this.occurrenceDic)) this.occurrenceDic[str] = [];
                this.occurrenceDic[str].push(i);
                if (b && i + 1 < this.slp.startVariables.length - 1) {
                    const nextpair = new VariablePair(this.slp.startVariables[i + 1], this.slp.startVariables[i + 2]);
                    if (nextpair.left == nextpair.right) {
                        i++;
                    }
                }
                
            }
        }
        public compress() {
            while (this.slp.startVariables.length > 1) {
                this.repair();
            }
        }
        public repair() {
            this.collect();
            let i = 0;
            let max = 0;
            for (let key in this.occurrenceDic) {
                const size = this.occurrenceDic[key].length;
                if (size > max) {
                    max = size;
                    i = this.occurrenceDic[key][0];
                }
            }
            const pair = new VariablePair(this.slp.startVariables[i], this.slp.startVariables[i + 1]);
            this.replace(pair);
        }
        private replace(pair: VariablePair) {

            const arr = this.occurrenceDic[pair.toHash()];
            const newNumber = this.slp.addVariable(pair.left, pair.right);
            for (let i = arr.length-1; i >= 0; i--) {

                const x = arr[i];
                this.slp.startVariables.splice(x, 2, newNumber);
            }
        }

        static createRepairHistory(text : string) : CFG[] {
            const r = new Grammar.RepairCompressor(text);
            const grammars : CFG[] = [];
            grammars.push(r.slp.toCFG());
        
            while(r.slp.startVariables.length > 1){
                r.repair();
                grammars.push(r.slp.toCFG());
            }
            return grammars;
        }
    }

}