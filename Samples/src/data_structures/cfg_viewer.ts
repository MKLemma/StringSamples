
namespace Grammar {
    export class CFGViewer {
        tree: GraphTableSVG.GGraph;
        table: GraphTableSVG.GTable;
        dic: CFG;

        constructor(svg: SVGElement, tableClass: string | null = null, graphClass: string | null = null) {            
            this.tree = new GraphTableSVG.GGraph(svg, {class : graphClass!});
            this.table = new GraphTableSVG.GTable(svg, {class : tableClass!});
            this.table.setSize(1,1)
            if(this.tree.vertexXInterval == null) this.tree.vertexXInterval = 50;
            if(this.tree.vertexYInterval == null) this.tree.vertexYInterval = 50;

            if(graphClass == null){
                GraphTableSVG.CustomAttributeNames.defaultCircleRadius = 15;
            }
            this.tree.relocateAttribute = "Grammar.CFGViewer.relocateFunction";
        }
        public static relocateFunction(graph : GraphTableSVG.GGraph) : void {
            GraphTableSVG.TreeArrangement.alignVerticeByLeave(graph);
            GraphTableSVG.TreeArrangement.addOffset(graph, 50, 200);
        }
        
        public update(){
            //this.locate();
            this.table.update();
            //this.tree.update();
            this.tree.relocate();
            const rect = this.tree.getRegion();
            this.table.svgGroup.setX(rect.right + 50);
        }

        public create(cfg: CFG) {
            this.dic = cfg;
            this.tree.clear();
            this.table.clear();
            this.tree.constructFromLogicTree([this.dic.createDerivationTree()], {isLatexMode : true});
            this.table.construct(this.dic.createRuleArray(), {isLatexMode : true});
            this.update();
        }
        
        
    }

    export class HistoryDic{
        history : CFG[] = [];
        historyIndex : number = 0;
        viewer : CFGViewer;
        public constructor(viewer : CFGViewer){
            this.viewer = viewer;
        }

        public changeHistory(i : number){
            this.viewer.create(this.history[i].copy());
            this.historyIndex = i;
        }
        public add(dic : CFG){            
            if(this.historyIndex + 1 < this.history.length)this.history.splice(this.historyIndex+1, this.history.length - this.historyIndex - 1);            
            this.history.push(dic.copy());
            this.historyIndex++;
        }
        public prev(){
            this.changeHistory(this.historyIndex-1);
        }
        public get canPrev() :boolean{
            return this.historyIndex > 0;
        }
        public succ(){
            this.changeHistory(this.historyIndex+1);
        }
        public get canSucc() :boolean{
            return this.historyIndex + 1 < this.history.length;
        }
    }
    
}