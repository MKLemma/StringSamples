﻿namespace Grammar {

    export class Clicker extends SLPViewer {

        private _firstSelectedNode: GraphTableSVG.GVertex | null = null;

        public rootNodeClass: string = "root-node";
        public chosenNodeClass: string = "chosen-node";
        public unrootNodeClass: string = "unroot-node";
        public isChoosable: boolean = true;
        public history : SLPDictionary[] = [];
        public historyIndex : number = 0;

        public changeHistory(i : number){
            this.firstSelectedNode = null;
            this.create(this.history[i].copy());
            this.historyIndex = i;
        }

        get firstSelectedNode(): GraphTableSVG.GVertex | null {
            return this._firstSelectedNode;
        }
        set firstSelectedNode(value: GraphTableSVG.GVertex | null) {
            if (this.firstSelectedNode != null) {
                if (this.firstSelectedNode.surface != null) {
                    this.firstSelectedNode.surface.setAttribute("class", this.rootNodeClass);
                }
            }
            this._firstSelectedNode = value;
            if (this._firstSelectedNode != null) {
                if (this._firstSelectedNode.surface != null) {
                    this._firstSelectedNode.surface.setAttribute("class", this.chosenNodeClass);
                }
            }
        }

        constructor(text: string, svg: SVGElement, r: number = 30, tableClass: string | null = null, nodeClass: string | null = null) {
            super(svg, r, tableClass, nodeClass);

            
            this.graph.vertices.forEach((v) => {
                v.svgGroup.onclick = this.click;
                if (v.surface != null) {
                    v.surface.setAttribute("class", v.isNoParent ? this.rootNodeClass : this.unrootNodeClass);
                }
            })
            
            this.create(Clicker.createSLP(text));
            this.history.push(this.slp.copy());
        }
        private static createSLP(text: string): SLPDictionary {
            const slp = new SLPDictionary();
            for (let i = 0; i < text.length; i++) {
                const c = slp.addChar(text[i]);
                slp.startVariables.push(c);
            }
            return slp;
        }
        protected createNodeFunc(node : GraphTableSVG.GVertex) {
            this.updateNode(node);
            node.svgGroup.onclick = this.click;            
        }

        private click = (x: MouseEvent) => {
            if (!this.isChoosable) return;

            const svg: HTMLElement = <HTMLElement>x.currentTarget;
            const id = svg.getAttribute(GraphTableSVG.CustomAttributeNames.objectIDName);
            if (id == null) throw Error("Null Error");
            const node = GraphTableSVG.GObject.getObjectFromObjectID(id);
            if (node instanceof GraphTableSVG.GVertex) {
                if (node.isNoParent) {
                    const rootIndex = this.graph.roots.indexOf(node);
                    if (this.firstSelectedNode != null) {
                        if (this.firstSelectedNode == node) {
                            this.firstSelectedNode = null;
                        } else {
                            const fstRootIndex = this.graph.roots.indexOf(this.firstSelectedNode);
                            let newNode: GraphTableSVG.GVertex | null = null;
                            if (rootIndex + 1 == fstRootIndex) {
                                newNode = this.connect(rootIndex);
                            } else if (fstRootIndex + 1 == rootIndex) {
                                newNode = this.connect(fstRootIndex);
                            } else {
                                this.firstSelectedNode = node;
                            }

                            if (newNode != null) {
                                this.firstSelectedNode = null;
                                if (newNode.surface != null) newNode.surface.setAttribute("class", this.rootNodeClass);
                                const node1 = newNode.outcomingEdges[0].endVertex;
                                const node2 = newNode.outcomingEdges[1].endVertex;
                                if (node1 == null || node2 == null) throw Error("Null Error");
                                if (node1.surface != null) node1.surface.setAttribute("class", this.unrootNodeClass);
                                if (node2.surface != null) node2.surface.setAttribute("class", this.unrootNodeClass);
                                newNode.svgGroup.onclick = this.click;
                            }

                        }
                    } else {
                        this.firstSelectedNode = node;
                    }
                }
            }


        }
        private updateNode(node : GraphTableSVG.GVertex){            
            if (node.surface != null) {
                node.surface.setAttribute("class", node.isNoParent ? this.rootNodeClass : this.unrootNodeClass);
            }
        }
        public connect(i : number) : GraphTableSVG.GVertex {

            if(this.historyIndex + 1 < this.history.length)this.history.splice(this.historyIndex+1, this.history.length - this.historyIndex - 1);
            const result = super.connect(i);
            result.outcomingEdges.forEach((v)=> {
                const w = v.endVertex;
                if(w != null) this.updateNode(w);
            })
            this.updateNode(result);
            this.history.push(this.slp.copy());
            this.historyIndex++;
            this.clickCallback.forEach((v)=> v());
            return result;
        }
        public clickCallback: (() => void)[] = [];
    }
}