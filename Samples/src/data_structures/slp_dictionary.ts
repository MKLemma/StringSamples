namespace Grammar {
    interface NoncharVariable { type: "nonchar", value: number, left: number, right: number };
    interface CharVariable { type: "char", value: number, child: string };

    export class SLPDictionary {
        slpNodes: (NoncharVariable | CharVariable)[] = [];
        startVariables: number[] = [];
        //private _outcomingEdgesDic: { [key: number]: Edge[]; } = [];
        public addVariable(left: number, right: number): number {
            const result = this.getVariable(left, right);
            if (result == null) {
                const newNumber = this.slpNodes.length;
                const node: NoncharVariable = { type: "nonchar", value: newNumber, left: left, right: right };
                this.slpNodes.push(node);
                return node.value;
            } else {
                return result.value;
            }

        }
        public connect(i : number) : number{
            const left = this.startVariables[i];
            const right = this.startVariables[i+1];
            const newSig = this.addVariable(left,right);
            this.startVariables.splice(i, 2, newSig);
            return newSig;
        }
        public addChar(char: string): number {
            const result = this.getChar(char);
            if (result == null) {
                const newNumber = this.slpNodes.length;
                const node: CharVariable = { type: "char", value: newNumber, child: char };
                this.slpNodes.push(node);
                return node.value;
            } else {
                return result.value;
            }
        }

        public getVariable(left: number, right: number): NoncharVariable | null {
            for (let i = 0; i < this.slpNodes.length; i++) {
                const p = this.slpNodes[i];
                if (p.type == "nonchar") {
                    if (p.left == left && p.right == right) {
                        return p;
                    }
                }
            }
            return null;
        }
        public getChar(child: string): CharVariable | null {
            for (let i = 0; i < this.slpNodes.length; i++) {
                const p = this.slpNodes[i];
                if (p.type == "char") {
                    if (p.child == child) {
                        return p;
                    }
                }
            }
            return null;
        }
        public getTextCharvariables(variable: number | null = null): CharVariable[] {
            if (variable != null) {
                const p = this.slpNodes[variable];
                if (p.type == "char") {
                    return [p];
                } else {
                    const left = this.getTextCharvariables(p.left);
                    const right = this.getTextCharvariables(p.right);
                    right.forEach((v) => left.push(v));
                    return left;
                }
            } else {
                const r: CharVariable[] = [];
                const roots = this.startVariables.map((v) => this.getTextCharvariables(v)).forEach((v) => {
                    v.forEach((w) => {
                        r.push(w);
                    })
                });
                return r;
            }
        }
        get text(): string {
            let r: string = "";
            this.getTextCharvariables().forEach((v) => { r += v.child });
            return r;
        }
        public copy(): SLPDictionary {
            const r = new SLPDictionary();
            this.slpNodes.forEach((v) => { r.slpNodes.push(v) });
            this.startVariables.forEach((v) => { r.startVariables.push(v) });
            return r;

        }

        public toCFG() : CFG{
            const r = new CFG();
            r.rules = this.slpNodes.map((v,i) =>{
                if(v.type == "char"){
                    return new Rule(`X_{${i+1}}`, [v.child]);
                }else{
                    return new Rule(`X_{${i+1}}`, [v.left, v.right]);
                }
            })
            if(this.startVariables.length != 1){
                r.rules.push(new Rule(`S`, this.startVariables.map((v)=>v)));
            }
            return r;
        }

        
    }
}