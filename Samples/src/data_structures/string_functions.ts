﻿
namespace StringModule{
    function bunrui(strings: string[]): { [key: number]: string[]; } {
        const p: { [key: number]: string[]; } = {};
        for (let i = 0; i < strings.length; i++) {
            const str1 = strings[i];
            const c = str1.charCodeAt(0);
            if (p[c] == null) p[c] = new Array(0);

            p[c].push(str1.substring(1));
        }
        return p;
    }
    function getChars(str: string): string[] {
        const p: { [key: number]: string; } = {};

        const r = new Array(0);
        let count = 0;
        for (let i = 0; i < str.length; i++) {
            if (p[str.charAt(i)] == null) {
                p[str.charCodeAt(i)] = str.charAt(i);
                count++;
            } else {
            }
        }

        for (let c in p) {
            const ch: number = +c;
            r.push(String.fromCharCode(ch));
        }
        return r;
    }
    export function compare(str1: string, str2: string): [number, number] {
        const min = Math.min(str1.length, str2.length);
        for (let i = 0; i <= min; i++) {
            if (str1.charAt(i) < str2.charAt(i)) {
                return [i, - 1];
            } else if (str1.charAt(i) > str2.charAt(i)) {
                return [i, 1];
            }
        }
        if (str1 == str2) {
            return [str1.length, 0];
        } else {
            return str1.length < str2.length ? [str1.length, 1] : [str2.length, -1];
        }

    }

    export function computeSuffixArray(str: string): number[] {
        const arr: number[] = new Array(str.length);
        for (let i = 0; i < str.length; i++) {
            arr[i] = i;
        }

        const func = function (item1: number, item2: number): number {
            for (let i = 0; i <= str.length; i++) {
                if (item1 + i >= str.length || item2 + i >= str.length) break;
                if (str.charAt(item1 + i) < str.charAt(item2 + i)) {
                    return - 1;
                } else if (str.charAt(item1 + i) > str.charAt(item2 + i)) {
                    return 1;
                }
            }
            if (item1 == item2) {
                return 0;
            } else {
                return item1 < item2 ? 1 : -1;
            }
        };
        arr.sort(func);
        return arr;
    }
    function getSortedSuffixes(str: string): string[] {
        const arr = computeSuffixArray(str);
        const r: string[] = new Array(arr.length);
        for (let i = 0; i < arr.length; i++) {
            r[i] = str.substring(arr[i]);
        }
        return r;
    }
    function computeLCP(str1: string, str2: string): number {
        const min = str1.length < str2.length ? str1.length : str2.length;
        let lcp = 0;
        for (let i = 0; i < min; i++) {
            if (str1.charAt[i] == str2.charAt[i]) {
                lcp++;
            } else {
                break;
            }
        }
        return lcp;
    }
    function computeLCPArray(str: string, sufarr: number[]): number[] {
        const lcparr = new Array(sufarr.length);
        for (let i = 0; i < sufarr.length; i++) {
            if (i == 0 && sufarr.length > 1) {
                lcparr[i] = -1;
            } else {
                lcparr[i] = computeLCP(str.substring(sufarr[i]), str.substring(sufarr[i - 1]));
            }
        }
        return lcparr;
    }
    export function createAllSuffixes(str: string): string[] {
        let str2 = "";
        for (let i= 0; i < str.length;i++){
            if (str[i] != "\n") {
                str2 += str[i];
            }
        }
        const suffixes = new Array(str2.length);
        for (let i = 0; i < suffixes.length; i++) {
            suffixes[i] = str2.substring(i);
        }
        return suffixes;
    }
    export function createAllTruncatedSuffixes(str: string, truncatedLength: number): string[] {
        const suffixes = new Array(str.length);
        for (let i = 0; i < suffixes.length; i++) {
            suffixes[i] = str.substr(i, truncatedLength);
        }
        return suffixes;
    }

    export function removeSpace(str: string): string {
        let r = "";
        const emptyCode = " ".charCodeAt(0);
        for (let i = 0; i < str.length; i++) {
            if (str.charCodeAt(i) != emptyCode) {
                r += str.charAt(i);
            }
        }
        return r;
    }
    function removeFirstSpaces(str: string): string {
        let i = 0;
        for (i = 0; i < str.length; i++) {
            if (str[i] != " ") break;
        }
        if (i == str.length) {
            return "";
        } else {
            return str.substring(i);
        }
    }
    export function reverse(str: string): string {
        const rv: string[] = [];
        for (let i = 0, n = str.length; i < n; i++) {
            rv[i] = str.charAt(n - i - 1);
        }
        return rv.join("");
    }

    export function LZ77WithSelfReference(str: string): ([number, number] | string)[] {
        const r = new Array(0);
        let startPos = 0;
        let lastRefPos = -1;
        let i = 0;
        while (i < str.length) {
            const substr = str.substr(startPos, i - startPos + 1);
            const reference = i == 0 ? "" : str.substr(0, i);
            const refPos = reference.indexOf(substr);
            if (refPos == -1) {
                if (lastRefPos == -1) {
                    r.push(substr);
                    i++;
                } else {
                    r.push([lastRefPos, i - startPos]);
                }
                startPos = i;
                lastRefPos = -1;
            } else {
                lastRefPos = refPos;
                i++;
            }
        }
        if (lastRefPos != -1) {
            r.push([lastRefPos, str.length - startPos]);
        }
        return r;
    }
    export function computeEditDisutanceTable(str1: string, str2: string): number[][]{
        str1 = "_" + str1;
        str2 = "_" + str2;
        const r: number[][] = new Array(str1.length);
        for (let y = 0; y < r.length; y++) {
            r[y] = new Array(str2.length);
            for (let x = 0; x < r[y].length; x++) {
                r[y][x] = -1;
            }
        }
        for (let y = 0; y < str1.length; y++) {
            r[y][0] = y;
        }
        for (let x = 0; x < str2.length; x++) {
            r[0][x] = x;
        }

        for (let y = 0; y < r.length; y++) {
            for (let x = 0; x < r[y].length; x++) {
                if (r[y][x] == -1) {
                    const top = r[y - 1][x] + 1;
                    const left = r[y][x - 1] + 1;
                    const upperLeft = r[y - 1][x - 1] + (str1[y] == str2[x] ? 0 : 1);

                    r[y][x] = Math.min(top, left, upperLeft);
                    
                }
            }
        }
        return r;
    }
    
}
