﻿
namespace Grammar {
    export class SLPViewer {
        graph: GraphTableSVG.GGraph;
        table: GraphTableSVG.GTable;
        slp: SLPDictionary;
        svg : SVGElement;
        tableClass: string | null = null;
        graphClass: string | null = null;
        //nodeClass: string | null = null;
        r: number;
        //graphOffsetY : number = 300;
        //graphOffsetX : number = 50;

        //private _nodeXInterval: number = 50;
        //private _nodeYInterval: number = 50;

        get nodeXInterval(): number {
            if(this.graph.vertexXInterval == null) this.graph.vertexXInterval = 50;
            return this.graph.vertexXInterval;
        }
        set nodeXInterval(value: number) {
            this.graph.vertexXInterval = value;
            this.locate();
        }
        get nodeYInterval(): number {
            if(this.graph.vertexYInterval == null) this.graph.vertexYInterval = 50;
            return this.graph.vertexYInterval;
        }
        set nodeYInterval(value: number) {
            this.graph.vertexYInterval = value;
            this.locate();
        }
        
        public static relocateFunction(graph : GraphTableSVG.GGraph) : void {
            GraphTableSVG.TreeArrangement.alignVerticeByLeave(graph);
            GraphTableSVG.TreeArrangement.addOffset(graph, 50, 200);
        }
        //private _idVariableDic: { [key: number]: number; } = [];

        constructor(svg: SVGElement, r: number = 30, tableClass: string | null = null, graphClass: string | null = null) {
            this.r = r;
            this.svg = svg;
            this.tableClass = tableClass;
            this.graphClass = graphClass;

            this.graph = new GraphTableSVG.GGraph(this.svg, { class : this.graphClass!});
            this.graph.relocateAttribute = "Grammar.SLPViewer.relocateFunction";
            this.table = new GraphTableSVG.GTable(this.svg, { class : this.tableClass!});
            this.table.setSize(1,1);
            /*
            this.graph = new GraphTableSVG.Graph(svg, this.graphClass);
            this.table = new GraphTableSVG.Table(svg, 1, 1, this.tableClass);
            this.slp = slp;
            */
            //this.create(slp);
            //this.locate();
            //this.graph.update();
        }
        
        

        private locate() {
            this.graph.relocate();
            /*
            this.graph.vertices.forEach((v)=>{v.cx = 0 ; v.cy = 0});
            GraphTableSVG.GTreeArrangement.alignVerticeByLeave(this.graph);

            //GraphTableSVG.GTreeArrangement.reverse(this.graph, false, true);
            this.graph.svgGroup.setY(180);
            this.graph.svgGroup.setX(30);
            */
            const rect = this.graph.getRegion();
            
            this.table.svgGroup.setX(rect.right + 50);
        }
        public update(){
            this.locate();
            this.table.update();
            //this.graph.update();
        }

        public create(slp: SLPDictionary) {
            this.slp = slp;
            this.graph.clear();
            this.table.clear();

            this.slp.startVariables.forEach((v, i) => {
                const node = this.createVariable(v, null);
                this.graph.setRootIndex(node, i);
            });

            this.slp.slpNodes.forEach((v) => {
                this.appendInfo(v.value);
            })

            this.locate();
        }
        private appendInfo(variable: number) {
            let str = "";
            const c = this.slp.slpNodes[variable];

            if (c.type == 'nonchar') {
                str = `X_{${c.value + 1}} → X_{${c.left + 1}}X_{${c.right + 1}}`;
            } else {
                str = `X_{${c.value + 1}} → ${c.child}`;
            }

            /*
            if (this.table.cells[this.table.height - 1][0].svgText.textContent != "") {
            }
            */
            this.table.appendRow();
            this.table.cells[this.table.rowCount - 1][0].svgText.setTextContent(str,true);
            

        }
        protected createNode(variable: number | string): GraphTableSVG.GVertex{
            //const variableNode = GraphTableSVG.CircleVertex.create(this.graph, this.graph.defaultVertexClass);
            const variableNode = new GraphTableSVG.GEllipse(this.graph.svgGroup, {class : this.graph.defaultVertexClass!});
            
            variableNode.svgText.setAttribute("pointer-events", "none");
            if (typeof (variable) == "string") {
                variableNode.svgGroup.setAttribute("str", variable);            
                variableNode.svgText.textContent = `${variable}`;
            } else {
                variableNode.svgGroup.setAttribute("variable", variable.toString());
                variableNode.svgText.setTextContent(`X_{${variable + 1}}`, true);
            }
            return variableNode;
        }
        protected createNodeFunc(node : GraphTableSVG.GVertex){

        }
        public createVariable(variable: number, parent: GraphTableSVG.GVertex | null, insertIndex: number = 0) : GraphTableSVG.GVertex {
            const v = this.slp.slpNodes[variable];

            const variableNode = this.createNode(variable);
            if (parent != null) {
                const edge = new GraphTableSVG.GEdge(this.graph.svgGroup, {class : this.graph.defaultEdgeClass!});
                //this.graph.connect(parent, edge, variableNode, insertIndex,null, "bottom", "top");
                this.graph.connect(parent, edge, variableNode, 
                    {outcomingInsertIndex : insertIndex});

            } else {
                this.graph.setRootIndex(variableNode, this.graph.roots.length);                
                //this.graph.roots.push(variableNode);
            }
            

            if (v.type == "nonchar") {
                this.createVariable(v.left, variableNode, 0);
                this.createVariable(v.right, variableNode, 1);
            } else {
                const charNode = this.createNode(v.child);
                //charNode.svgText.textContent = `${v.child}`;
                const edge2 = new GraphTableSVG.GEdge(this.graph.svgGroup, { class : this.graph.defaultEdgeClass!});
                //this.graph.connect(variableNode, edge2, charNode, 0, null, "bottom", "top");
                this.graph.connect(variableNode, edge2, charNode, {outcomingInsertIndex : 0});

                this.createNodeFunc(charNode);

            }
            this.createNodeFunc(variableNode);
            return variableNode;
        }
        public connect(i: number): GraphTableSVG.GVertex {
            const node1 = this.graph.roots[i];
            const node2 = this.graph.roots[i+1];
            const variable1 = Number(node1.svgGroup.getAttribute("variable"));
            const variable2 = Number(node2.svgGroup.getAttribute("variable"));
            const b = this.slp.getVariable(variable1, variable2) == null;
            const variable3 = this.slp.connect(i);
            if (b) {
                this.appendInfo(variable3);
            }
            const newNode = this.createNode(variable3);
            this.graph.setRootIndex(newNode, i);
            //this._idVariableDic[newNode.symbol] = variable3;
            const newEdge1 = new GraphTableSVG.GEdge(this.graph.svgGroup, {class : this.graph.defaultEdgeClass!});
            const newEdge2 = new GraphTableSVG.GEdge(this.graph.svgGroup, {class : this.graph.defaultEdgeClass!});
            //this.graph.connect(newNode, newEdge1, node1, 0, null, "bottom", "top");
            this.graph.connect(newNode, newEdge1, node1, {outcomingInsertIndex : 0});
            this.graph.connect(newNode, newEdge2, node2,{outcomingInsertIndex : 1});
            this.locate();
            //this.graph.update();
            return newNode;
        }

        
    }

    
}