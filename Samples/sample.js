var Grammar;
(function (Grammar) {
    class CFG {
        constructor() {
            this.rules = [];
        }
        get startVariable() {
            return this.rules[this.rules.length - 1];
        }
        //startVariables: (number | string)[] = [];
        copy() {
            const r = new CFG();
            this.rules.forEach((v) => { r.rules.push(v.copy()); });
            //this.startVariable.right.forEach((v) => { r.startVariables.push(v) });
            return r;
        }
        createDerivationTree() {
            return this.createDerivationTreeSub(this.rules.length - 1);
            //this.startVariable.right.map((v)=>this.createDerivationTreeSub(v));
        }
        createDerivationTreeSub(i) {
            if (typeof (i) == "number") {
                const variable = this.rules[i];
                const children = variable.right.map((v) => this.createDerivationTreeSub(v));
                const r = new GraphTableSVG.LogicTree({ item: variable.name, children: children, vertexText: variable.name });
                return r;
            }
            else {
                const r = new GraphTableSVG.LogicTree({ item: i, children: [], vertexText: i });
                return r;
            }
        }
        toString(i) {
            const rule = this.rules[i];
            return `${rule.name} → ${rule.right.map((v) => typeof (v) == "number" ? this.rules[v].name : v).join("")}`;
        }
        createRuleArray() {
            return this.rules.map((v, i) => [this.toString(i)]);
        }
        static parse(text) {
            const r = new CFG();
            const rules = text.split("\n");
            const dic = {};
            rules.forEach((v, i) => {
                const rule = v.split(",");
                let name = "";
                rule.forEach((w, j) => {
                    const w2 = w.trim();
                    if (j == 0) {
                        name = w2;
                        dic[name] = [i, []];
                    }
                    else {
                        dic[name][1].push(w2);
                    }
                });
            });
            r.rules = new Array(rules.length);
            Object.keys(dic).forEach((key) => {
                const val = dic[key]; // this は obj
                const element = document.getElementById(key);
                const right = val[1].map((v) => { return v in dic ? dic[v][0] : v; });
                const rule = new Rule(key, right);
                r.rules[val[0]] = rule;
            }, dic);
            return r;
        }
        getRule(right) {
            for (let i = 0; i < this.rules.length; i++) {
                if (this.rules[i].equals(right))
                    return i;
            }
            return null;
        }
    }
    Grammar.CFG = CFG;
    class Rule {
        constructor(name, right = []) {
            this.name = name;
            this.right = right;
        }
        copy() {
            return new Rule(this.name, this.right.map((v) => v));
        }
        equals(right) {
            if (this.right.length == right.length) {
                for (let i = 0; i < this.right.length; i++) {
                    if (this.right[i] != right[i])
                        return false;
                }
                return true;
            }
            else {
                return false;
            }
        }
    }
    Grammar.Rule = Rule;
    /*
    export class SLP2Dictionary{
        public cfg : CFG;
        public dic : { [key: string]: number; }
        public constructor(cfg : CFG){

        }
    }
    */
})(Grammar || (Grammar = {}));
var Grammar;
(function (Grammar) {
    class CFGViewer {
        constructor(svg, tableClass = null, graphClass = null) {
            this.tree = new GraphTableSVG.GGraph(svg, { class: graphClass });
            this.table = new GraphTableSVG.GTable(svg, { class: tableClass });
            this.table.setSize(1, 1);
            if (this.tree.vertexXInterval == null)
                this.tree.vertexXInterval = 50;
            if (this.tree.vertexYInterval == null)
                this.tree.vertexYInterval = 50;
            if (graphClass == null) {
                GraphTableSVG.CustomAttributeNames.defaultCircleRadius = 15;
            }
            this.tree.relocateAttribute = "Grammar.CFGViewer.relocateFunction";
        }
        static relocateFunction(graph) {
            GraphTableSVG.TreeArrangement.alignVerticeByLeave(graph);
            GraphTableSVG.TreeArrangement.addOffset(graph, 50, 200);
        }
        update() {
            //this.locate();
            this.table.update();
            //this.tree.update();
            this.tree.relocate();
            const rect = this.tree.getRegion();
            this.table.svgGroup.setX(rect.right + 50);
        }
        create(cfg) {
            this.dic = cfg;
            this.tree.clear();
            this.table.clear();
            this.tree.constructFromLogicTree([this.dic.createDerivationTree()], { isLatexMode: true });
            this.table.construct(this.dic.createRuleArray(), { isLatexMode: true });
            this.update();
        }
    }
    Grammar.CFGViewer = CFGViewer;
    class HistoryDic {
        constructor(viewer) {
            this.history = [];
            this.historyIndex = 0;
            this.viewer = viewer;
        }
        changeHistory(i) {
            this.viewer.create(this.history[i].copy());
            this.historyIndex = i;
        }
        add(dic) {
            if (this.historyIndex + 1 < this.history.length)
                this.history.splice(this.historyIndex + 1, this.history.length - this.historyIndex - 1);
            this.history.push(dic.copy());
            this.historyIndex++;
        }
        prev() {
            this.changeHistory(this.historyIndex - 1);
        }
        get canPrev() {
            return this.historyIndex > 0;
        }
        succ() {
            this.changeHistory(this.historyIndex + 1);
        }
        get canSucc() {
            return this.historyIndex + 1 < this.history.length;
        }
    }
    Grammar.HistoryDic = HistoryDic;
})(Grammar || (Grammar = {}));
var Grammar;
(function (Grammar) {
    class VariablePair {
        constructor(left, right) {
            this.left = left;
            this.right = right;
        }
        toHash() {
            return `${this.left}/${this.right}`;
        }
    }
    ;
    class RepairCompressor {
        constructor(text) {
            this.occurrenceDic = {};
            this.slp = new Grammar.SLPDictionary();
            for (let i = 0; i < text.length; i++) {
                const v = this.slp.addChar(text[i]);
                this.slp.startVariables.push(v);
            }
        }
        collect() {
            this.occurrenceDic = {};
            for (let i = 0; i < this.slp.startVariables.length - 1; i++) {
                const pair = new VariablePair(this.slp.startVariables[i], this.slp.startVariables[i + 1]);
                const b = pair.left == pair.right;
                const str = pair.toHash();
                if (!(str in this.occurrenceDic))
                    this.occurrenceDic[str] = [];
                this.occurrenceDic[str].push(i);
                if (b && i + 1 < this.slp.startVariables.length - 1) {
                    const nextpair = new VariablePair(this.slp.startVariables[i + 1], this.slp.startVariables[i + 2]);
                    if (nextpair.left == nextpair.right) {
                        i++;
                    }
                }
            }
        }
        compress() {
            while (this.slp.startVariables.length > 1) {
                this.repair();
            }
        }
        repair() {
            this.collect();
            let i = 0;
            let max = 0;
            for (let key in this.occurrenceDic) {
                const size = this.occurrenceDic[key].length;
                if (size > max) {
                    max = size;
                    i = this.occurrenceDic[key][0];
                }
            }
            const pair = new VariablePair(this.slp.startVariables[i], this.slp.startVariables[i + 1]);
            this.replace(pair);
        }
        replace(pair) {
            const arr = this.occurrenceDic[pair.toHash()];
            const newNumber = this.slp.addVariable(pair.left, pair.right);
            for (let i = arr.length - 1; i >= 0; i--) {
                const x = arr[i];
                this.slp.startVariables.splice(x, 2, newNumber);
            }
        }
        static createRepairHistory(text) {
            const r = new Grammar.RepairCompressor(text);
            const grammars = [];
            grammars.push(r.slp.toCFG());
            while (r.slp.startVariables.length > 1) {
                r.repair();
                grammars.push(r.slp.toCFG());
            }
            return grammars;
        }
    }
    Grammar.RepairCompressor = RepairCompressor;
})(Grammar || (Grammar = {}));
var Grammar;
(function (Grammar) {
    ;
    ;
    class SLPDictionary {
        constructor() {
            this.slpNodes = [];
            this.startVariables = [];
        }
        //private _outcomingEdgesDic: { [key: number]: Edge[]; } = [];
        addVariable(left, right) {
            const result = this.getVariable(left, right);
            if (result == null) {
                const newNumber = this.slpNodes.length;
                const node = { type: "nonchar", value: newNumber, left: left, right: right };
                this.slpNodes.push(node);
                return node.value;
            }
            else {
                return result.value;
            }
        }
        connect(i) {
            const left = this.startVariables[i];
            const right = this.startVariables[i + 1];
            const newSig = this.addVariable(left, right);
            this.startVariables.splice(i, 2, newSig);
            return newSig;
        }
        addChar(char) {
            const result = this.getChar(char);
            if (result == null) {
                const newNumber = this.slpNodes.length;
                const node = { type: "char", value: newNumber, child: char };
                this.slpNodes.push(node);
                return node.value;
            }
            else {
                return result.value;
            }
        }
        getVariable(left, right) {
            for (let i = 0; i < this.slpNodes.length; i++) {
                const p = this.slpNodes[i];
                if (p.type == "nonchar") {
                    if (p.left == left && p.right == right) {
                        return p;
                    }
                }
            }
            return null;
        }
        getChar(child) {
            for (let i = 0; i < this.slpNodes.length; i++) {
                const p = this.slpNodes[i];
                if (p.type == "char") {
                    if (p.child == child) {
                        return p;
                    }
                }
            }
            return null;
        }
        getTextCharvariables(variable = null) {
            if (variable != null) {
                const p = this.slpNodes[variable];
                if (p.type == "char") {
                    return [p];
                }
                else {
                    const left = this.getTextCharvariables(p.left);
                    const right = this.getTextCharvariables(p.right);
                    right.forEach((v) => left.push(v));
                    return left;
                }
            }
            else {
                const r = [];
                const roots = this.startVariables.map((v) => this.getTextCharvariables(v)).forEach((v) => {
                    v.forEach((w) => {
                        r.push(w);
                    });
                });
                return r;
            }
        }
        get text() {
            let r = "";
            this.getTextCharvariables().forEach((v) => { r += v.child; });
            return r;
        }
        copy() {
            const r = new SLPDictionary();
            this.slpNodes.forEach((v) => { r.slpNodes.push(v); });
            this.startVariables.forEach((v) => { r.startVariables.push(v); });
            return r;
        }
        toCFG() {
            const r = new Grammar.CFG();
            r.rules = this.slpNodes.map((v, i) => {
                if (v.type == "char") {
                    return new Grammar.Rule(`X_{${i + 1}}`, [v.child]);
                }
                else {
                    return new Grammar.Rule(`X_{${i + 1}}`, [v.left, v.right]);
                }
            });
            if (this.startVariables.length != 1) {
                r.rules.push(new Grammar.Rule(`S`, this.startVariables.map((v) => v)));
            }
            return r;
        }
    }
    Grammar.SLPDictionary = SLPDictionary;
})(Grammar || (Grammar = {}));
var Grammar;
(function (Grammar) {
    class SLPViewer {
        //private _idVariableDic: { [key: number]: number; } = [];
        constructor(svg, r = 30, tableClass = null, graphClass = null) {
            this.tableClass = null;
            this.graphClass = null;
            this.r = r;
            this.svg = svg;
            this.tableClass = tableClass;
            this.graphClass = graphClass;
            this.graph = new GraphTableSVG.GGraph(this.svg, { class: this.graphClass });
            this.graph.relocateAttribute = "Grammar.SLPViewer.relocateFunction";
            this.table = new GraphTableSVG.GTable(this.svg, { class: this.tableClass });
            this.table.setSize(1, 1);
            /*
            this.graph = new GraphTableSVG.Graph(svg, this.graphClass);
            this.table = new GraphTableSVG.Table(svg, 1, 1, this.tableClass);
            this.slp = slp;
            */
            //this.create(slp);
            //this.locate();
            //this.graph.update();
        }
        //graphOffsetY : number = 300;
        //graphOffsetX : number = 50;
        //private _nodeXInterval: number = 50;
        //private _nodeYInterval: number = 50;
        get nodeXInterval() {
            if (this.graph.vertexXInterval == null)
                this.graph.vertexXInterval = 50;
            return this.graph.vertexXInterval;
        }
        set nodeXInterval(value) {
            this.graph.vertexXInterval = value;
            this.locate();
        }
        get nodeYInterval() {
            if (this.graph.vertexYInterval == null)
                this.graph.vertexYInterval = 50;
            return this.graph.vertexYInterval;
        }
        set nodeYInterval(value) {
            this.graph.vertexYInterval = value;
            this.locate();
        }
        static relocateFunction(graph) {
            GraphTableSVG.TreeArrangement.alignVerticeByLeave(graph);
            GraphTableSVG.TreeArrangement.addOffset(graph, 50, 200);
        }
        locate() {
            this.graph.relocate();
            /*
            this.graph.vertices.forEach((v)=>{v.cx = 0 ; v.cy = 0});
            GraphTableSVG.GTreeArrangement.alignVerticeByLeave(this.graph);

            //GraphTableSVG.GTreeArrangement.reverse(this.graph, false, true);
            this.graph.svgGroup.setY(180);
            this.graph.svgGroup.setX(30);
            */
            const rect = this.graph.getRegion();
            this.table.svgGroup.setX(rect.right + 50);
        }
        update() {
            this.locate();
            this.table.update();
            //this.graph.update();
        }
        create(slp) {
            this.slp = slp;
            this.graph.clear();
            this.table.clear();
            this.slp.startVariables.forEach((v, i) => {
                const node = this.createVariable(v, null);
                this.graph.setRootIndex(node, i);
            });
            this.slp.slpNodes.forEach((v) => {
                this.appendInfo(v.value);
            });
            this.locate();
        }
        appendInfo(variable) {
            let str = "";
            const c = this.slp.slpNodes[variable];
            if (c.type == 'nonchar') {
                str = `X_{${c.value + 1}} → X_{${c.left + 1}}X_{${c.right + 1}}`;
            }
            else {
                str = `X_{${c.value + 1}} → ${c.child}`;
            }
            /*
            if (this.table.cells[this.table.height - 1][0].svgText.textContent != "") {
            }
            */
            this.table.appendRow();
            this.table.cells[this.table.rowCount - 1][0].svgText.setTextContent(str, true);
        }
        createNode(variable) {
            //const variableNode = GraphTableSVG.CircleVertex.create(this.graph, this.graph.defaultVertexClass);
            const variableNode = new GraphTableSVG.GEllipse(this.graph.svgGroup, { class: this.graph.defaultVertexClass });
            variableNode.svgText.setAttribute("pointer-events", "none");
            if (typeof (variable) == "string") {
                variableNode.svgGroup.setAttribute("str", variable);
                variableNode.svgText.textContent = `${variable}`;
            }
            else {
                variableNode.svgGroup.setAttribute("variable", variable.toString());
                variableNode.svgText.setTextContent(`X_{${variable + 1}}`, true);
            }
            return variableNode;
        }
        createNodeFunc(node) {
        }
        createVariable(variable, parent, insertIndex = 0) {
            const v = this.slp.slpNodes[variable];
            const variableNode = this.createNode(variable);
            if (parent != null) {
                const edge = new GraphTableSVG.GEdge(this.graph.svgGroup, { class: this.graph.defaultEdgeClass });
                //this.graph.connect(parent, edge, variableNode, insertIndex,null, "bottom", "top");
                this.graph.connect(parent, edge, variableNode, { outcomingInsertIndex: insertIndex });
            }
            else {
                this.graph.setRootIndex(variableNode, this.graph.roots.length);
                //this.graph.roots.push(variableNode);
            }
            if (v.type == "nonchar") {
                this.createVariable(v.left, variableNode, 0);
                this.createVariable(v.right, variableNode, 1);
            }
            else {
                const charNode = this.createNode(v.child);
                //charNode.svgText.textContent = `${v.child}`;
                const edge2 = new GraphTableSVG.GEdge(this.graph.svgGroup, { class: this.graph.defaultEdgeClass });
                //this.graph.connect(variableNode, edge2, charNode, 0, null, "bottom", "top");
                this.graph.connect(variableNode, edge2, charNode, { outcomingInsertIndex: 0 });
                this.createNodeFunc(charNode);
            }
            this.createNodeFunc(variableNode);
            return variableNode;
        }
        connect(i) {
            const node1 = this.graph.roots[i];
            const node2 = this.graph.roots[i + 1];
            const variable1 = Number(node1.svgGroup.getAttribute("variable"));
            const variable2 = Number(node2.svgGroup.getAttribute("variable"));
            const b = this.slp.getVariable(variable1, variable2) == null;
            const variable3 = this.slp.connect(i);
            if (b) {
                this.appendInfo(variable3);
            }
            const newNode = this.createNode(variable3);
            this.graph.setRootIndex(newNode, i);
            //this._idVariableDic[newNode.symbol] = variable3;
            const newEdge1 = new GraphTableSVG.GEdge(this.graph.svgGroup, { class: this.graph.defaultEdgeClass });
            const newEdge2 = new GraphTableSVG.GEdge(this.graph.svgGroup, { class: this.graph.defaultEdgeClass });
            //this.graph.connect(newNode, newEdge1, node1, 0, null, "bottom", "top");
            this.graph.connect(newNode, newEdge1, node1, { outcomingInsertIndex: 0 });
            this.graph.connect(newNode, newEdge2, node2, { outcomingInsertIndex: 1 });
            this.locate();
            //this.graph.update();
            return newNode;
        }
    }
    Grammar.SLPViewer = SLPViewer;
})(Grammar || (Grammar = {}));
var Grammar;
(function (Grammar) {
    class Clicker extends Grammar.SLPViewer {
        constructor(text, svg, r = 30, tableClass = null, nodeClass = null) {
            super(svg, r, tableClass, nodeClass);
            this._firstSelectedNode = null;
            this.rootNodeClass = "root-node";
            this.chosenNodeClass = "chosen-node";
            this.unrootNodeClass = "unroot-node";
            this.isChoosable = true;
            this.history = [];
            this.historyIndex = 0;
            this.click = (x) => {
                if (!this.isChoosable)
                    return;
                const svg = x.currentTarget;
                const id = svg.getAttribute(GraphTableSVG.CustomAttributeNames.objectIDName);
                if (id == null)
                    throw Error("Null Error");
                const node = GraphTableSVG.GObject.getObjectFromObjectID(id);
                if (node instanceof GraphTableSVG.GVertex) {
                    if (node.isNoParent) {
                        const rootIndex = this.graph.roots.indexOf(node);
                        if (this.firstSelectedNode != null) {
                            if (this.firstSelectedNode == node) {
                                this.firstSelectedNode = null;
                            }
                            else {
                                const fstRootIndex = this.graph.roots.indexOf(this.firstSelectedNode);
                                let newNode = null;
                                if (rootIndex + 1 == fstRootIndex) {
                                    newNode = this.connect(rootIndex);
                                }
                                else if (fstRootIndex + 1 == rootIndex) {
                                    newNode = this.connect(fstRootIndex);
                                }
                                else {
                                    this.firstSelectedNode = node;
                                }
                                if (newNode != null) {
                                    this.firstSelectedNode = null;
                                    if (newNode.surface != null)
                                        newNode.surface.setAttribute("class", this.rootNodeClass);
                                    const node1 = newNode.outcomingEdges[0].endVertex;
                                    const node2 = newNode.outcomingEdges[1].endVertex;
                                    if (node1 == null || node2 == null)
                                        throw Error("Null Error");
                                    if (node1.surface != null)
                                        node1.surface.setAttribute("class", this.unrootNodeClass);
                                    if (node2.surface != null)
                                        node2.surface.setAttribute("class", this.unrootNodeClass);
                                    newNode.svgGroup.onclick = this.click;
                                }
                            }
                        }
                        else {
                            this.firstSelectedNode = node;
                        }
                    }
                }
            };
            this.clickCallback = [];
            this.graph.vertices.forEach((v) => {
                v.svgGroup.onclick = this.click;
                if (v.surface != null) {
                    v.surface.setAttribute("class", v.isNoParent ? this.rootNodeClass : this.unrootNodeClass);
                }
            });
            this.create(Clicker.createSLP(text));
            this.history.push(this.slp.copy());
        }
        changeHistory(i) {
            this.firstSelectedNode = null;
            this.create(this.history[i].copy());
            this.historyIndex = i;
        }
        get firstSelectedNode() {
            return this._firstSelectedNode;
        }
        set firstSelectedNode(value) {
            if (this.firstSelectedNode != null) {
                if (this.firstSelectedNode.surface != null) {
                    this.firstSelectedNode.surface.setAttribute("class", this.rootNodeClass);
                }
            }
            this._firstSelectedNode = value;
            if (this._firstSelectedNode != null) {
                if (this._firstSelectedNode.surface != null) {
                    this._firstSelectedNode.surface.setAttribute("class", this.chosenNodeClass);
                }
            }
        }
        static createSLP(text) {
            const slp = new Grammar.SLPDictionary();
            for (let i = 0; i < text.length; i++) {
                const c = slp.addChar(text[i]);
                slp.startVariables.push(c);
            }
            return slp;
        }
        createNodeFunc(node) {
            this.updateNode(node);
            node.svgGroup.onclick = this.click;
        }
        updateNode(node) {
            if (node.surface != null) {
                node.surface.setAttribute("class", node.isNoParent ? this.rootNodeClass : this.unrootNodeClass);
            }
        }
        connect(i) {
            if (this.historyIndex + 1 < this.history.length)
                this.history.splice(this.historyIndex + 1, this.history.length - this.historyIndex - 1);
            const result = super.connect(i);
            result.outcomingEdges.forEach((v) => {
                const w = v.endVertex;
                if (w != null)
                    this.updateNode(w);
            });
            this.updateNode(result);
            this.history.push(this.slp.copy());
            this.historyIndex++;
            this.clickCallback.forEach((v) => v());
            return result;
        }
    }
    Grammar.Clicker = Clicker;
})(Grammar || (Grammar = {}));
var StringModule;
(function (StringModule) {
    function bunrui(strings) {
        const p = {};
        for (let i = 0; i < strings.length; i++) {
            const str1 = strings[i];
            const c = str1.charCodeAt(0);
            if (p[c] == null)
                p[c] = new Array(0);
            p[c].push(str1.substring(1));
        }
        return p;
    }
    function getChars(str) {
        const p = {};
        const r = new Array(0);
        let count = 0;
        for (let i = 0; i < str.length; i++) {
            if (p[str.charAt(i)] == null) {
                p[str.charCodeAt(i)] = str.charAt(i);
                count++;
            }
            else {
            }
        }
        for (let c in p) {
            const ch = +c;
            r.push(String.fromCharCode(ch));
        }
        return r;
    }
    function compare(str1, str2) {
        const min = Math.min(str1.length, str2.length);
        for (let i = 0; i <= min; i++) {
            if (str1.charAt(i) < str2.charAt(i)) {
                return [i, -1];
            }
            else if (str1.charAt(i) > str2.charAt(i)) {
                return [i, 1];
            }
        }
        if (str1 == str2) {
            return [str1.length, 0];
        }
        else {
            return str1.length < str2.length ? [str1.length, 1] : [str2.length, -1];
        }
    }
    StringModule.compare = compare;
    function computeSuffixArray(str) {
        const arr = new Array(str.length);
        for (let i = 0; i < str.length; i++) {
            arr[i] = i;
        }
        const func = function (item1, item2) {
            for (let i = 0; i <= str.length; i++) {
                if (item1 + i >= str.length || item2 + i >= str.length)
                    break;
                if (str.charAt(item1 + i) < str.charAt(item2 + i)) {
                    return -1;
                }
                else if (str.charAt(item1 + i) > str.charAt(item2 + i)) {
                    return 1;
                }
            }
            if (item1 == item2) {
                return 0;
            }
            else {
                return item1 < item2 ? 1 : -1;
            }
        };
        arr.sort(func);
        return arr;
    }
    StringModule.computeSuffixArray = computeSuffixArray;
    function getSortedSuffixes(str) {
        const arr = computeSuffixArray(str);
        const r = new Array(arr.length);
        for (let i = 0; i < arr.length; i++) {
            r[i] = str.substring(arr[i]);
        }
        return r;
    }
    function computeLCP(str1, str2) {
        const min = str1.length < str2.length ? str1.length : str2.length;
        let lcp = 0;
        for (let i = 0; i < min; i++) {
            if (str1.charAt[i] == str2.charAt[i]) {
                lcp++;
            }
            else {
                break;
            }
        }
        return lcp;
    }
    function computeLCPArray(str, sufarr) {
        const lcparr = new Array(sufarr.length);
        for (let i = 0; i < sufarr.length; i++) {
            if (i == 0 && sufarr.length > 1) {
                lcparr[i] = -1;
            }
            else {
                lcparr[i] = computeLCP(str.substring(sufarr[i]), str.substring(sufarr[i - 1]));
            }
        }
        return lcparr;
    }
    function createAllSuffixes(str) {
        let str2 = "";
        for (let i = 0; i < str.length; i++) {
            if (str[i] != "\n") {
                str2 += str[i];
            }
        }
        const suffixes = new Array(str2.length);
        for (let i = 0; i < suffixes.length; i++) {
            suffixes[i] = str2.substring(i);
        }
        return suffixes;
    }
    StringModule.createAllSuffixes = createAllSuffixes;
    function createAllTruncatedSuffixes(str, truncatedLength) {
        const suffixes = new Array(str.length);
        for (let i = 0; i < suffixes.length; i++) {
            suffixes[i] = str.substr(i, truncatedLength);
        }
        return suffixes;
    }
    StringModule.createAllTruncatedSuffixes = createAllTruncatedSuffixes;
    function removeSpace(str) {
        let r = "";
        const emptyCode = " ".charCodeAt(0);
        for (let i = 0; i < str.length; i++) {
            if (str.charCodeAt(i) != emptyCode) {
                r += str.charAt(i);
            }
        }
        return r;
    }
    StringModule.removeSpace = removeSpace;
    function removeFirstSpaces(str) {
        let i = 0;
        for (i = 0; i < str.length; i++) {
            if (str[i] != " ")
                break;
        }
        if (i == str.length) {
            return "";
        }
        else {
            return str.substring(i);
        }
    }
    function reverse(str) {
        const rv = [];
        for (let i = 0, n = str.length; i < n; i++) {
            rv[i] = str.charAt(n - i - 1);
        }
        return rv.join("");
    }
    StringModule.reverse = reverse;
    function LZ77WithSelfReference(str) {
        const r = new Array(0);
        let startPos = 0;
        let lastRefPos = -1;
        let i = 0;
        while (i < str.length) {
            const substr = str.substr(startPos, i - startPos + 1);
            const reference = i == 0 ? "" : str.substr(0, i);
            const refPos = reference.indexOf(substr);
            if (refPos == -1) {
                if (lastRefPos == -1) {
                    r.push(substr);
                    i++;
                }
                else {
                    r.push([lastRefPos, i - startPos]);
                }
                startPos = i;
                lastRefPos = -1;
            }
            else {
                lastRefPos = refPos;
                i++;
            }
        }
        if (lastRefPos != -1) {
            r.push([lastRefPos, str.length - startPos]);
        }
        return r;
    }
    StringModule.LZ77WithSelfReference = LZ77WithSelfReference;
    function computeEditDisutanceTable(str1, str2) {
        str1 = "_" + str1;
        str2 = "_" + str2;
        const r = new Array(str1.length);
        for (let y = 0; y < r.length; y++) {
            r[y] = new Array(str2.length);
            for (let x = 0; x < r[y].length; x++) {
                r[y][x] = -1;
            }
        }
        for (let y = 0; y < str1.length; y++) {
            r[y][0] = y;
        }
        for (let x = 0; x < str2.length; x++) {
            r[0][x] = x;
        }
        for (let y = 0; y < r.length; y++) {
            for (let x = 0; x < r[y].length; x++) {
                if (r[y][x] == -1) {
                    const top = r[y - 1][x] + 1;
                    const left = r[y][x - 1] + 1;
                    const upperLeft = r[y - 1][x - 1] + (str1[y] == str2[x] ? 0 : 1);
                    r[y][x] = Math.min(top, left, upperLeft);
                }
            }
        }
        return r;
    }
    StringModule.computeEditDisutanceTable = computeEditDisutanceTable;
})(StringModule || (StringModule = {}));
var TreeFunctions;
(function (TreeFunctions) {
    class TreeNode {
        constructor() {
            this.children = [];
            this.edgeText = "";
            this.nodeText = "";
            this.parent = null;
            this._id = TreeNode.counter++;
            this.tag = "";
        }
        get id() {
            return this._id;
        }
        get path() {
            if (this.parent == null) {
                return this.edgeText;
            }
            else {
                return this.parent.path + this.edgeText;
            }
        }
        get isRoot() {
            return this.parent == null;
        }
        get isLeaf() {
            return this.children.length == 0;
        }
        getNodes() {
            const r = [this];
            if (this.isLeaf) {
                return r;
            }
            else {
                this.children.forEach(function (x, i, arr) {
                    x.getNodes().forEach(function (y, j, arr2) {
                        r.push(y);
                    });
                });
                return r;
            }
        }
        addLeaf(insertIndex, str) {
            const newNode = new TreeNode();
            newNode.parent = this;
            newNode.edgeText = str;
            this.children.splice(insertIndex, 0, newNode);
            return newNode;
        }
        split(splitPosition) {
            const pref = this.edgeText.substr(0, splitPosition);
            const suf = this.edgeText.substr(splitPosition);
            if (this.parent != null) {
                const newNode = new TreeNode();
                const i = this.parent.children.indexOf(this);
                this.parent.children[i] = newNode;
                newNode.children.push(this);
                this.parent = newNode;
                newNode.edgeText = pref;
                this.edgeText = suf;
                return newNode;
            }
            else {
                return this;
            }
        }
        locus(pattern) {
            //const matchLen = 0;
            if (pattern.length == 0)
                return [this, 0];
            const [matchLen, comp] = StringModule.compare(this.edgeText, pattern);
            if (matchLen == this.edgeText.length && this.edgeText.length < pattern.length) {
                const edges = this.children.map(function (x, i, arr) { return x.edgeText.charCodeAt(0); });
                const suf = pattern.substr(matchLen);
                const [i, b] = getInsertIndex(edges, suf.charCodeAt(0));
                if (b) {
                    return this.children[i].locus(suf);
                }
                else {
                    return [this, matchLen];
                }
            }
            else {
                return [this, matchLen];
            }
        }
        toLogicTree() {
            const node = new GraphTableSVG.LogicTree({ item: this, children: [], vertexText: this.nodeText, parentEdgeText: this.edgeText });
            this.children.forEach((v) => node.children.push(v.toLogicTree()));
            return node;
        }
    }
    TreeNode.counter = 0;
    TreeFunctions.TreeNode = TreeNode;
    function addString(node, pattern) {
        if (pattern.length == 0)
            return node;
        const edges = node.children.map(function (x, i, arr) { return x.edgeText.charCodeAt(0); });
        const [i, isMatch] = getInsertIndex(edges, pattern.charCodeAt(0));
        if (!isMatch) {
            node.addLeaf(i, pattern[0]);
        }
        return addString(node.children[i], pattern.substr(1));
    }
    TreeFunctions.addString = addString;
    function getInsertIndex(texts, pattern) {
        for (let i = 0; i < texts.length; i++) {
            if (pattern < texts[i]) {
            }
            else if (pattern == texts[i]) {
                return [i, true];
            }
            else {
                return [i, false];
            }
        }
        return [texts.length, false];
    }
    TreeFunctions.getInsertIndex = getInsertIndex;
    function shrink(root) {
        if (root.parent != null) {
            if (root.children.length == 1) {
                const child = root.children[0];
                const i = root.parent.children.indexOf(root);
                root.parent.children[i] = child;
                child.parent = root.parent;
                child.edgeText = root.edgeText + child.edgeText;
                shrink(child);
            }
            else {
                root.children.forEach(function (x) { shrink(x); });
            }
        }
        else {
            root.children.forEach(function (x) { shrink(x); });
        }
    }
    TreeFunctions.shrink = shrink;
    /*
    function createNode(treeNode: TreeNode, graph: GraphTableSVG.Graph): GraphTableSVG.Vertex {
        const node = GraphTableSVG.CircleVertex.create(graph);
        node.svgText.textContent = treeNode.id.toString();
        treeNode.tag = node.objectID;
        return node;

        
    }
    */
})(TreeFunctions || (TreeFunctions = {}));
var TreeFunctions;
(function (TreeFunctions) {
    function createTrie(text) {
        const root = new TreeFunctions.TreeNode();
        for (let i = 0; i < text.length; i++) {
            TreeFunctions.addString(root, text.substr(i));
            //root.addString(text.substr(i));
        }
        return root;
    }
    TreeFunctions.createTrie = createTrie;
    function createSuffixTree(text) {
        const root = new TreeFunctions.TreeNode();
        for (let i = 0; i < text.length; i++) {
            TreeFunctions.addString(root, text.substr(i));
            //root.addString(text.substr(i));
        }
        TreeFunctions.shrink(root);
        return root;
    }
    TreeFunctions.createSuffixTree = createSuffixTree;
    /*
    function addString1(node: TreeNode, pattern: string): TreeNode {
        if (pattern.length == 0) return node;
        const edges = node.children.map(function (x, i, arr) { return x.edgeText.charCodeAt(0); });
        const [i, isMatch] = getInsertIndex(edges, pattern.charCodeAt(0));
        if (!isMatch) {
            return node.addLeaf(i, pattern[0]);
        } else {
            return addString1(node.children[i], pattern.substr(1));
        }

    }
    */
})(TreeFunctions || (TreeFunctions = {}));
class FooterButton {
    constructor(_buttonIDPrefix, _contentIDPrefix, id) {
        const content = document.getElementById(`${_contentIDPrefix}${id}`);
        const button = document.getElementById(`${_buttonIDPrefix}${id}`);
        if (content == null || button == null)
            throw Error("Null Error");
        this.button_ = button;
        this.content_ = content;
        //this.isActive = isActive;
    }
    /*
    static isActive(content: HTMLDivElement) {
        return content.style.display == "block";
    }
    
    static getContent(_contentIDPrefix: string, id: number): HTMLDivElement {
        const content: HTMLDivElement = <HTMLDivElement>document.getElementById(`${_contentIDPrefix}${id}`);
        return content;
    }
    static getButton(_buttonIDPrefix: string, id: number): HTMLDivElement {
        const content: HTMLDivElement = <HTMLDivElement>document.getElementById(`${_buttonIDPrefix}${id}`);
        return content;
    }
    static setMode(value: boolean) {
        if (value) {
            this.content_.style.display = "block";
            this.button_.style.backgroundColor = FooterButton.activeColor;
            this.content_.style.backgroundColor = FooterButton.activeColor;
        } else {

            this.content_.style.display = "none";
            this.button_.style.backgroundColor = FooterButton.nonActiveColor;
            this.content_.style.backgroundColor = FooterButton.nonActiveColor;
        }
        this.isActive_ = value;

    }
    */
    get isActive() {
        return this.content_.style.display == "block";
    }
    get isExist() {
        return this.content_ != null;
    }
    set isActive(value) {
        if (value) {
            this.content_.style.display = "block";
            this.button_.style.backgroundColor = FooterButton.activeColor;
            this.content_.style.backgroundColor = FooterButton.activeColor;
        }
        else {
            this.content_.style.display = "none";
            this.button_.style.backgroundColor = FooterButton.nonActiveColor;
            this.content_.style.backgroundColor = FooterButton.nonActiveColor;
        }
    }
    static call(_buttonIDPrefix, _contentIDPrefix, id) {
        let i = 0;
        while (true) {
            const item = new FooterButton(_buttonIDPrefix, _contentIDPrefix, i);
            if (!item.isExist)
                break;
            item.isActive = i == id;
            i++;
        }
    }
}
FooterButton.activeColor = "#31c8aa";
FooterButton.nonActiveColor = "#524b4b";
